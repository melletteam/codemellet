<?php
/**
 * Partie de code qui est utilisé pour les acteurs
 */

class acteursManager
{
    /**
     * @var PDO
     */
    private $db;
    /**
     * acteursManager constructor.
     * @param PDO $db
     */
    function __construct($db)
    {
        $this->db = $db;
    }
    /**
     * @return Acteurs[]
     */

    /**
     * selection de tous les acteurs triés par leurs noms
     */
    function selectAll(){
        $query = "Select * from acteurs ORDER BY Nom_Acteur ";
        $prep = null;
        $acteurs = array();
        try{
            $prep = $this->db->prepare($query);
            $prep->execute();
            $arr = $prep->fetchAll();
            foreach ($arr as $specArr){
                $acteur= new Acteurs($specArr);
                array_push($acteurs,$acteur);
            }
        }catch (PDOException $e){
            die($e->getMessage());
        }finally{
            $prep->closeCursor();
        }
        return $acteurs;
    }

    /**
     * @param Acteurs $acteur
     */
    /**
     * enregistrement d'un acteur
     */
    function save($acteur)
    {
        $query = null;
        if($acteur->getID_Acteur() <= 0){
            $query = "INSERT INTO acteurs(Nom_Acteur,Prenom_Acteur,Statut_Acteurs)VALUES (:Nom_Acteur,:Prenom_Acteur,:Statut_Acteurs)";
        }else{
            $query="update acteurs set Nom_Acteur=:Nom_Acteur WHERE ID_Acteur=:ID_Acteur";
        }
        $prep = null;
        try{
            $prep=$this->db->prepare($query);

            if($acteur->getID_Acteur() <= 0){
                $prep->bindValue(":Nom_Acteur",$acteur->getNom_Acteur(),PDO::PARAM_STR);
                $prep->bindValue(":Prenom_Acteur",$acteur->getPrenom_Acteur(),PDO::PARAM_STR);
                $prep->bindValue(":Statut_Acteurs",$acteur->getStatut_Acteurs(),PDO::PARAM_STR);

            }
            if($acteur->getID_Acteur() > 0){
                $prep->bindValue(":ID_Acteur",$acteur->getID_Acteur(),PDO::PARAM_INT);
            }
            $prep->execute();
            if($acteur->getID_Acteur() <= 0){
                $acteur->setID_Acteur($this->db->lastInsertId());
            }
        }catch (Exception $e){
            die($e->getMessage());

        }finally{
            $prep->closeCursor();
        }
    }

    /**
     * @param Acteurs $acteur
     */
    /**
     * suppression d'un acteur
     */
    function delete($acteur){
        $query = "DELETE FROM acteurs WHERE ID_Acteur=:ID_Acteur";
        $prep = null;
        try{
            $prep = $this->db->prepare($query);
            $prep->bindValue(":ID_Acteur",$acteur->getID_Acteur(),PDO::PARAM_INT);
            $no = $prep->execute();
        }catch(PDOException $e){
            die($e->getMessage());
        }finally {
            $prep = null;
        }
    }

    /**
     * selection d'un acteur via son ID
     */
    public function getActeur($ID_Acteur)
    {
        $acteur = null;
        try {
            $prep = null;
            $query = "SELECT * FROM acteurs WHERE ID_Acteur=:ID_Acteur";
            $prep = $this->db->prepare($query);
            $prep->bindValue(":ID_Acteur", $ID_Acteur, PDO::PARAM_INT);
            $prep->execute();
            $arr = $prep->fetch();
            if ($arr) {
                $acteur= new acteurs($arr);
            }
        }
        catch (Exception $e) {
            die($e->getMessage());
        }
        finally {
            if($prep != null)
                $prep->closeCursor();
        }
        return $acteur;
    }
    /**
     * recherche d'un acteur dans la DB
     */
    public function getfullActeur($varRecherche)
    {
        $acteurs = array();
        try {
            $prep = null;
            $query ="SELECT * FROM acteurs WHERE Nom_Acteur LIKE :search OR Prenom_Acteur LIKE :search OR Statut_Acteurs LIKE :search ";
            $prep = $this->db->prepare($query);
            $prep->bindValue(":search", '%'.$varRecherche.'%', PDO::PARAM_STR);
            $prep->execute();
            $arr = $prep->fetchAll();
            foreach ($arr as $specArr){
                $acteur= new Acteurs($specArr);
                array_push($acteurs,$acteur);
            }
        }
        catch (Exception $e) {
            die($e->getMessage());
        }
        finally {
            if($prep != null)
                $prep->closeCursor();
        }
        return $acteurs;

    }

}