<?php
/**
 * Partie du code qui est utilisé pour la table représentation
 */

class representationManager
{
    /**
     * @var PDO
     */
    private $db;
    /**
     * representationManager constructor.
     * @param PDO $db
     */
    function __construct($db)
    {
        $this->db = $db;
    }
    /**
     * representationManager constructor.
     * @param representation $representation
     */
    /**
     * création d'une representation
     */
    function createRepresentationAndReturnId($representation)
    {
        $query = "INSERT INTO representation(Date_Representation,Nb_Lignes_Representation,Nb_Col_Representation,ID_Spectacle) VALUES (:Date_Representation,:Nb_Lignes_Representation,:Nb_Col_Representation,:ID_Spectacle)";
        $prep = null;
        $representationID = null;
        try{
            $prep=$this->db->prepare($query);
            $prep->bindValue(":Date_Representation",$representation->getDate_Representation(),PDO::PARAM_STR);
            $prep->bindValue(":Nb_Lignes_Representation",$representation->getNb_Lignes_Representation(),PDO::PARAM_INT);
            $prep->bindValue(":Nb_Col_Representation",$representation->getNb_Col_Representation(),PDO::PARAM_INT);
            $prep->bindValue(":ID_Spectacle",$representation->getID_Spectacle(),PDO::PARAM_INT);
            $prep->execute();
            $representationID = $this->db->lastInsertId();
        }catch (Exception $e){
            die($e->getMessage());

        }finally{
            $prep->closeCursor();
        }
        return $representationID;
    }
    /**
     * @return representations[]
     */
    /**
     * Sélection de toutes les representations
     */
    function  selectAllRepresentation(){
        $query = "Select * from representation ORDER BY Date_Representation DESC ";
        $prep = null;
        $representations = array();
        try{
            $prep = $this->db->prepare($query);
            $prep->execute();
            $arr = $prep->fetchAll();
            foreach ($arr as $specArr){
                $representation= new representation($specArr);
                array_push($representations,$representation);
            }
        }catch (PDOException $e){
            die($e->getMessage());
        }finally{
            $prep->closeCursor();
        }
        return $representations;
    }
    /**
     * Sélection d'une representation en fonction de l'ID du spectacle
     */
    function selectByIDSpectacle($id){
        $representations = array();
        try {
            $prep = null;
            $query ="SELECT * FROM representation WHERE ID_Spectacle = :ID_Spectacle ORDER BY Date_Representation";
            $prep = $this->db->prepare($query);
            $prep->bindValue(":ID_Spectacle", $id, PDO::PARAM_INT);
            $prep->execute();
            $arr = $prep->fetchAll();
            foreach ($arr as $specArr){
                $representation = new representation($specArr);
                array_push($representations,$representation);
            }
        }
        catch (Exception $e) {
            die($e->getMessage());
        }
        finally {
            if($prep != null)
                $prep->closeCursor();
        }
        return $representations;

    }
    /**
     * Sélection du nombre de colonnes en fonction de l'ID de la representation
     */
    function selectNbCol($id){
        $nbCol=0;
        try {
            $prep = null;
            $query ="SELECT Nb_Col_Representation FROM representation WHERE ID_Representation = :ID_Representation";
            $prep = $this->db->prepare($query);
            $prep->bindValue(":ID_Representation", $id, PDO::PARAM_INT);
            $prep->execute();
            $arr = $prep->fetch();
            $nbCol=$arr[0];
        }
        catch (Exception $e) {
            die($e->getMessage());
        }
        finally {
            if($prep != null)
                $prep->closeCursor();
        }
        return $nbCol;

    }
    /**
     * Sélection du nombre de lignes en fonction de l'ID de la representation
     */
    function selectNbLignes($id){
        $nbLignes=0;
        try {
            $prep = null;
            $query ="SELECT Nb_Lignes_Representation FROM representation WHERE ID_Representation = :ID_Representation";
            $prep = $this->db->prepare($query);
            $prep->bindValue(":ID_Representation", $id, PDO::PARAM_INT);
            $prep->execute();
            $arr = $prep->fetch();
            $nbLignes=$arr[0];
        }
        catch (Exception $e) {
            die($e->getMessage());
        }
        finally {
            if($prep != null)
                $prep->closeCursor();
        }
        return $nbLignes;

    }

}