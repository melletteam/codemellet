<?php
/**
 * Partie du code qui est utilisé pour la table assurer
 */
class assurerManager
{
    /**
     * @var PDO
     */
    private $db;
    /**
     * assurerManager constructor.
     * @param PDO $db
     */
    function __construct($db)
    {
        $this->db = $db;
    }
    /**
     * @param assurer $assure
     */
    /**
     * ajout d'un acteur à la table assurer
     */
    function addAssurer($assure){
        $query = null;
            $query = "INSERT INTO assurer(ID_Acteur,ID_Spectacle,Role_Acteur) VALUES (:ID_Acteur,:ID_Spectacle,:Role_Acteur)";

            $prep = null;
            try {
                $prep = $this->db->prepare($query);
                $prep->bindValue(":ID_Acteur", $assure->getID_Acteur(), PDO::PARAM_INT);
                $prep->bindValue(":ID_Spectacle", $assure->getID_Spectacle(), PDO::PARAM_INT);
                $prep->bindValue(":Role_Acteur", $assure->getRole_Acteur(), PDO::PARAM_STR);
                $prep->execute();
            }
            catch
                (Exception $e){
                    die($e->getMessage());

                }finally{
                    $prep->closeCursor();
                    }
    }
    /**
     * Sélection de la table assurer en fonction du spectacle
     */
    function selectByIDSpectacle($id){
            $assures = array();
            try {
                $prep = null;
                $query ="SELECT * FROM assurer WHERE ID_Spectacle = :ID_Spectacle ";
                $prep = $this->db->prepare($query);
                $prep->bindValue(":ID_Spectacle", $id, PDO::PARAM_INT);
                $prep->execute();
                $arr = $prep->fetchAll();
                foreach ($arr as $specArr){
                    $assure = new assurer($specArr);
                    array_push($assures,$assure);
                }
            }
            catch (Exception $e) {
                die($e->getMessage());
            }
            finally {
                if($prep != null)
                    $prep->closeCursor();
            }
            return $assures;

        }

}