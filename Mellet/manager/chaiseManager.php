<?php
/**
 * Partie du code qui est utilisé pour la table chaise
 */
class chaiseManager
{
    /**
     * @var PDO
     */
    private $db;
    /**
     * chaiseManager constructor.
     * @param PDO $db
     */
    function __construct($db)
    {
        $this->db = $db;
    }
    /**
     * @param chaise $chaise
     */
    /**
     * ajout d'une chaise dans la DB
     */
    function ajoutChaise($chaise)
    {
        $query = "INSERT INTO chaise (ID_Representation,Num_Lignes_Chaise,Num_Col_Chaise,Statut_Chaise) VALUES (:ID_Representation,:Num_Lignes_Chaise,:Num_Col_Chaise,:Statut_Chaise)";
        $prep = null;
        try{
            $prep=$this->db->prepare($query);
            $prep->bindValue(":ID_Representation",$chaise->getID_Representation(),PDO::PARAM_INT);
            $prep->bindValue(":Num_Lignes_Chaise",$chaise->getNum_Lignes_Chaise(),PDO::PARAM_STR);
            $prep->bindValue(":Num_Col_Chaise",$chaise->getNum_Col_Chaise(),PDO::PARAM_INT);
            $prep->bindValue(":Statut_Chaise",$chaise->getStatut_Chaise(),PDO::PARAM_STR);
            $prep->execute();
        }catch (Exception $e){
            die($e->getMessage());

        }finally{
            $prep->closeCursor();
        }
    }
    /**
     * Sélection de toutes les chaises en fonction de la représentation choisie
     */
    function selectAll($id){
        $query = "Select * from chaise where ID_Representation=:ID_Representation ORDER BY ID_Chaise ASC";
        $prep = null;
        $chaises = array();
        try{
            $prep = $this->db->prepare($query);
            $prep->bindValue(":ID_Representation",$id,PDO::PARAM_INT);
            $prep->execute();
            $arr = $prep->fetchAll();
            foreach ($arr as $specArr){
                $chaise= new chaise($specArr);
                array_push($chaises,$chaise);
            }
        }catch (PDOException $e){
            die($e->getMessage());
        }finally{
            $prep->closeCursor();
        }
        return $chaises;
    }
    /**
     * @param chaise $chaise
     */
    /**
     * Mise à jour du statut et de l'ID de resevation en fonction du numéro de ligne, de colonne et de l'ID de la representation
     */
    function UpdateChaise($chaise)
    {
        $query = "update chaise set Statut_Chaise=:Statut_Chaise,ID_Reservation=:ID_Reservation WHERE Num_Lignes_Chaise=:Num_Lignes_Chaise AND Num_Col_Chaise=:Num_Col_Chaise AND ID_Representation=:ID_Representation";
        $prep = null;
        try{
            $prep=$this->db->prepare($query);
            $prep->bindValue(":Statut_Chaise",$chaise->getStatut_Chaise(),PDO::PARAM_STR);
            $prep->bindValue(":ID_Reservation",$chaise->getID_Reservation(),PDO::PARAM_INT);
            $prep->bindValue(":Num_Lignes_Chaise",$chaise->getNum_Lignes_Chaise(),PDO::PARAM_STR);
            $prep->bindValue(":Num_Col_Chaise",$chaise->getNum_Col_Chaise(),PDO::PARAM_INT);
            $prep->bindValue(":ID_Representation",$chaise->getID_Representation(),PDO::PARAM_INT);
            $prep->execute();
        }catch (Exception $e){
            die($e->getMessage());

        }finally{
            $prep->closeCursor();
        }
    }
    /**
     * @param chaise $chaise
     */
    /**
     * mise à jour du statut et de l'ID de resevation en fonction de l'ID de la representation et de l'ID de reservation
     */
    function UpdateChaiseByID($chaise)
    {
        $query = "update chaise set Statut_Chaise=:Statut_Chaise,ID_Reservation=:ID_ReservationVal WHERE ID_Representation=:ID_Representation AND ID_Reservation=:ID_Reservation";
        $prep = null;
        try{
            $prep=$this->db->prepare($query);
            $prep->bindValue(":ID_Reservation",$chaise->getID_Reservation(),PDO::PARAM_INT);
            $prep->bindValue(":Statut_Chaise","D",PDO::PARAM_STR);
            $prep->bindValue(":ID_ReservationVal",null,PDO::PARAM_INT);
            $prep->bindValue(":ID_Representation",$chaise->getID_Representation(),PDO::PARAM_INT);
            $prep->execute();
        }catch (Exception $e){
            die($e->getMessage());

        }finally{
            $prep->closeCursor();
        }
    }

}