<?php
/**
 * Partie du code qui est utilisé pour les Users
 */

class administrationManager
{
    /**
     * @var PDO
     */
    private $db;
    /**
     * AdministrationManager constructor.
     * @param PDO $db
     */
    function __construct($db)
    {
        $this->db = $db;
    }
    /**
     * @return User[]
     */
    /**
     * Selection de tous les Users
     */
    function selectAll(){
        $query = "Select * from user ORDER BY Login_User ";
        $prep = null;
        $users = array();
        try{
            $prep = $this->db->prepare($query);
            $prep->execute();
            $arr = $prep->fetchAll();
            foreach ($arr as $specArr){
                $user= new Administration($specArr);
                array_push($users,$user);
            }
        }catch (PDOException $e){
            die($e->getMessage());
        }finally{
            $prep->closeCursor();
        }
        return $users;
    }

    /**
     * @param User $user
     */
    /**
     * Enregistrement d'un User
     */
    function save($user)
    {
        $query = null;
        if($user->getID_User() <= 0){
            $query = "INSERT INTO user(Login_User,Pswd_User,Statut_User) VALUES (:Login_User,:Pswd_User,:Statut_User)";
        }else{
            $query="UPDATE user SET Login_User=:Login_User WHERE ID_User=:ID_User";
        }
        $prep = null;
        try{
            $prep=$this->db->prepare($query);

            if($user->getID_User() <= 0){
                $prep->bindValue(":Login_User",$user->getLogin_User(),PDO::PARAM_STR);
                $prep->bindValue(":Pswd_User",$user->getPswd_User(),PDO::PARAM_STR);
                $prep->bindValue(":Statut_User",$user->getStatut_User(),PDO::PARAM_STR);

            }
            if($user->getID_User() > 0){
                $prep->bindValue(":ID_User",$user->getID_User(),PDO::PARAM_INT);
            }
            $prep->execute();
            if($user->getID_User() <= 0){
                $user->setID_User($this->db->lastInsertId());
            }
        }catch (Exception $e){
            die($e->getMessage());

        }finally{
            $prep->closeCursor();
        }
    }

    /**
     * @param User $user
     */
    /**
     * Suppression d'un User
     */
    function delete($user){
        $query = "DELETE FROM user WHERE ID_User=:ID_User";
        $prep = null;
        try{
            $prep = $this->db->prepare($query);
            $prep->bindValue(":ID_User",$user,PDO::PARAM_INT);
            $prep->execute();
        }catch(PDOException $e){
            die($e->getMessage());
        }finally {
            $prep = null;
        }
    }
    /**
     * Selection d'un User via son ID
     */
    public function getUser($ID_User)
    {
        $user = null;
        try {
            $prep = null;
            $query = "SELECT * FROM user WHERE ID_User=:ID_User";
            $prep = $this->db->prepare($query);
            $prep->bindValue(":ID_User", $ID_User, PDO::PARAM_INT);
            $prep->execute();
            $arr = $prep->fetch();
            if($arr!=null){
                $user=new Administration($arr);
            }

        }
        catch (Exception $e) {
            die($e->getMessage());
        }
        finally {
            if($prep != null)
                $prep->closeCursor();
        }
        return $user;
    }
    /**
     * Sélection d'un User via son nom
     */
    public function getUserByName($nom)
    {
        $user = null;
        try {
            $prep = null;
            $query = "SELECT * FROM user WHERE Login_User=:Login_User";
            $prep = $this->db->prepare($query);
            $prep->bindValue(":Login_User", $nom, PDO::PARAM_STR);
            $prep->execute();
            $arr= $prep->fetch();
            if($arr!=null){
                $user=new user($arr);
            }

        }
        catch (Exception $e) {
            die($e->getMessage());
        }
        finally {
            if($prep != null)
                $prep->closeCursor();
        }
        return $user;
    }

    /**
     * Modification du statut d'un User
     */
    public function modify($ID_User, $Statut)
    {
        $prep=null;
        try{
            $query = "UPDATE user SET Statut_User=:Statut_User WHERE ID_User=:ID_User";
            $prep = $this->db->prepare($query);
            $prep->bindValue(":ID_User", $ID_User, PDO::PARAM_INT);
            $prep->bindValue(":Statut_User", $Statut, PDO::PARAM_INT);
            $prep->execute();

        }catch (Exception $e){
            die($e->getMessage());
        }
        finally{
            if($prep != null)
                $prep->closeCursor();
        }


    }

    /**
     * Sélection du statut d'un User via son ID
     */
    public function getStatut($ID_User)
    {
        $userStatut=null;
        $prep=null;
        try{
            $query = "SELECT Statut_User FROM user WHERE ID_User=:ID_User";
            $prep = $this->db->prepare($query);
            $prep->bindValue(":ID_User", $ID_User, PDO::PARAM_INT);
            $prep->execute();
            $arr = $prep->fetch();
            if($arr){
                $userStatut = new Administration($arr);
            }

        }catch (Exception $e){
            die($e->getMessage());
        }
        finally{
            if($prep != null)
                $prep->closeCursor();
        }

        return $userStatut->getStatut_User();
    }

    /**
     * Suppression des tables : assurer, chaise, reservation, representation, spectacle
     */
    public function deleteSpectacles(){
        $prep=null;
        try{
            $query1 = "DELETE FROM assurer";
            $queryAssurer = $this->db->prepare($query1);
            $queryAssurer->execute();

            $query2 = "DELETE FROM chaise";
            $queryChaise = $this->db->prepare($query2);
            $queryChaise->execute();

            $query4 = "DELETE FROM reservation";
            $queryReservation = $this->db->prepare($query4);
            $queryReservation->execute();

            $query3 = "DELETE FROM representation";
            $queryRepresentation = $this->db->prepare($query3);
            $queryRepresentation->execute();

            $query5 = "DELETE FROM spectacle";
            $querySpecatcle = $this->db->prepare($query5);
            $querySpecatcle->execute();

        }catch (Exception $e){
            die($e->getMessage());
        }
        finally{
            if($prep != null)
                $prep->closeCursor();
        }
    }

    /**
     * Modification d'un mot de passe
     */
    public function modifierPswd($ID_User, $pswd)
    {
        $prep=null;
        try{
            $query = "UPDATE user SET Pswd_User=:Pswd_User WHERE ID_User=:ID_User";
            $prep = $this->db->prepare($query);
            $prep->bindValue(":ID_User", $ID_User, PDO::PARAM_INT);
            $prep->bindValue(":Pswd_User", $pswd, PDO::PARAM_INT);
            $prep->execute();

        }catch (Exception $e){
            die($e->getMessage());
        }
        finally{
            if($prep != null)
                $prep->closeCursor();
        }


    }

}