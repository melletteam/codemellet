<?php

/**
 * Partie du code qui s'occupe de la connexion/déconnexion à la DB
 */
class DBManager
{
    /**
     * @var PDO
     */
    private $connection;

    /**
     * @return PDO
     */

    /**
     * connexion à la DB
     */
    function connect(){

        $strConnection = 'mysql:host=localhost;dbname=mellet';
        try{
            $this->connection = new PDO($strConnection,'root','');
            $this->connection ->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
            $this->connection;
        }catch (PDOException $e){
            die($e->getMessage());
        }
        return $this->connection;
    }

    /**
     * déconnexion à la DB
     */
    function disconnect(){
        $this->connection=null;
    }
}