<?php
/**
 * Partie du code qui est utilisé pour la table spectateur
 */
class spectateurManager
{
    /**
     * @var PDO
     */
    private $db;
    /**
     * spectateurManager constructor.
     * @param PDO $db
     */
    function __construct($db)
    {
        $this->db = $db;
    }
    /**
     * @return Spectateur[]
     */
    /**
     * selection de tous les spectateurs
     */
    function selectAll(){
        $query = "Select * from spectateur ORDER BY Nom_Spectateur ";
        $prep = null;
        $spectateurs = array();
        try{
            $prep = $this->db->prepare($query);
            $prep->execute();
            $arr = $prep->fetchAll();
            foreach ($arr as $specArr){
                $spectateur= new Spectateur($specArr);
                array_push($spectateurs,$spectateur);
            }
        }catch (PDOException $e){
            die($e->getMessage());
        }finally{
            $prep->closeCursor();
        }
        return $spectateurs;
    }

    /**
     * @param Spectateur $spect
     */
    /**
     * enregistrement d'un spectateur en DB
     */
    function save($spect)
    {
        $query = null;
        if($spect->getID_Spectateur() <= 0){
            $query = "INSERT INTO spectateur(Nom_Spectateur, Prenom_Spectateur, Rue_Spectateur, NumeroRue_Spectateur, CP_Spectateur, Ville_Spectateur, NumFix_Spectateur, NumGsm_Spectateur, Email_Spectateur, Com_Spectateur) 
VALUES (:Nom_Spectateur, :Prenom_Spectateur, :Rue_Spectateur, :NumeroRue_Spectateur, :CP_Spectateur, :Ville_Spectateur, :NumFix_Spectateur, :NumGsm_Spectateur, :Email_Spectateur, :Com_Spectateur)";
        }else{
            $query="update spectateur set Nom_Spectateur=:Nom_Spectateur WHERE id=:id";
        }
        $prep = null;
        try{
            $prep=$this->db->prepare($query);

            if($spect->getID_Spectateur() <= 0){
                $prep->bindValue(":Nom_Spectateur",$spect->getNom_Spectateur(),PDO::PARAM_STR);
                $prep->bindValue(":Prenom_Spectateur",$spect->getPrenom_Spectateur(),PDO::PARAM_STR);
                $prep->bindValue(":Rue_Spectateur",$spect->getRue_Spectateur(),PDO::PARAM_STR);
                $prep->bindValue(":NumeroRue_Spectateur",$spect->getNumeroRue_Spectateur(),PDO::PARAM_INT);
                $prep->bindValue(":CP_Spectateur",$spect->getCP_Spectateur(),PDO::PARAM_INT);
                $prep->bindValue(":Ville_Spectateur",$spect->getVille_Spectateur(),PDO::PARAM_STR);
                $prep->bindValue(":NumFix_Spectateur",$spect->getNumFix_Spectateur(),PDO::PARAM_STR);
                $prep->bindValue(":NumGsm_Spectateur",$spect->getNumGsm_Spectateur(),PDO::PARAM_STR);
                $prep->bindValue(":Email_Spectateur",$spect->getEmail_Spectateur(),PDO::PARAM_STR);
                $prep->bindValue(":Com_Spectateur",$spect->getCom_Spectateur(),PDO::PARAM_STR);

            }
            if($spect->getID_Spectateur() > 0){
                $prep->bindValue(":id",$spect->getID_Spectateur(),PDO::PARAM_INT);
            }
            $prep->execute();
            if($spect->getID_Spectateur() <= 0){
                $spect->setID_Spectateur($this->db->lastInsertId());
            }
        }catch (Exception $e){
            die($e->getMessage());

        }finally{
            $prep->closeCursor();
        }
    }

    /**
     * @param spectateur $spectateur
     */
    /**
     * Suppression d'un spectateur
     */
    function delete($spectateur){
        $query = "DELETE FROM spectateur WHERE ID_Spectateur=:ID_Spectateur";
        $prep = null;
        try{
            $prep = $this->db->prepare($query);
            $prep->bindValue(":ID_Spectateur",$spectateur->getID_Spectateur(),PDO::PARAM_INT);
            $no = $prep->execute();
        }catch(PDOException $e){
            die($e->getMessage());
        }finally {
            $prep = null;
        }
    }
    /**
     * Selection d'un spectateur en fonction de son ID
     */
    public function getSpectateur($ID_Spectateur)
    {
        $spectateur = null;
        try {
            $prep = null;
            $query = "SELECT * FROM spectateur WHERE ID_Spectateur=:ID_Spectateur";
            $prep = $this->db->prepare($query);
            $prep->bindValue(":ID_Spectateur", $ID_Spectateur, PDO::PARAM_INT);
            $prep->execute();
            $arr = $prep->fetch();
            if ($arr) {
                $spectateur = new Spectateur($arr);
            }
        }
        catch (Exception $e) {
            die($e->getMessage());
        }
        finally {
            if($prep != null)
                $prep->closeCursor();
        }
        return $spectateur;
    }
    /**
     * Selection d'un ou de plusieurs spectateurs en fonction d'une recherche
     */
    public function getfullSpectateur($varRecherche)
    {
        $spectateurs = array();
        try {
            $prep = null;
            $query ="SELECT * FROM spectateur WHERE Nom_Spectateur LIKE :search OR Prenom_Spectateur LIKE :search OR NumFix_Spectateur LIKE :search OR NumGsm_Spectateur LIKE :search";
            $prep = $this->db->prepare($query);
            $prep->bindValue(":search", '%'.$varRecherche.'%', PDO::PARAM_STR);
            $prep->execute();
            $arr = $prep->fetchAll();
            foreach ($arr as $specArr){
                $spectateur= new Spectateur($specArr);
                array_push($spectateurs,$spectateur);
            }
        }
        catch (Exception $e) {
            die($e->getMessage());
        }
        finally {
            if($prep != null)
                $prep->closeCursor();
        }
        return $spectateurs;

    }

}