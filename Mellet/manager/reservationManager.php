<?php
/**
 * Partie du code qui est utilisé pour la table réservation
 */

class reservationManager
{
    /**
     * @var PDO
     */
    private $db;
    /**
     * reservationManager constructor.
     * @param PDO $db
     */
    function __construct($db)
    {
        $this->db = $db;
    }
    /**
     * @param reservation $reser
     */
    /**
     * Ajout d'une reservation dans la table reservation
     */
    function ajoutReservation($reser)
    {
        $ReservationID=null;
        $query = "INSERT INTO reservation (NbPlaces_Reser,ID_Representation,ID_Spectateur) VALUES (:NbPlaces_Reser,:ID_Representation,:ID_Spectateur)";
        $prep = null;
        try{
            $prep=$this->db->prepare($query);
            $prep->bindValue(":ID_Representation",$reser->getID_Representation(),PDO::PARAM_INT);
            $prep->bindValue(":ID_Spectateur",$reser->getID_Spectateur(),PDO::PARAM_INT);
            $prep->bindValue(":NbPlaces_Reser",$reser->getNbPlaces_Reser(),PDO::PARAM_INT);
            $prep->execute();
            $ReservationID=$this->db->lastInsertId();
        }catch (Exception $e){
            die($e->getMessage());

        }finally{
            $prep->closeCursor();
        }
        return $ReservationID;
    }
    /**
     * @param reservation $reservation
     */
    /**
     * Suppression d'une reservation en fonction de l'ID de reservation et de representation
     */
    function delete($reservation){
        $query = "DELETE FROM reservation WHERE ID_Reservation=:ID_Reservation AND ID_Representation=:ID_Representation";
        $prep = null;
        try{
            $prep = $this->db->prepare($query);
            $prep->bindValue(":ID_Reservation",$reservation->getID_Reservation(),PDO::PARAM_INT);
            $prep->bindValue(":ID_Representation",$reservation->getID_Representation(),PDO::PARAM_INT);
            $no = $prep->execute();
        }catch(PDOException $e){
            die($e->getMessage());
        }finally {
            $prep = null;
        }
    }
    /**
     * @param reservation $reservation
     */
    /**
     * Sélection de toutes les reservations en fonction de l'ID de representation
     */
    function selectAllByRepre($id){
        $query = "Select * from reservation where ID_Representation=:ID_Representation";
        $prep = null;
        $reservations = array();
        try{
            $prep = $this->db->prepare($query);
            $prep->bindValue(":ID_Representation",$id,PDO::PARAM_INT);
            $prep->execute();
            $arr = $prep->fetchAll();
            foreach ($arr as $specArr){
                $reser= new reservation($specArr);
                array_push($reservations,$reser);
            }
        }catch (PDOException $e){
            die($e->getMessage());
        }finally{
            $prep->closeCursor();
        }
        return $reservations;
    }

    /**
     * Sélection du nombre de places réservées en fonction de l'ID de réservation
     */
    function selectByIDRes($id){
        $query = "Select NbPlaces_Reser from reservation where ID_Reservation=:ID_Reservation";
        $prep = null;
        $nbPL= null;
        try{
            $prep = $this->db->prepare($query);
            $prep->bindValue(":ID_Reservation",$id,PDO::PARAM_INT);
            $prep->execute();
            $arr = $prep->fetch();
            $nbPL= new reservation($arr);

        }catch (PDOException $e){
            die($e->getMessage());
        }finally{
            $prep->closeCursor();
        }
        return $nbPL;
    }

}