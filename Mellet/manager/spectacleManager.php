<?php
/**
 * Partie du code qui est utilisé pour la table spectacle
 */

class spectacleManager
{
    /**
     * @var PDO
     */
    private $db;
    /**
     * spectacleManager constructor.
     * @param PDO $db
     */
    function __construct($db)
    {
        $this->db = $db;
    }
    /**
     * création d'un spectacle
     */
    function createSpectacleAndReturnID($titre,$prix)
    {
        $spectacleID=null;
        $query = "INSERT INTO spectacle(Titre_Spectacle,Prix_Spectacle) VALUES (:Titre_Spectacle,:Prix_Spectacle)";
        $prep = null;
        try{
            $prep=$this->db->prepare($query);
            $prep->bindValue(":Titre_Spectacle",$titre,PDO::PARAM_STR);
            $prep->bindValue(":Prix_Spectacle",$prix,PDO::PARAM_STR);
            $prep->execute();
            $spectacleID=$this->db->lastInsertId();
        }catch (Exception $e){
            die($e->getMessage());

        }finally{
            $prep->closeCursor();
        }
        return $spectacleID;
    }
    /**
     * @return Spectacles[]
     */
    /**
     * Sélection de tous les spectacles triés par leurs titres
     */
    function  selectAllSpectacle(){
    $query = "Select * from spectacle ORDER BY Titre_Spectacle";
    $prep = null;
    $spectacles = array();
    try{
        $prep = $this->db->prepare($query);
        $prep->execute();
        $arr = $prep->fetchAll();
        foreach ($arr as $specArr){
            $spectacle= new Spectacle($specArr);
            array_push($spectacles,$spectacle);
        }
    }catch (PDOException $e){
        die($e->getMessage());
    }finally{
        $prep->closeCursor();
    }
    return $spectacles;
}
    /**
     * Sélection du nom d'un spectacle en fonction de son ID
     */
    function selectByIdAndReturnTitle($id){
        $query = "Select * from spectacle WHERE ID_Spectacle=:ID_Spectacle";
        $prep = null;
        $spectateurs = array();
        try{
            $prep = $this->db->prepare($query);
            $prep->bindValue(":ID_Spectacle", $id, PDO::PARAM_INT);
            $prep->execute();
            $arr = $prep->fetch();
            if ($arr) {
                $spectacle = new Spectacle($arr);
            }
        }catch (PDOException $e){
            die($e->getMessage());
        }finally{
            $prep->closeCursor();
        }
        return $spectacle->getTitre_Spectacle();
    }
    /**
     * Sélection du prix d'un spectacle en fonction de son ID
     */
    function selectByIdAndReturnPrix($id){
        $query = "Select * from spectacle WHERE ID_Spectacle=:ID_Spectacle";
        $prep = null;
        $spect = array();
        try{
            $prep = $this->db->prepare($query);
            $prep->bindValue(":ID_Spectacle", $id, PDO::PARAM_INT);
            $prep->execute();
            $arr = $prep->fetch();
            if ($arr) {
                $spect = new Spectacle($arr);
            }
        }catch (PDOException $e){
            die($e->getMessage());
        }finally{
            $prep->closeCursor();
        }
        return $spect->getPrix_Spectacle();
    }

}