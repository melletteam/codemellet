<?php
if(!isset($_COOKIE['ID_login'])){
echo'<!DOCTYPE HTML>
<html>
    <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Théatre Mellet</title>
    <link rel="icon" href="images/favicon.ico" />
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Animate.css -->
    <link rel="stylesheet" href="css/animate.css">
    <!-- Icomoon Icon Fonts-->
    <link rel="stylesheet" href="css/icomoon.css">
    <!-- Bootstrap  -->
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/login.css">
    <!-- Magnific Popup -->
    <link rel="stylesheet" href="css/magnific-popup.css">
    <!-- Owl Carousel  -->
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">
    <!-- Theme style  -->
    <link rel="stylesheet" href="css/style.css">
    <!-- Modernizr JS -->
    <script src="js/modernizr-2.6.2.min.js"></script>
    <link rel="stylesheet" href="../css/bootstrap.min.import.css">
    <script src="../js/jquery.min.import.js"></script>
    <script src="../js/bootstrap.min.import.js"></script>
    </head>
    <body>';
    /**
     * Contrôle de connexion pour l'utilisateur
     */
    if(isset($_GET["connection"])) {
        if ($_GET["connection"] == "0")
            echo '<div class="alert alert-success" role="alert">Login ou Mot de passe incorrect.</div>';
    }
    /**
     * Affichage Page Accueil
     */
    echo'<div class="gtco-loader"></div>
    <div id="page">
        <nav class="gtco-nav" role="navigation">
            <div class="gtco-container">
                <div class="row">
                    <div class="col-xs-2">
                        <div id="gtco-logo"><img src="images/melletCalque.png" width="30 height="30">
                            <a href="#">Mellet.</a></div>
                    </div>
                </div>
            </div>
        </nav>
        <header id="gtco-header" class="gtco-cover gtco-cover-sm" role="banner" style="background-image:url(images/img_bg_1.jpg);height: 100px">
            <div class="gtco-container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2 text-center">
                        <div class="display-t">
                            <div class="display-tc animate-box" data-animate-effect="fadeIn">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <div class="gtco-section">
            <div class="gtco-container">
                <div class="row animate-box">
                <div class="loginmodal-container">
					<h1>Connectez vous</h1><br>
				  <form method="post" action="./Services/Connection.php">
					<input type="text" name="user" placeholder="Login" required>
					<input type="password" name="pass" placeholder="Mot de passe" required>
					<input type="submit" name="login" class="login loginmodal-submit" value="Login">
				  </form>
				</div>
                </div>
            </div>
        </div>
    </div>
        <footer id="gtco-footer" role="contentinfo" style="background-color: #1b6d85;height: 100px">
            <div class="gtco-container">
                <div class="row copyright">
                    <div class="col-md-12">
                        <p class="pull-left">
                            <small class="block">&copy; 2016 Free HTML5. All Rights Reserved.</small>
                        </p>
                    </div>
                </div>
            </div>
        </footer>
    </div>
    <div class="gototop js-top">
        <a href="#" class="js-gotop"><i class="icon-arrow-up"></i></a>
    </div>
    <!-- jQuery -->
    <script src="js/jquery.min.js"></script>
    <!-- jQuery Easing -->
    <script src="js/jquery.easing.1.3.js"></script>
    <!-- Bootstrap -->
    <script src="js/bootstrap.min.js"></script>
    <!-- Waypoints -->
    <script src="js/jquery.waypoints.min.js"></script>
    <!-- Carousel -->
    <script src="js/owl.carousel.min.js"></script>
    <!-- countTo -->
    <script src="js/jquery.countTo.js"></script>
    <!-- Magnific Popup -->
    <script src="js/jquery.magnific-popup.min.js"></script>
    <script src="js/magnific-popup-options.js"></script>
    <!-- Main -->
    <script src="js/main.js"></script>
    </body>
</html>';
}else header("Location: ./view/reservation.php");