/**
 * Fonction qui affiche dans un champ les numéros de sièges séléctionnés,
 * qui change la couleur du siège séléctionné
 * et qui enlève le numéro de siège mais aussi remet en disponible la place déselectionnée.
 */
function clickPlace() {
    var arrayTest = document.getElementsByClassName('buttonTableImage');
    var place =document.getElementById('place');
    var i;
    for(i=0;i<arrayTest.length;i++){
        arrayTest[i].onclick=function () {
            if(place.value.indexOf(this.id)>=0){
                place.value=place.value.replace(this.id+',','').replace(this.id,'');
                document.getElementById(this.id).style.background ="url('../images/Disponible.png') no-repeat center";
            }else{
                if(place.value == ""){
                    place.value = this.id;
                }else{
                    place.value =place.value +',' +this.id;
                }
                document.getElementById(this.id).style.background ="url('../images/Select.png') no-repeat center";
            }
        };
    }
}