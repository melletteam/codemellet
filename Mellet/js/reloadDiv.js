$(document).ready(function(){
    //PAGE SPECTACLE
    $("#spectacle").change(function(){
        var valeur=document.getElementById("spectacle").value;
        var xmlhttp;
        if (valeur == "") {
            document.getElementById("txtHint").innerHTML = "<h2><strong>veuillez sélectionner un spectacle</strong></h2>";
            return;
        }else {
            if (window.XMLHttpRequest) {
                xmlhttp = new XMLHttpRequest();
            } else {
                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
            }
            xmlhttp.onreadystatechange = function() {
                if (this.readyState == 4 && this.status == 200) {
                    document.getElementById("txtHint").innerHTML = this.responseText;
                }
            };
            xmlhttp.open("GET","../controller/ControllerSpectacle.php?q="+valeur,true);
            xmlhttp.send();
        }
    });
    //page SALLE
    $("#salle").change(function(){
        refreshSalle()
    });
    //PAGE RESERVATION
    $("#reservation").change(function(){
        refreshReser();
    });


});
function refreshSalle(){
    var valeur=document.getElementById("salle").value;
    var xmlhttp;
    if (valeur == "") {
        document.getElementById("txtHint").innerHTML = "<h2><strong>veuillez sélectionner une représentation</strong></h2>";
        return;
    }else {
        if (window.XMLHttpRequest) {
            xmlhttp = new XMLHttpRequest();
        } else {
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                if (this.responseText != null) {
                    document.getElementById("txtHint").innerHTML = this.responseText;
                        document.getElementById('gererSpect').onclick = AjoutPlaceNonDispo;
                        clickPlace();
                    }
                }
        };
        xmlhttp.open("GET","../controller/ControllerSalle.php?q="+valeur,true);
        xmlhttp.send();
    }
}
function refreshReser() {
    var valeur=document.getElementById("reservation").value;
    var xmlhttp;
    if (valeur == "") {
        document.getElementById("txtHint").innerHTML = "<h2><strong>veuillez sélectionner une représentation</strong></h2>";
        return;
    }else {
        if (window.XMLHttpRequest) {
            xmlhttp = new XMLHttpRequest();
        } else {
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById("txtHint").innerHTML = this.responseText;
                document.getElementById('ValidReser').onclick=AjoutReservation;
                document.getElementById('SupprReser').onclick=SupprimerReservation;
                clickPlace();
            }
        };
        xmlhttp.open("GET","../controller/ControllerReservation.php?q="+valeur,true);
        xmlhttp.send();
    }
}
function SupprimerReservation() {
    if (window.XMLHttpRequest) {
        xmlhttp = new XMLHttpRequest();
    } else {
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            refreshReser();
        }
    };
    var param1 ="";
    param1+="idRepSup="+document.getElementById("idRepSup").value;
    param1+="&IDRes="+document.getElementById("IDRes").value;
    xmlhttp.open("POST","../Services/SupprimerReservation.php",true);
    xmlhttp.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
    xmlhttp.send(param1);
}
function AjoutReservation() {
    if (window.XMLHttpRequest) {
        xmlhttp = new XMLHttpRequest();
    } else {
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            refreshReser();
        }
    };
    var param1 ="";
    param1+="idRep="+document.getElementById("idRep").value;
    param1+="&idSpect="+document.getElementById("idSpect").value;
    param1+="&place="+document.getElementById("place").value;
    param1+="&typeReservation="+document.getElementById("typeReservation").value;
    xmlhttp.open("POST","../Services/AjoutReservation.php",true);
    xmlhttp.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
    xmlhttp.send(param1);
}
function AjoutPlaceNonDispo(){
    if (window.XMLHttpRequest) {
        xmlhttp = new XMLHttpRequest();
    } else {
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            refreshSalle();
        }
    };
    var param1 ="";
    param1+="place="+document.getElementById("place").value;
    param1+="&idrep="+document.getElementById("idrep").value;
    xmlhttp.open("POST","../Services/GererSalle.php",true);
    xmlhttp.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
    xmlhttp.send(param1);
}
