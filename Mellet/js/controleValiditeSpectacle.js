/**
 * Fonctions qui s'occupent de vérifier la validité des champs du formulaire spectacle
 */
function ajouterControleur(i) {
    var elementColonne = document.getElementById("nbCRepresentation" + i);
    var elementLigne = document.getElementById("nbLRepresentation" + i);
    elementColonne.onchange = function () {
        var valueCo = parseInt(elementColonne.value);
        if (valueCo <= 0 || valueCo >= 21) {
            document.getElementById("validNouveauSpect").disabled = true;
            elementColonne.style.backgroundColor = "#FA5858";
        } else {
            document.getElementById("validNouveauSpect").disabled = false;
            elementColonne.style.backgroundColor = "#58FA58";
        }
    };
        elementLigne.onchange = function () {
            var valueLi = parseInt(elementLigne.value);
            if (valueLi <= 0 || valueLi >= 21) {
                document.getElementById("validNouveauSpect").disabled = true;
                elementLigne.style.backgroundColor = "#FA5858";
            } else {
                document.getElementById("validNouveauSpect").disabled = false;
                elementLigne.style.backgroundColor = "#58FA58";
            }
        };
    }
    function ajouterControleurAdd(i) {
        var elementColonne = document.getElementById("nbColoRepresentation" + i);
        var elementLigne = document.getElementById("nbLignRepresentation" + i);
        elementColonne.onchange = function () {
            var valueCo = parseInt(elementColonne.value);
            if (valueCo <= 0 || valueCo >= 21) {
                document.getElementById("validRepresentationAdd").disabled = true;
                elementColonne.style.backgroundColor = "#FA5858";
            } else {
                document.getElementById("validRepresentationAdd").disabled = false;
                elementColonne.style.backgroundColor = "#58FA58";
            }
        };
        elementLigne.onchange = function () {
            var valueLi = parseInt(elementLigne.value);
            if (valueLi <= 0 ||valueLi >= 21) {
                document.getElementById("validRepresentationAdd").disabled = true;
                elementLigne.style.backgroundColor = "#FA5858";
            } else {
                document.getElementById("validRepresentationAdd").disabled = false;
                elementLigne.style.backgroundColor = "#58FA58";
            }
        };
    }