/**
 * Fonction qui s'occupe de déactiver les fonctionnalité du site si l'utilisateur est un simple User.
 */
$(document).ready(function(){
    var statut=getCookie("Statut_login");
    if(statut!=null){
        if(statut=="u"){
            //SPECTACLE
            if(document.getElementById("newSpectacle")!=null)
                $("#newSpectacle").attr("disabled","disabled");
            if(document.getElementById("newRepresentation")!=null)
                $("#newRepresentation").attr("disabled","disabled");
            //ACTEURS
            if(document.getElementById("nouveauActeur")!=null)
                $("#nouveauActeur").attr("disabled","disabled");
            //ADMINISTRATION
            if(document.getElementById("ajoutUser")!=null)
                $("#ajoutUser").attr("disabled","disabled");
            if(document.getElementById("suppressionSpectacle")!=null)
                $("#suppressionSpectacle").attr("disabled","disabled");
            if(document.getElementById("backup")!=null)
                $("#backup").attr("disabled","disabled");
            for(var i=0;i<document.getElementById("NbUser").value;i++){
                if(document.getElementById("modifierUser"+i)!=null)
                    $("#modifierUser"+i).attr("disabled","disabled");
                if(document.getElementById("supprimerUser"+i)!=null)
                    $("#supprimerUser"+i).attr("disabled","disabled");
            }
        }
    }

});
function getCookie(nomCookie) {
    deb = document.cookie.indexOf(nomCookie+ "=");
    if (deb >= 0) {
        deb += nomCookie.length + 1;
        fin = document.cookie.indexOf(";",deb);
        if (fin < 0) fin = document.cookie.length;
        return unescape(document.cookie.substring(deb,fin));
    }else return null;
}