/**
 * Fonction qui s'occupe d'ajouter et d'afficher des champs pour la création de spectacle et de représentation.
 */

window.onload=init;
var nbRepresentation=0;
var nbRepresentationAdd=0;
var nbActeur=0;
var nbMise=0;
var nbRegie=0;
var nbDecor=0;
var nbAide=0;
var nbPresentation=0;
var nbMaquillage=0;
function init() {

    var acteurs=document.getElementById("arrayActor").innerHTML;
    var scenes=document.getElementById("arrayScene").innerHTML;
    var regies=document.getElementById("arrayRegie").innerHTML;
    var decors=document.getElementById("arrayDecor").innerHTML;
    var aides=document.getElementById("arrayAide").innerHTML;
    var presentations=document.getElementById("arrayPresentation").innerHTML;
    var maquillages=document.getElementById("arrayMaquillage").innerHTML;
    //init du nb de champ de chaque divField
    document.getElementById('divFieldsActeur').innerHTML +="<input name='nbActeur' id='nbActeur' type='hidden' value="+nbActeur+">";
    document.getElementById('divFieldsMisescene').innerHTML +="<input name='nbMise' id='nbMise' type='hidden' value="+nbMise+">";
    document.getElementById('divFieldsRegie').innerHTML +="<input name='nbRegie' id='nbRegie' type='hidden' value="+nbRegie+">";
    document.getElementById('divFieldsDecor').innerHTML +="<input name='nbDecor' id='nbDecor' type='hidden' value="+nbDecor+">";
    document.getElementById('divFieldsAide').innerHTML +="<input name='nbAide' id='nbAide' type='hidden' value="+nbAide+">";
    document.getElementById('divFieldsPresentation').innerHTML +="<input name='nbPresentation' id='nbPresentation' type='hidden' value="+nbPresentation+">";
    document.getElementById('divFieldsMaquillage').innerHTML +="<input name='nbMaquillage' id='nbMaquillage' type='hidden' value="+nbMaquillage+">";
    //init du nb de champ de chaque divField

    //REPRESENTATION
    document.getElementById("buttonPlusRepresentation").onmouseover = function () { document.getElementById("buttonPlusRepresentation").src="../images/plus_valid.png" };
    document.getElementById("buttonPlusRepresentation").onmouseleave = function () { document.getElementById("buttonPlusRepresentation").src="../images/plus.png" };
    document.getElementById("buttonPlusRepresentation").onclick = function () {
        document.getElementById('validNouveauSpect').disabled = true;
        var elDiv = document.createElement("div");
        elDiv.setAttribute("class","form-group");
        elDiv.innerHTML = "<div class='form-group'><label >Représentation"+(nbRepresentation+1)+"</label>" +
            "<br><div class='form-group col-md-5'><input class='form-control' type='datetime-local' name="+'datepicker'+ nbRepresentation +" placeholder='Date'></div>" +
            "<div class='form-group col-md-4'><input class='form-control' type='text' name="+'nbColonneRepresentation'+ nbRepresentation +" id="+'nbCRepresentation'+ nbRepresentation +" placeholder='Nombre colonne' required ></div>" +
            "<div class='form-group col-md-3'><input class='form-control' type='text' name="+'nbLigneRepresentation'+ nbRepresentation +" id="+'nbLRepresentation'+ nbRepresentation +" placeholder='Nombre ligne' required ></div></div>";
        nbRepresentation++;
        document.getElementById('divFieldsRepresentation').appendChild(elDiv);
        document.getElementById('nbRepresentation').value = nbRepresentation;
        ajouterControleur((nbRepresentation-1));};

    //ACTEUR
    document.getElementById("buttonPlusActeur").onmouseover = function () { document.getElementById("buttonPlusActeur").src="../images/plus_valid.png" };
    document.getElementById("buttonPlusActeur").onmouseleave = function () { document.getElementById("buttonPlusActeur").src="../images/plus.png" };
    document.getElementById("buttonPlusActeur").onclick = function () {
        var elDiv = document.createElement("div");
        elDiv.setAttribute("class","form-group");
        elDiv.innerHTML = "<label >Acteur" + (nbActeur + 1) + "</label>" +
            "<br><div class='form-row'>" +
            "<div class='form-group col-md-6'>" +
            "<SELECT class='form-control' name=" + 'acteur' + nbActeur + " size='1' required>"+acteurs+
            "</SELECT>" +
            "</div>" +
            "<div class='form-group col-md-6'><input type='text' class='form-control' value='' name=" + 'roleActeur' + nbActeur + " placeholder='role'></div>" +
            "</div>";
        nbActeur++;
        document.getElementById('divFieldsActeur').appendChild(elDiv);
        document.getElementById('nbActeur').value = nbActeur;};

    //Mise en scene
    document.getElementById("buttonPlusMisescene").onmouseover = function () { document.getElementById("buttonPlusMisescene").src="../images/plus_valid.png" };
    document.getElementById("buttonPlusMisescene").onmouseleave = function () { document.getElementById("buttonPlusMisescene").src="../images/plus.png" };
    document.getElementById("buttonPlusMisescene").onclick = function () {
        var elDiv = document.createElement("div");
        elDiv.setAttribute("class","form-group");
        elDiv.innerHTML = "<div class='form-group'><label >Mise en scène"+(nbMise+1)+"</label>" +
            "<br><div class='form-row'>" +
            "<div class='form-group col-md-6'>" +
            "<SELECT class='form-control' name="+'mise'+ nbMise +" size='1' required>"+scenes+
            "</SELECT>" +
            "</div>" +
            "<div class='form-group col-md-6'><input type='text' class='form-control' name="+'roleMise'+ nbMise +" placeholder='role'></div>" +
            "</div></div>";
        nbMise++;
        document.getElementById('divFieldsMisescene').appendChild(elDiv);
        document.getElementById('nbMise').value = nbMise;};

    //Regie
    document.getElementById("buttonPlusRegie").onmouseover = function () { document.getElementById("buttonPlusRegie").src="../images/plus_valid.png" };
    document.getElementById("buttonPlusRegie").onmouseleave = function () { document.getElementById("buttonPlusRegie").src="../images/plus.png" };
    document.getElementById("buttonPlusRegie").onclick = function () {
        var elDiv = document.createElement("div");
        elDiv.setAttribute("class","form-group");
        elDiv.innerHTML = "<div class='form-group'><label >Régie"+(nbRegie+1)+"</label>" +
            "<br><div class='form-row'>" +
            "<div class='form-group col-md-6'>" +
            "<SELECT class='form-control' name="+'regie'+ nbRegie +" size='1' required>"+regies+
            "</SELECT></div>" +
            "<div class='form-group col-md-6'><input type='text' class='form-control' name="+'roleRegie'+ nbRegie +" placeholder='role'></div>" +
            "</div></div>";
        nbRegie++;
        document.getElementById('divFieldsRegie').appendChild(elDiv);
        document.getElementById('nbRegie').value = nbRegie;};

    //Decor
    document.getElementById("buttonPlusDecor").onmouseover = function () { document.getElementById("buttonPlusDecor").src="../images/plus_valid.png" };
    document.getElementById("buttonPlusDecor").onmouseleave = function () { document.getElementById("buttonPlusDecor").src="../images/plus.png" };
    document.getElementById("buttonPlusDecor").onclick = function () {
        var elDiv = document.createElement("div");
        elDiv.setAttribute("class","form-group");
        elDiv.innerHTML = "<div class='form-group'><label >Décor"+(nbDecor+1)+"</label>" +
            "<br><div class='form-row'>" +
            "<div class='form-group col-md-6'>" +
            "<SELECT class='form-control' name="+'decor'+ nbDecor +" size='1' required>"+decors+
            "</SELECT></div>" +
            "<div class='form-group col-md-6'><input type='text' class='form-control' name="+'roleDecor'+ nbDecor +" placeholder='role'></div>" +
            "</div></div>";
        nbDecor++;
        document.getElementById('divFieldsDecor').appendChild(elDiv);
        document.getElementById('nbDecor').value = nbDecor;};
    //Aide
    document.getElementById("buttonPlusAide").onmouseover = function () { document.getElementById("buttonPlusAide").src="../images/plus_valid.png" };
    document.getElementById("buttonPlusAide").onmouseleave = function () { document.getElementById("buttonPlusAide").src="../images/plus.png" };
    document.getElementById("buttonPlusAide").onclick = function () {
        var elDiv = document.createElement("div");
        elDiv.setAttribute("class","form-group");
        elDiv.innerHTML = "<div class='form-group'><label >Aide"+(nbAide+1)+"</label>" +
            "<br><div class='form-row'>" +
            "<div class='form-group col-md-6'>" +
            "<SELECT class='form-control' name="+'aide'+ nbAide +" size='1' required>"+aides+
            "</SELECT></div>" +
            "<div class='form-group col-md-6'><input type='text' class='form-control' name="+'roleAide'+ nbAide +" placeholder='role'></div>" +
            "</div></div>";
        nbAide++;
        document.getElementById('divFieldsAide').appendChild(elDiv);
        document.getElementById('nbAide').value = nbAide;};
    //Presentation
    document.getElementById("buttonPlusPresentation").onmouseover = function () { document.getElementById("buttonPlusPresentation").src="../images/plus_valid.png" };
    document.getElementById("buttonPlusPresentation").onmouseleave = function () { document.getElementById("buttonPlusPresentation").src="../images/plus.png" };
    document.getElementById("buttonPlusPresentation").onclick = function () {
        var elDiv = document.createElement("div");
        elDiv.setAttribute("class","form-group");
        elDiv.innerHTML = "<div class='form-group'><label >Presentation"+(nbPresentation+1)+"</label>" +
            "<br><div class='form-row'>" +
            "<div class='form-group col-md-6'>" +
            "<SELECT class='form-control' name="+'presentation'+ nbPresentation +" size='1' required>"+presentations+
            "</SELECT></div>" +
            "<div class='form-group col-md-6'><input type='text' class='form-control' name="+'rolePresentation'+ nbPresentation +" placeholder='role'></div>" +
            "</div></div>";
        nbPresentation++;
        document.getElementById('divFieldsPresentation').appendChild(elDiv);
        document.getElementById('nbPresentation').value = nbPresentation;};
    //Maquillage
    document.getElementById("buttonPlusMaquillage").onmouseover = function () { document.getElementById("buttonPlusMaquillage").src="../images/plus_valid.png" };
    document.getElementById("buttonPlusMaquillage").onmouseleave = function () { document.getElementById("buttonPlusMaquillage").src="../images/plus.png" };
    document.getElementById("buttonPlusMaquillage").onclick = function () {
        var elDiv = document.createElement("div");
        elDiv.setAttribute("class","form-group");
        elDiv.innerHTML = "<div class='form-group'><label >Maquillage"+(nbMaquillage+1)+"</label>" +
            "<br><div class='form-row'>" +
            "<div class='form-group col-md-6'>" +
            "<SELECT class='form-control' name="+'maquillage'+ nbMaquillage +" size='1' required>"+maquillages+
            "</SELECT></div>" +
            "<div class='form-group col-md-6'><input type='text' class='form-control' name="+'roleMaquillage'+ nbMaquillage +" placeholder='role'></div>" +
            "</div></div>";
        nbMaquillage++;
        document.getElementById('divFieldsMaquillage').appendChild(elDiv);
        document.getElementById('nbMaquillage').value = nbMaquillage;};
    //AddREPRESENTATION
    document.getElementById("buttonPlusAddRepresentation").onmouseover = function () { document.getElementById("buttonPlusAddRepresentation").src="../images/plus_valid.png" };
    document.getElementById("buttonPlusAddRepresentation").onmouseleave = function () { document.getElementById("buttonPlusAddRepresentation").src="../images/plus.png" };
    document.getElementById("buttonPlusAddRepresentation").onclick = function () {
        document.getElementById('validRepresentationAdd').disabled = true;
        var elDiv = document.createElement("div");
        elDiv.setAttribute("class","form-group");
        elDiv.innerHTML = "<div class='form-group'><label >Représentation"+(nbRepresentationAdd+1)+"</label>" +
            "<br><div class='form-group col-md-5'><input class='form-control' type='datetime-local' name="+'datepicker'+ nbRepresentationAdd +" placeholder='Date'></div>" +
            "<div class='form-group col-md-4'><input class='form-control' type='text' name="+'nbColonneRepresentation'+ nbRepresentationAdd +" id="+'nbColoRepresentation'+ nbRepresentationAdd +" placeholder='Nombre colonne'></div>" +
            "<div class='form-group col-md-3'><input class='form-control' type='text' name="+'nbLigneRepresentation'+ nbRepresentationAdd +" id="+'nbLignRepresentation'+ nbRepresentationAdd +" placeholder='Nombre ligne'></div></div>";
        nbRepresentationAdd++;
        document.getElementById('divFieldsAddRepresentation').appendChild(elDiv);
        document.getElementById('nbRepresentationAdd').value = nbRepresentationAdd;
        ajouterControleurAdd((nbRepresentationAdd-1));};
}