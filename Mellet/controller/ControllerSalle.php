<?php
/**
 * Partie de code qui est utilisé pour la gestion de la salle
 */
require_once '../manager/DBManager.php';
require_once '../manager/representationManager.php';
require_once '../model/representation.php';
require_once '../model/chaise.php';
require_once '../manager/chaiseManager.php';

/**
 * Variables utilisées
 */
$q = intval($_GET['q']);
$dbManager = new DBManager();
$pdo = $dbManager->connect();
$chaiseManager= new chaiseManager($pdo);
$chaises=$chaiseManager->selectAll($q);
$representationManager= new representationManager($pdo);
$nbLignes=$representationManager->selectNbLignes($q);
$nbCol=$representationManager->selectNbCol($q);
$chaiseManager= new chaiseManager($pdo);
$nbCol=$representationManager->selectNbCol($q);
$c=0;
$arrayLettre=array(0=>"A",1=>"B",2=>"C",3=>"D",4=>"E",5=>"F",6=>"G",7=>"H",8=>"I",9=>"J",10=>"K",11=>"L",12=>"M",13=>"N",14=>"O",15=>"P",16=>"Q",17=>"R",18=>"S",19=>"T");
$arrayChiffre=array(0=>"01",1=>"02",2=>"03",3=>"04",4=>"05",5=>"06",6=>"07",7=>"08",8=>"09",9=>"10",10=>"11",11=>"12",12=>"13",13=>"14",14=>"15",15=>"16",16=>"17",17=>"18",18=>"19",19=>"20");
/**
 * Affichage du plan de la salle
 */
echo'<div class="col-md-9"><table class="left-table">';
echo '<th></th>';
for ($i=0;$i<$nbCol;$i++)
    echo "<th>".$arrayChiffre[$i]."</th>";
for ($i=0 ; $i<$nbLignes ; $i++){
    echo "<tr><td>".$arrayLettre[$i]."</td>";
    for ($j=0 ; $j<$nbCol ; $j++){
        $chaise=new chaise($chaises[$c]);
        if($chaise->getStatut_Chaise()=="D"){
            echo "<td><button class=\"buttonTableImage\" type=Button style='background-image: url(\"../images/Disponible.png\")' id=".$arrayLettre[$i].($j+1)." value=\"".$chaise->getStatut_Chaise()."\"></button></td>";
        }
        if($chaise->getStatut_Chaise()=="O"){
            echo "<td><button disabled class=\"buttonTableImage\" type=Button style='background-image: url(\"../images/Occupe.png\")' id=".$arrayLettre[$i].($j+1)." value=\"".$chaise->getStatut_Chaise()."\"></button></td>";
        }
        if($chaise->getStatut_Chaise()=="R"){
            echo "<td><button disabled class=\"buttonTableImage\" type=Button style='background-image: url(\"../images/Reserve.png\")' id=".$arrayLettre[$i].($j+1)." value=\"".$chaise->getStatut_Chaise()."\"></button></td>";
        }
        if($chaise->getStatut_Chaise()=="A"){
            echo "<td><button disabled class=\"buttonTableImage\" type=Button style='background-image: url(\"../images/NonDisponible.png\")' id=".$arrayLettre[$i].($j+1)." value=\"".$chaise->getStatut_Chaise()."\"></button></td>";
        }
        $c++;
    }
    echo '</tr>';
}
echo '<tr><td></td>';
for ($i=0;$i<$nbCol;$i++)
    echo "<td>".$arrayChiffre[$i]."</td>";
echo '</tr>';
/**
 * Affichage fonctions utilisées par la page Salle
 */
echo'</table></div>
<div class="col-md-3">
<h5 class="modal-title">Sélectionner les places non disponibles</h5>
<input class="form-control" type="text" id="place" name="place" readonly >
<div style="display: none;"><input class="form-control" type="text" id="idrep" name="idrep" value='.$q.' ></div>';
if(isset($_COOKIE["Statut_login"])) {
    if ($_COOKIE["Statut_login"] == "u") echo '<input class="btn btn-primary" type="submit" id="gererSpect" name="valideGerer" value="Appliquer les modifications" disabled>';
    else echo '<input class="btn btn-primary" type="submit" id="gererSpect" name="valideGerer" value="Appliquer les modifications">';
}
echo"</div>";

