<?php
/**
 * Partie de code qui est utilisé pour les réservations
 */
require_once '../manager/DBManager.php';
require_once '../manager/representationManager.php';
require_once '../model/representation.php';
require_once '../model/chaise.php';
require_once '../manager/chaiseManager.php';
require_once '../model/spectateur.php';
require_once '../manager/spectateurManager.php';
require_once '../manager/reservationManager.php';
require_once '../model/reservation.php';

/**
 * Variables nécessaires
 */
$q = intval($_GET['q']);
$dbManager = new DBManager();
$pdo = $dbManager->connect();
$representationManager= new representationManager($pdo);
$chaiseManager= new chaiseManager($pdo);
$spectateursManager = new spectateurManager($pdo);
$reservationManager = new reservationManager($pdo);
$chaises=$chaiseManager->selectAll($q);
$nbLignes=$representationManager->selectNbLignes($q);
$nbCol=$representationManager->selectNbCol($q);
$spectateurs = $spectateursManager->selectAll();
$reservations =$reservationManager->selectAllByRepre($q);
$arrayLettre=array(0=>"A",1=>"B",2=>"C",3=>"D",4=>"E",5=>"F",6=>"G",7=>"H",8=>"I",9=>"J",10=>"K",11=>"L",12=>"M",13=>"N",14=>"O",15=>"P",16=>"Q",17=>"R",18=>"S",19=>"T");
$arrayChiffre=array(0=>"01",1=>"02",2=>"03",3=>"04",4=>"05",5=>"06",6=>"07",7=>"08",8=>"09",9=>"10",10=>"11",11=>"12",12=>"13",13=>"14",14=>"15",15=>"16",16=>"17",17=>"18",18=>"19",19=>"20");
$c=0;
/**
 * Affichage du plan de salle
 */
echo'<div class="col-md-8"><table class="left-table">';
echo '<th></th>';
for ($i=0;$i<$nbCol;$i++)
    echo "<th>".$arrayChiffre[$i]."</th>";
for ($i=0 ; $i<$nbLignes ; $i++){
    echo "<tr><td>".$arrayLettre[$i]."</td>";
    for ($j=0 ; $j<$nbCol ; $j++){
        $chaise=new chaise($chaises[$c]);
        if($chaise->getStatut_Chaise()=="D"){
            echo "<td><button class=\"buttonTableImage\" type=Button style='background-image: url(\"../images/Disponible.png\")' id=".$arrayLettre[$i].($j+1)." value=\"".$chaise->getStatut_Chaise()."\"></button></td>";
        }
        if($chaise->getStatut_Chaise()=="O"){
            echo "<td><button disabled class=\"buttonTableImage\" type=Button style='background-image: url(\"../images/Occupe.png\")' id=".$arrayLettre[$i].($j+1)." value=\"".$chaise->getStatut_Chaise()."\"></button></td>";
        }
        if($chaise->getStatut_Chaise()=="R"){
            echo "<td><button disabled class=\"buttonTableImage\" type=Button style='background-image: url(\"../images/Reserve.png\")' id=".$arrayLettre[$i].($j+1)." value=\"".$chaise->getStatut_Chaise()."\"></button></td>";
        }
        if($chaise->getStatut_Chaise()=="A"){
            echo "<td><button disabled class=\"buttonTableImage\" type=Button style='background-image: url(\"../images/NonDisponible.png\")' id=".$arrayLettre[$i].($j+1)." value=\"".$chaise->getStatut_Chaise()."\"></button></td>";
        }
        $c++;
    }
    echo '</tr>';
}
echo '<tr><td></td>';
for ($i=0;$i<$nbCol;$i++)
    echo "<td>".$arrayChiffre[$i]."</td>";
echo '</tr>';
echo'</table></div>';
/**
 * Affichage des fonctionnalités liées à une réservation
 */
echo'<div class="col-md-4">
        <fieldset>
            <div class="form-group">
                <div class="form-group">
                    <br>
                    <input id="idRep" name="idRep" type="hidden" value="'.$q.'">
                    <label for="nom">Nom</label><br>
                     <SELECT class="form-control" name="idSpect" id="idSpect" size="1" required>
                                <option value="">Sélectionner un spectateur:</option>';
foreach ($spectateurs as $spectateur) {
    $spec=$spectateur->getNom_Spectateur()." ".$spectateur->getPrenom_Spectateur()." de ".$spectateur->getVille_Spectateur();
    echo '<option value='. $spectateur->getID_Spectateur() . '>' .$spec. '</option>';
}
echo'
                    </SELECT>
                    <label for="place">Place(s)</label><br>
                    <input class="form-control" type="text" id="place" name="place" readonly>
                    <label for="typeReservation">Type de réservation</label>                 
                    <SELECT class="form-control" id="typeReservation" name="typeReservation" size="1" required>
                        <OPTION value="R">Réservé
                        <OPTION value="O">Occupé
                    </SELECT>
                    <br>
                    <input class="btn btn-primary" type="submit" id="ValidReser" name="valideReservation" value="Valider">
                </div>
        </fieldset>
    <label for="place">Supprimer  une réservation ?</label><br>
    <button type="button" class="btn btn-primary" data-toggle="modal" data-backdrop="false" data-target="#supprimerReservation">Supprimer</button><br>
    <label for="place">Ajouter un nouveau spectateur ?</label><br>
    <button type="button" class="btn btn-primary" data-toggle="modal" data-backdrop="false" onclick=window.location.href=\'spectateurs.php\';>Ajouter</button>
                        
</div>
                        <!-- Modal supprimer resevation -->
<div class="modal fade" id="supprimerReservation" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Supprimer une réservation</h4>
            </div>
            <div class="modal-body">
                    <fieldset>
                    <input id="idRepSup" name="idRepSup" type="hidden" value="'.$q.'">
                        <div class="form-group">
                            <label for="Reservation">Réservation :</label>';
$idspec=null;           echo'<SELECT id="IDRes" class="form-control" NAME="IDRes" size="1" required >';
foreach ($reservations as $reservation) {
    for($i=0;$i<sizeof($spectateurs);$i++){
        if($spectateurs[$i]->getID_Spectateur()==$reservation->getID_Spectateur()){
            if($idspec !==$reservation->getID_Spectateur()){
                $idspec=$spectateurs[$i]->getID_Spectateur();

                echo'<OPTION value='.$reservation->getID_Reservation().'>'. $reservation->getNbPlaces_Reser()." place(s) pour ".$spectateurs[$i]->getNom_Spectateur()." ".$spectateurs[$i]->getPrenom_Spectateur()." , ".$spectateurs[$i]->getVille_Spectateur().'</OPTION>';

            }
        }
    }
}echo'</SELECT>
                        </div>                            
                        <input type="submit" class="btn btn-primary" id="SupprReser" name="SupprReser" value="valider">
                    </fieldset>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- Fin Modal supprimer reservation-->';
?>