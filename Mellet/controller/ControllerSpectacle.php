<?php

/**
 * Partie de code qui est utilisé pour les spectacles
 */
require_once '../manager/DBManager.php';
require_once '../manager/spectacleManager.php';
require_once '../manager/representationManager.php';
require_once '../manager/AssurerManager.php';
require_once '../manager/ActeursManager.php';
require_once '../model/Acteurs.php';
require_once '../model/spectacle.php';
require_once '../model/assurer.php';
require_once '../model/representation.php';

/**
 * Variables utilisées
 */
$q = intval($_GET['q']);
$dbManager = new DBManager();
$pdo = $dbManager->connect();
$spectacleManager= new spectacleManager($pdo);
$assurerManager= new assurerManager($pdo);
$acteursManager= new acteursManager($pdo);
$representationManager= new representationManager($pdo);
$regisseur=null;
$decor=null;
$aide=null;
$presentation=null;
$maquillage=null;
$date=null;
$cpt=0;
$acteurs=null;
$cptact=0;
$assures=$assurerManager->selectByIDSpectacle($q);
$title=$spectacleManager->selectByIdAndReturnTitle($q);
$representations=$representationManager->selectByIDSpectacle($q);
$prix=$spectacleManager->selectByIdAndReturnPrix($q);

/**
 * Affichage des informations du spectacle choisi par l'utilisateur
 */
echo'<div class="row animate-box">
                    <div class="col-md-8 col-md-offset-2 text-center gtco-heading gtco-heading-sm">
                        <h2>'.$title.'</h2>
                    </div>
                </div>';
echo'<div class="row animate-box">
                    <div class="col-lg-10">
                        <p>
                            <strong>Distribution :</strong> <br>';
foreach ($assures as $assure){
    $acteur=$acteursManager->getActeur($assure->getID_Acteur());
    if($acteur->getStatut_Acteurs()=="acteur"){
        if(empty($acteurs)){
            $acteurs=$assure->getRole_Acteur().' : '.$acteur->getNom_Acteur().' '.$acteur->getPrenom_Acteur();

        }else{
            $acteurs = $acteurs .' , '.$assure->getRole_Acteur().' : '.$acteur->getNom_Acteur().' '.$acteur->getPrenom_Acteur();

        }
    }
} echo'<p>'.$acteurs.'</p>';
echo' </p><p>
                            <strong>Mise en scène :</strong> <br>';
foreach ($assures as $assure){
    $acteur=$acteursManager->getActeur($assure->getID_Acteur());
    if($acteur->getStatut_Acteurs()=="scene") {
        echo '<p>' . $acteur->getNom_Acteur() . ' ' . $acteur->getPrenom_Acteur() . '</p>';
    }}
echo' </p><p>
                            <strong>Régisseur :</strong> <br>';
foreach ($assures as $assure){
    $acteur=$acteursManager->getActeur($assure->getID_Acteur());
    if($acteur->getStatut_Acteurs()=="regie") {
        if($regisseur == null){
            $regisseur = $acteur->getNom_Acteur() . ' ' . $acteur->getPrenom_Acteur()  ;
        }elseif (!empty($regisseur) ){
            $regisseur =$regisseur .' , '. $acteur->getNom_Acteur() . ' ' . $acteur->getPrenom_Acteur()  ;
        }
    }}echo '<p>' .$regisseur. '</p>';
echo' </p><p>
                            <strong>Décor :</strong> <br>';
foreach ($assures as $assure){
    $acteur=$acteursManager->getActeur($assure->getID_Acteur());
    if($acteur->getStatut_Acteurs()=="decor") {
        if($decor == null){
            $decor = $acteur->getNom_Acteur() . ' ' . $acteur->getPrenom_Acteur()  ;
        }elseif (!empty($decor) ){
            $decor =$decor .' , '. $acteur->getNom_Acteur() . ' ' . $acteur->getPrenom_Acteur()  ;
        }
    }}echo '<p>' .$decor. '</p>';
echo' </p><p>
                            <strong>Aide mémoire :</strong> <br>';
foreach ($assures as $assure){
    $acteur=$acteursManager->getActeur($assure->getID_Acteur());
    if($acteur->getStatut_Acteurs()=="aide") {
        if($aide == null){
            $aide = $acteur->getNom_Acteur() . ' ' . $acteur->getPrenom_Acteur()  ;
        }elseif (!empty($aide) ){
            $aide =$aide .' , '. $acteur->getNom_Acteur() . ' ' . $acteur->getPrenom_Acteur()  ;
        }
    }}echo '<p>' .$aide. '</p>';
echo' </p><p>
                            <strong>Présentation du spectacle :</strong> <br>';
foreach ($assures as $assure){
    $acteur=$acteursManager->getActeur($assure->getID_Acteur());
    if($acteur->getStatut_Acteurs()=="present") {
        if($presentation == null){
            $presentation = $acteur->getNom_Acteur() . ' ' . $acteur->getPrenom_Acteur()  ;
        }elseif (!empty($presentation) ){
            $presentation =$presentation .' , '. $acteur->getNom_Acteur() . ' ' . $acteur->getPrenom_Acteur()  ;
        }
    }}echo '<p>' .$presentation. '</p>';
echo' </p><p>
                            <strong>Maquillage:</strong> <br>';
foreach ($assures as $assure){
    $acteur=$acteursManager->getActeur($assure->getID_Acteur());
    if($acteur->getStatut_Acteurs()=="maqui") {
        if($maquillage == null){
            $maquillage = $acteur->getNom_Acteur() . ' ' . $acteur->getPrenom_Acteur()  ;
        }elseif (!empty($maquillage) ){
            $maquillage =$presentation .' , '. $acteur->getNom_Acteur() . ' ' . $acteur->getPrenom_Acteur()  ;
        }
    }}echo '<p>' .$maquillage. '</p>';
echo' </p><p>
                            <strong>Date de représentation:</strong><br>';
$nbRep=count($representations);
foreach ($representations as $representation){
    $rep=new representation($representation);
    if(empty($date)){
        $date=$rep->getDate_Representation();
        $cpt++;
    }else{
        $date = $date .' , '.$rep->getDate_Representation();
        $cpt++;
    }
}echo '<p>' .$date. '</p>';
echo'</p>';
echo'<p>
                            <strong>Prix:</strong>'.$prix.' €<br>';
echo'</p>';
echo' </div>';
echo'</div>';
