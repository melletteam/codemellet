<?php
/**
 * Created by IntelliJ IDEA.
 * User: bouffa
 * Date: 06-05-18
 * Time: 10:24
 */
require_once '../manager/DBManager.php';
require_once '../model/acteurs.php';
require_once '../model/chaise.php';
require_once '../model/reservation.php';
require_once '../model/spectateur.php';
require_once '../manager/spectateurManager.php';
require_once '../manager/acteursManager.php';
require_once '../manager/reservationManager.php';
require_once '../manager/chaiseManager.php';

$tests=Array("ajoutActeur" => false,
    "ListerActeur" => false ,
    "AjoutReservation"=> false,
    "ListerReservation" => false,
    "SupprimerReservation"=> false);
$listeErreurs = Array();

$dbManager = new DBManager();
$pdo = $dbManager->connect();
$actManager = new acteursManager($pdo);
$reserManager = new reservationManager($pdo);
$chaiseManager = new chaiseManager($pdo);
$spectateursManager =new spectateurManager($pdo);
$act = null;
$acteurs=null;
$reservations =$reserManager->selectAllByRepre(3);
$spectateurs = $spectateursManager->selectAll();
$chaise =new chaise();
$reservation = new reservation();


/**
 * Test Unitaire sur l'ajout d'acteur
 */
$nom ='TestActNom';
$prenom ='TestActPrenom';
$statut ='present';

if(!empty($nom) && !empty($prenom) && !empty($statut)){
    $act = new acteurs();
    $act->setNom_Acteur($nom);
    $act->setPrenom_Acteur($prenom);
    $act->setStatut_Acteurs($statut);
    //$actManager->save($act);
    $tests["ajoutActeur"] =true;
}else $listeErreurs["ajoutActeur"] = "Erreur dans 1 des champs de l'ajout acteur";


/**
 * Test Unitaire sur l'affichage d'acteur
 */
echo"Test Affichage Acteurs";
$acteurs = $actManager->selectAll();
if($acteurs !=null){

    echo'<br/>';
    echo'<table id="tabHigh" class="table table-bordered" >
                        <thead>
                            <tr>
                                <th>Nom</th>
                                <th>Prenom</th>
                                <th>Statut</th>
                            </tr>
                        </thead>
                        <tbody>';

    foreach ($acteurs as $acteur) {
        echo '
                            <tr>
                                 <td>' . $acteur->getNom_Acteur() . '</td>
                                 <td>' . $acteur->getPrenom_Acteur() . '</td>
                                 <td>' . $acteur->getStatut_Acteurs() . '</td>
                            </tr>
                        </tbody>';
    }
    echo'</table>';
    $tests['ListerActeur']= true;
}else $listeErreurs["ListerActeur"] = "Erreur il est vide";

/**
 * Test Unitaire sur l'ajout de reservation
 */
$Place="A1,A2,A3";
$IDRep="3";
$IdSpect="1";
$TypeR="R";
$reservation=new reservation();
$chaise= new chaise();
if($Place) {
    $idrep = htmlentities($IDRep);
    $idspect = htmlentities($IdSpect);
    $place = htmlentities($Place);
    $typeR = htmlentities($TypeR);
    $places = explode(",", $place);
    //calcule le nombre de chaise(s)
    $nb = count($places);
    //le met dans la variable qui va vers la DB
    $NbPlaces = $nb;
    $reservation->setID_Representation((int)$idrep);
    $reservation->setID_Spectateur((int)$idspect);
    $reservation->setNbPlaces_Reser($NbPlaces);
    $reserID = $reserManager->ajoutReservation($reservation);
    $val[] = null;
    for ($i = 0; $i < $nb; $i++) {
        $val[$i] = str_split($places[$i]);
        //verifie si le chiffre est composée de 2 chiffre
        if (count($val[$i]) == 3) {
            $NLignes = $val[$i][0];
            $NCol = $val[$i][1] . $val[$i][2];
            $chaise->setID_Representation((int)$idrep);
            $chaise->setID_Reservation((int)$reserID);
            $chaise->setStatut_Chaise($typeR);
            $chaise->setNum_Lignes_Chaise($NLignes);
            $chaise->setNum_Col_Chaise((int)$NCol);
        } else {
            $NLignes = $val[$i][0];
            $NCol = $val[$i][1];
            $chaise->setID_Representation((int)$idrep);
            $chaise->setID_Reservation((int)$reserID);
            $chaise->setStatut_Chaise($typeR);
            $chaise->setNum_Lignes_Chaise($NLignes);
            $chaise->setNum_Col_Chaise((int)$NCol);
        }
        $chaiseManager->UpdateChaise($chaise);
    }
    $tests['AjoutReservation']= true;
}else $listeErreurs["AjoutReservation"] = "Erreur il est vide";


/**
 * Test Unitaire sur lister les reservations
 */
echo'<table id="tabHigh" class="table table-bordered" >';
$idspec=null;
if($reservations !=null){
    foreach ($reservations as $reservation) {
        for ($i = 0; $i < sizeof($spectateurs); $i++) {
            if ($spectateurs[$i]->getID_Spectateur() == $reservation->getID_Spectateur()) {
                if ($idspec !== $reservation->getID_Spectateur()) {
                    $idspec = $spectateurs[$i]->getID_Spectateur();

                    echo '<tr>
                                 <td>' . $reservation->getNbPlaces_Reser() . " place(s) pour " . $spectateurs[$i]->getNom_Spectateur() . " " . $spectateurs[$i]->getPrenom_Spectateur() . " , " . $spectateurs[$i]->getVille_Spectateur() . '</td>
                                
                            </tr>';

                }
            }
        }
    }echo '</table>';
    $tests['SupprimerReservation']= true;
}else $listeErreurs["SupprimerReservation"] = "Erreur il est vide";

/**
 * Test Unitaire sur supprimer une reservation
 */
$IDRep=3;
$IDRes=7;
if(!empty($IDRep)){
    $idRep=$IDRep;
    $idRes=$IDRes;
    $nbPlaceReser=$reserManager->selectByIDRes($idRes);
    $nbPlace=$nbPlaceReser->getNbPlaces_Reser();



    //pour la boucle aller rechercher le nbre de place par reservation
    for($i=0;$i<$nbPlace;$i++){
        $chaise->setID_Representation((int)$idRep);
        $chaise->setID_Reservation((int)$idRes);
        $chaiseManager->UpdateChaiseByID($chaise);
    }
    $reservation->setID_Reservation($idRes);
    $reservation->setID_Representation($idRep);
    $reserManager->delete($reservation);


    $tests['ListerReservation']= true;
}else $listeErreurs["ListerReservation"] = "Erreur il est vide";

/**
 * affichage du tableau des résultats
 */
$colorResult = "green";
foreach ($tests as $value) {
    if($value == false){
        $colorResult = "red";
    }
}
echo '<table width="60%">'
    . '<tr><th colspan="3" style="color:'. $colorResult .';">Résultat des tests</th></tr>'
    . '<tr style="text-align:center; font-weight:bold;"><td>Manager</td><td>Résultat du test</td><td>Erreur rencontrée</td></tr>';
foreach($tests as $key => $value){
    $result = $value?"Réussite":"Echec";
    $colorResult = $value?"green":"red";
    $mess = "";
    if(isset($listeErreurs[$key])){
        $mess = $listeErreurs[$key];
    }
    echo '<tr><td>'. $key .'</td><td style="color:'. $colorResult .';">'. $result .'</td><td>'. $mess .'</td></tr>';
}
echo '</table>';
