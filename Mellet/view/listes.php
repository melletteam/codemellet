<?php
/**
 * Page Listes
 */
if(isset($_COOKIE['ID_login'])) {
require_once '../manager/DBManager.php';
require_once '../manager/spectacleManager.php';
require_once '../manager/representationManager.php';
require_once '../model/spectacle.php';
require_once '../model/representation.php';
    /**
     * Variables nécessaires
     */
$DBManager= new DBManager();
$pdo= $DBManager->connect();
$spectacleManager= new spectacleManager($pdo);
$representationManager= new representationManager($pdo);
$arrayRepresentations= $representationManager->selectAllRepresentation();

    /**
     * Affichage de la page Listes
     */
echo'
<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Mellet &mdash; Listes</title>
    <link rel="icon" href="../images/favicon.ico" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Animate.css -->
    <link rel="stylesheet" href="../css/animate.css">
    <!-- Icomoon Icon Fonts-->
    <link rel="stylesheet" href="../css/icomoon.css">
    <!-- Bootstrap  -->
    <link rel="stylesheet" href="../css/bootstrap.css">
    <!-- Magnific Popup -->
    <link rel="stylesheet" href="../css/magnific-popup.css">
    <!-- Owl Carousel  -->
    <link rel="stylesheet" href="../css/owl.carousel.min.css">
    <link rel="stylesheet" href="../css/owl.theme.default.min.css">
    <!-- Theme style  -->
    <link rel="stylesheet" href="../css/style.css">
    <!-- Modernizr JS -->
    <script src="../js/modernizr-2.6.2.min.js"></script>
    <link rel="stylesheet" href="../css/bootstrap.min.import.css">
    <script src="../js/jquery.min.import.js"></script>
    <script src="../js/bootstrap.min.import.js"></script>
</head>
<body>
<div id="page">
    <nav class="gtco-nav" role="navigation">
        <div class="gtco-container">
            <div class="row">
                <div class="col-xs-2">
                   <div id="gtco-logo"><img src="../images/melletCalque.png" width="30 height="30">
                            <a href="#">Mellet.</a></div>
                </div>
                <div class="col-xs-8 text-center menu-1">
                    <ul>
                        <li><a href="salle.php">Salle</a></li>
                        <li><a href="spectacle.php">Spectacle</a></li>
                        <li><a href="spectateurs.php">Spectateurs</a></li>
                        <li><a href="acteurs.php">Acteurs</a></li>
                        <li><a href="reservation.php">Réservation</a></li>
                        <li class="active"><a href="listes.php">Listes</a></li>
                        <li><a href="administration.php">Administration</a></li>
                    </ul>
                </div>
                <div class="col-xs-2 text-right hidden-xs menu-2">
                    <ul>
                        <li class="btn-cta"><a href="../Services/Deconnection.php"><span>Déconnexion</span></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </nav>
    <header id="gtco-header" class="gtco-cover gtco-cover-sm" role="banner" style="background-image:url(../images/img_bg_1.jpg);">
        <div class="gtco-container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2 text-center">
                    <div class="display-t">
                        <div class="display-tc animate-box" data-animate-effect="fadeIn">
                            <h1>LISTES</h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <div class="gtco-section">
        <div class="gtco-container">
            <div class="row animate-box">
                <div class="col-md-6">
                    <p>Liste Réservations</p>
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-backdrop="false" data-target="#listeReservation">Afficher</button>
                </div>
                <div class="col-md-6">
                    <p>Plan Salle</p>
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-backdrop="false" data-target="#planSalle">Afficher</button>
                </div>
            </div>
        </div>
        <!-- DEBUT Modal extraire liste reservation-->
        <div class="modal fade" id="listeReservation" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Liste Réservations</h4>
                    </div>
                        <div class="modal-body">
                            <div class="form-group">
                               <form method="POST" action="../Services/ExtractionListeReservation.php">
                                <SELECT class="form-control" name="ID_Representation" id="ID_Representation" size="1">
                                <option value="">Choisissez une représentation:</option>';
    /**
     * Fonctionnalité pour la liste de Réservations
     */
if(!empty($arrayRepresentations)) {
    foreach ($arrayRepresentations as $representation) {
        echo '<option value='. $representation->getID_Representation() . '_'. $representation->getDate_Representation() . '>' .$spectacleManager->selectByIdAndReturnTitle($representation->getID_Spectacle())."   ".$representation->getDate_Representation() . '</option>';
    }
}else echo "<option value=''>Aucune Représentation actuellement</option>";
echo'</SELECT>
                            <br>
                            <input class="btn btn-primary" type="submit" name="extraire" value="Extraire">
                            </form>
                            </div>
                            </div>
                            <div class="modal-footer">
                                 <button type="button" class="btn btn-primary" data-dismiss="modal">Fermer</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- FIN Modal extraire liste reservation-->
        
        <!-- DEBUT Modal extraire Salle-->
        <div class="modal fade" id="planSalle" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Plan de la salle</h4>
                    </div>
                          <div class="modal-body">
                                <form method="POST" action="../Services/ExtractionPlanSalle.php">
                                <SELECT class="form-control" name="ID_Representation" id="ID_Representation" size="1">
                                <option value="">Choisissez une représentation:</option>';
    /**
     * Fonctionnalité pour le plan de la Salle
     */
if(!empty($arrayRepresentations)) {
    foreach ($arrayRepresentations as $representation) {
        echo '<option value='. $representation->getID_Representation(). '_'. $representation->getDate_Representation()  . '>' .$spectacleManager->selectByIdAndReturnTitle($representation->getID_Spectacle())."   ".$representation->getDate_Representation() . '</option>';
    }
}else echo "<option value=''>Aucune Représentation actuellement</option>";
echo'</SELECT>
                            <br>
                            <input class="btn btn-primary" type="submit" name="extraire" value="Extraire">
                            </form>
                            </div>
                            <div class="modal-footer">
                                 <button type="button" class="btn btn-primary" data-dismiss="modal">Fermer</button>
                          </div>
                    </div>
              </div>
        </div>
        <!-- FIN Modal extraire Salle-->        
    </div>
</div>
<div class="gototop js-top">
    <a href="#" class="js-gotop"><i class="icon-arrow-up"></i></a>
</div>
<!-- jQuery -->
<script src="../js/jquery.min.js"></script>
<!-- jQuery Easing -->
<script src="../js/jquery.easing.1.3.js"></script>
<!-- Bootstrap -->
<script src="../js/bootstrap.min.js"></script>
<!-- Waypoints -->
<script src="../js/jquery.waypoints.min.js"></script>
<!-- Carousel -->
<script src="../js/owl.carousel.min.js"></script>
<!-- countTo -->
<script src="../js/jquery.countTo.js"></script>
<!-- Magnific Popup -->
<script src="../js/jquery.magnific-popup.min.js"></script>
<script src="../js/magnific-popup-options.js"></script>
<!-- Main -->
<script src="../js/main.js"></script>
</body>
</html>';
}else header("Location: ../index.php");