<?php
/**
 * Page Spectacle
 */
if(isset($_COOKIE['ID_login'])) {
require_once '../manager/DBManager.php';
require_once '../manager/spectacleManager.php';
require_once '../manager/ActeursManager.php';
require_once '../model/spectacle.php';
require_once '../model/Acteurs.php';
    /**
     * Variables nécessaires
     */
$DBManager= new DBManager();
$pdo= $DBManager->connect();
$spectacleManager= new spectacleManager($pdo);
$acteurManager=new acteursManager($pdo);
$arraySpectacle= $spectacleManager->selectAllSpectacle();
$arrayActeur= $acteurManager->selectAll();
    /**
     * Affichage de la page Spectacle
     */
echo'<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Mellet &mdash; Spectacle</title>
    <link rel="icon" href="../images/favicon.ico" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Animate.css -->
    <link rel="stylesheet" href="../css/animate.css">
    <!-- Icomoon Icon Fonts-->
    <link rel="stylesheet" href="../css/icomoon.css">
    <!-- Bootstrap  -->
    <link rel="stylesheet" href="../css/bootstrap.css">
    <!-- Magnific Popup -->
    <link rel="stylesheet" href="../css/magnific-popup.css">
    <!-- Owl Carousel  -->
    <link rel="stylesheet" href="../css/owl.carousel.min.css">
    <link rel="stylesheet" href="../css/owl.theme.default.min.css">
    <!-- Theme style  -->
    <link rel="stylesheet" href="../css/style.css">
    <!-- Modernizr JS -->
    <script src="../js/modernizr-2.6.2.min.js"></script>
    <link rel="stylesheet" href="../css/bootstrap.min.import.css">
    <script src="../js/jquery.min.import.js"></script>
    <script src="../js/bootstrap.min.import.js"></script>
    <script src="../js/restrictionUser.js"></script>
    <script src="../js/controleValiditeSpectacle.js"></script>
    <script src="../js/boutonPlusRepresentation.js"></script>
    <script src="../js/reloadDiv.js"></script>

</head>
<body>';
    /**
     * Affichage message lors de l'ajout d'un spectacle ou d'une representation
     */
if(isset($_GET["ajoutSpect"])) {
    if ($_GET["ajoutSpect"] == "1")
        echo '<div class="alert alert-success" role="alert"><strong>Super !</strong> Votre Spectacle a été ajouté avec succès.</div>';
    elseif ($_GET["ajoutSpect"] == "0")
        echo '<div class="alert alert-success" role="alert"><strong>Dommage !</strong> Une erreur est survenue lors de l\'ajout.</div>';
}
if(isset($_GET["ajoutRep"])) {
    if ($_GET["ajoutRep"] == "1")
        echo '<div class="alert alert-success" role="alert"><strong>Super !</strong> Votre représentation a été ajouté avec succès.</div>';
    elseif ($_GET["ajoutRep"] == "0")
        echo '<div class="alert alert-success" role="alert"><strong>Dommage !</strong> Une erreur est survenue lors de l\'ajout.</div>';
}
echo'<div class="gtco-loader"></div>
<div id="page">
    <nav class="gtco-nav" role="navigation">
        <div class="gtco-container">
            <div class="row">
                <div class="col-xs-2">
                    <div id="gtco-logo"><img src="../images/melletCalque.png" width="30 height="30">
                            <a href="#">Mellet.</a></div>
                </div>
                <div class="col-xs-8 text-center menu-1">
                    <ul>
                        <li><a href="salle.php">Salle</a></li>
                        <li class="active"><a href="spectacle.php">Spectacle</a></li>
                        <li><a href="spectateurs.php">Spectateurs</a></li>
                        <li><a href="acteurs.php">Acteurs</a></li>
                        <li><a href="reservation.php">Réservation</a></li>
                        <li><a href="listes.php">Listes</a></li>
                        <li><a href="administration.php">Administration</a></li>
                    </ul>
                </div>
                <div class="col-xs-2 text-right hidden-xs menu-2">
                    <ul>
                        <li class="btn-cta"><a href="../Services/Deconnection.php"><span>Déconnexion</span></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </nav>
    <header id="gtco-header" class="gtco-cover gtco-cover-sm" role="banner" style="background-image:url(../images/img_bg_1.jpg);">
        <div class="gtco-container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2 text-center">
                    <div class="display-t">
                        <div class="display-tc animate-box" data-animate-effect="fadeIn">
                            <h1>SPECTACLE</h1>
                            <FORM method="post">
                                <SELECT class="form-control" name="Spectacle" id="spectacle" size="1" required>
                                <option value="">Sélectionner un spectacle:</option>';
    /**
     * Choix du Spectacle
     */
if(!empty($arraySpectacle)) {
    foreach ($arraySpectacle as $spectacle) {
        echo '<option value='. $spectacle->getID_Spectacle() . '>' .$spectacle->getTitre_Spectacle() . '</option>';
    }
}else echo '<option value="">Aucun spectacle actuellement</option>';
echo'</SELECT>
                            </FORM>
                            <br>
                            <button type="button" id="newSpectacle" data-toggle="modal" class="btn btn-primary" data-backdrop="false" data-target="#nouveauSpectacle">Créer un spectacle</button>
                             <button type="button" id="newRepresentation" data-toggle="modal" class="btn btn-primary" data-backdrop="false" data-target="#AjoutRepresentation">Ajouter des représentations</button>
                        </div>

                    </div>
                </div>

            </div>
        </div>
    </header>

        <div class="gtco-section">
            <div class="gtco-container">
                <div class="row animate-box">
                    <div id="txtHint"><h2><strong>veuillez sélectionner un spectacle</strong></h2></div>
                </div>
                <!-- Modal nouveau spectacle -->
                <div class="modal fade" id="nouveauSpectacle" role="dialog">
                    <div class="modal-dialog">
                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title">Nouveau spectacle</h4>
                            </div>
                            <div class="modal-body">';
    /**
     * Modal pour l'ajout Spectacle
     */
echo'<form method="post" action="../Services/AjoutSpectacle.php">
                                    <fieldset>
                                        <div class="form-group">
                                        <label for="titre">Titre</label>
                                            <br>
                                        <input class="form-control" type="text" id="titre" name="titre" required>
                                        </div>      
                                        <div class="form-group">
                                        <label for="distribution">Distribution</label>
                                        <img type="button" id="buttonPlusActeur" src=../images/plus.png widht="30px" height="30px" />
                                        <div id="divFieldsActeur"></div>
                                        </div>
                                        <div class="form-group">
                                        <label for="misescene">Mise en scène</label>
                                        <img type="button" id="buttonPlusMisescene" src=../images/plus.png widht="30px" height="30px" />
                                        <div id="divFieldsMisescene"></div>
                                        </div>
                                        <div class="form-group">
                                        <label for="regie">Régie</label>
                                        <img type="button" id="buttonPlusRegie" src=../images/plus.png widht="30px" height="30px" />
                                        <div id="divFieldsRegie"></div>
                                        </div>
                                        <div class="form-group">
                                        <label for="decor">Décor</label>
                                        <img type="button" id="buttonPlusDecor" src=../images/plus.png widht="30px" height="30px" />
                                        <div id="divFieldsDecor"></div>
                                        </div>
                                        <div class="form-group">
                                        <label for="aide">Aide-mémoire</label>
                                        <img type="button" id="buttonPlusAide" src=../images/plus.png widht="30px" height="30px" />
                                        <div id="divFieldsAide"></div>
                                        </div>
                                        <div class="form-group">
                                        <label for="presentation">Présentation du spectacle</label>
                                        <img type="button" id="buttonPlusPresentation" src=../images/plus.png widht="30px" height="30px" />
                                        </div>
                                        <div id="divFieldsPresentation"></div> 
                                        <div class="form-group">
                                        <label for="maquillage">Maquillage</label>
                                        <img type="button" id="buttonPlusMaquillage" src=../images/plus.png widht="30px" height="30px" />
                                        </div>
                                        <div id="divFieldsMaquillage"></div>
                                        <div class="form-group">
                                        <label for="representation">Représentation</label>
                                        <img type="button" id="buttonPlusRepresentation" src=../images/plus.png widht="30px" height="30px" />
                                        </div>
                                        <div id="divFieldsRepresentation"><input name=\'nbRepresentation\' id=\'nbRepresentation\' type=\'hidden\' value="0"></div>
                                        <div class="form-group">
                                            <label for="prix">Prix</label>
                                            <br>
                                            <input class="form-control" type="text" id="prix" name="prix" required>
                                        </div>
                                        <input type="submit" class="btn btn-primary" id="validNouveauSpect" name="validerNouveauSpectacle" value="valider">
                                    </fieldset>
                                </form>';
echo'</div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Fin Modal nouvelle representation-->
                 <div class="modal fade" id="AjoutRepresentation" role="dialog">
                    <div class="modal-dialog">
                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title">Ajout de représentation</h4>
                            </div>
                            <div class="modal-body">
                            <form method="post" action="../Services/AjoutRepresentation.php">
                                    <fieldset>
                                        
                                        
                                        <div class="form-group">
                                        <label for="titre">Titre</label>
                                            <br>
                                        <SELECT class="form-control" name="Spectacle" id="spectacle" size="1" required>
                                <option value="">Sélectionner un spectacle:</option>';
    /**
     * Choix du Spectacle dans la modal d'ajout de représentation
     */
if(!empty($arraySpectacle)) {
    foreach ($arraySpectacle as $spectacle) {
        echo '<option value='. $spectacle->getID_Spectacle() . '>' .$spectacle->getTitre_Spectacle() . '</option>';
    }
}else echo '<option value="">Aucun spectacle actuellement</option>';
echo'
                                        </select>
                                        </div>
                                        <div class="form-group">
                                        <img type="button" id="buttonPlusAddRepresentation" src=../images/plus.png widht="30px" height="30px" />
                                        </div>
                                        <div id="divFieldsAddRepresentation"><input name=\'nbRepresentationAdd\' id=\'nbRepresentationAdd\' type=\'hidden\' value="0"></div>
                                        <br>
                                        <input type="submit" class="btn btn-primary" name="validerNouvelleReservation" id="validRepresentationAdd" value="valider">
                                    </fieldset>
                                </form>
                                </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Fin Modal Ajout Representation-->
            </div>
    </div>
</div>

<div class="gototop js-top">
    <a href="#" class="js-gotop"><i class="icon-arrow-up"></i></a>
</div>

<div id="arrayActor" style="display: none;">';
    /**
     * Code pour le bon fonctionnement de l'ajout d'un spectacle
     */
foreach ($arrayActeur as $acteur){
    if($acteur->getStatut_Acteurs()=="acteur")
        echo '<option value='.$acteur->getID_Acteur().'>'.$acteur->getNom_Acteur().' '.$acteur->getPrenom_Acteur().'</option>';
}
echo'</div>';

echo'<div id="arrayScene" style="display: none;">';
foreach ($arrayActeur as $acteur){
    if($acteur->getStatut_Acteurs()=="scene")
        echo '<option value='.$acteur->getID_Acteur().'>'.$acteur->getNom_Acteur().' '.$acteur->getPrenom_Acteur().'</option>';
}
echo'</div>';

echo'<div id="arrayRegie" style="display: none;">';
foreach ($arrayActeur as $acteur){
    if($acteur->getStatut_Acteurs()=="regie")
        echo '<option value='.$acteur->getID_Acteur().'>'.$acteur->getNom_Acteur().' '.$acteur->getPrenom_Acteur().'</option>';
}
echo'</div>';

echo'<div id="arrayDecor" style="display: none;">';
foreach ($arrayActeur as $acteur){
    if($acteur->getStatut_Acteurs()=="decor")
        echo '<option value='.$acteur->getID_Acteur().'>'.$acteur->getNom_Acteur().' '.$acteur->getPrenom_Acteur().'</option>';
}
echo'</div>';

echo'<div id="arrayAide" style="display: none;">';
foreach ($arrayActeur as $acteur){
    if($acteur->getStatut_Acteurs()=="aide")
        echo '<option value='.$acteur->getID_Acteur().'>'.$acteur->getNom_Acteur().' '.$acteur->getPrenom_Acteur().'</option>';
}
echo'</div>';
echo'<div id="arrayPresentation" style="display: none;">';
foreach ($arrayActeur as $acteur){
    if($acteur->getStatut_Acteurs()=="present")
        echo '<option value='.$acteur->getID_Acteur().'>'.$acteur->getNom_Acteur().' '.$acteur->getPrenom_Acteur().'</option>';
}
echo'</div>';
echo'<div id="arrayMaquillage" style="display: none;">';
foreach ($arrayActeur as $acteur){
    if($acteur->getStatut_Acteurs()=="maqui")
        echo '<option value='.$acteur->getID_Acteur().'>'.$acteur->getNom_Acteur().' '.$acteur->getPrenom_Acteur().'</option>';
}
echo'</div>

<!-- jQuery -->
<script src="../js/jquery.min.js"></script>
<!-- jQuery Easing -->
<script src="../js/jquery.easing.1.3.js"></script>
<!-- Bootstrap -->
<script src="../js/bootstrap.min.js"></script>
<!-- Waypoints -->
<script src="../js/jquery.waypoints.min.js"></script>
<!-- Carousel -->
<script src="../js/owl.carousel.min.js"></script>
<!-- countTo -->
<script src="../js/jquery.countTo.js"></script>
<!-- Magnific Popup -->
<script src="../js/jquery.magnific-popup.min.js"></script>
<script src="../js/magnific-popup-options.js"></script>
<!-- Main -->
<script src="../js/main.js"></script>

</body>
</html>';
}else header("Location: ../index.php");