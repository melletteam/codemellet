<?php
/**
 * Page Spectateurs
 */
if(isset($_COOKIE['ID_login'])) {
require_once '../manager/DBManager.php';
require_once '../manager/spectateurManager.php';
require_once  '../model/spectateur.php';
    /**
     * Variables nécessaires
     */
$dbManager = new DBManager();
$pdo = $dbManager->connect();
$spectateurManager = new spectateurManager($pdo);
$spectateur = null;
    /**
     * Affichage de la page Spectateurs
     */
echo'<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Mellet &mdash; Spectateurs</title>
    <link rel="icon" href="../images/favicon.ico" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Animate.css -->
    <link rel="stylesheet" href="../css/animate.css">
    <!-- Icomoon Icon Fonts-->
    <link rel="stylesheet" href="../css/icomoon.css">
    <!-- Bootstrap  -->
    <link rel="stylesheet" href="../css/bootstrap.css">
    <!-- Magnific Popup -->
    <link rel="stylesheet" href="../css/magnific-popup.css">
    <!-- Owl Carousel  -->
    <link rel="stylesheet" href="../css/owl.carousel.min.css">
    <link rel="stylesheet" href="../css/owl.theme.default.min.css">
    <!-- Theme style  -->
    <link rel="stylesheet" href="../css/style.css">
    <!-- Modernizr JS -->
    <script src="../js/modernizr-2.6.2.min.js"></script>
    <link rel="stylesheet" href="../css/bootstrap.min.import.css">
    <script src="../js/jquery.min.import.js"></script>
    <script src="../js/bootstrap.min.import.js"></script>
</head>
<body>';
    /**
     * Affichage du message pour l'ajout d'un Spectateur
     */
if(isset($_GET["ajout"])) {
    if ($_GET["ajout"] == "1")
        echo '<div class="alert alert-success" role="alert"><strong>Super !</strong> Votre spectateur a été ajouté avec succès.</div>';
    elseif ($_GET["ajout"] == "0")
        echo '<div class="alert alert-success" role="alert"><strong>Dommage !</strong> Une erreur est survenue lors de l\'ajout.</div>';
}
echo'<div id="page">
    <nav class="gtco-nav" role="navigation">
        <div class="gtco-container">
            <div class="row">
                <div class="col-xs-2">
                   <div id="gtco-logo"><img src="../images/melletCalque.png" width="30 height="30">
                            <a href="#">Mellet.</a></div>
                </div>
                <div class="col-xs-8 text-center menu-1">
                    <ul>
                        <li><a href="salle.php">Salle</a></li>
                        <li><a href="spectacle.php">Spectacle</a></li>
                        <li class="active"><a href="spectateurs.php">Spectateurs</a></li>
                        <li><a href="acteurs.php">Acteurs</a></li>
                        <li><a href="reservation.php">Réservation</a></li>
                        <li><a href="listes.php">Listes</a></li>
                        <li><a href="administration.php">Administration</a></li>
                    </ul>
                </div>
                <div class="col-xs-2 text-right hidden-xs menu-2">
                    <ul>
                        <li class="btn-cta"><a href="../Services/Deconnection.php"><span>Déconnexion</span></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </nav>
    <header id="gtco-header" class="gtco-cover gtco-cover-sm" role="banner" style="background-image:url(../images/img_bg_1.jpg);">
        <div class="gtco-container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2 text-center">
                    <div class="display-t">
                        <div class="display-tc animate-box" data-animate-effect="fadeIn">
                            <h1>SPECTATEURS</h1>
                                <form method="post">
                                    <div class="form-group">
                                    <input type="text"  class="form-control" name="rechercheSpectateur"  placeholder="Saisie Recherche" size="30"/>
                                    <br>
                                    <button type="submit"  class="btn btn-primary" value="rechercher" >Rechercher</button>
                                     <button type="button"  class="btn btn-primary" data-toggle="modal" data-backdrop="false" data-target="#nouveauSpectateurAjout">Ajouter</button>
                                    </div>
                                </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <div class="gtco-section">
        <div class="gtco-container">
            <div class="row animate-box">
                <div class="col-lg-12 center-table">
                    <p>
                        <table id="tabHigh" class="table table-bordered" >
                        <thead>
                            <tr>
                                <th>Nom</th>
                                <th>Prénom</th>
                                <th>Adresse</th>
                                <th>Fixe</th>
                                <th>Gsm</th>
                                <th>Email</th>
                            </tr>
                        </thead>
                        <tbody>
                        ';
    /**
     * Affichage du tableau des Spectateurs + fonctionnalité de recherche
     */
if(isset($_POST['rechercheSpectateur'])){
    $spectateurs = $spectateurManager->getfullSpectateur($_POST['rechercheSpectateur']);
}
else if (empty($spectateurs)){
    $spectateurs = $spectateurManager->selectAll();
}
foreach ($spectateurs as $spectateur) {
    echo '
                            <tr>
                                 <td>' . $spectateur->getNom_Spectateur() . '</td>
                                 <td>' . $spectateur->getPrenom_Spectateur() . '</td>
                                 <td>' . $spectateur->getRue_Spectateur() . ' ' .$spectateur->getNumeroRue_Spectateur() . ',' . $spectateur->getCP_Spectateur() . ' ' . $spectateur->getVille_Spectateur() . '</td>
                                 <td>' . $spectateur->getNumFix_Spectateur() . '</td>
                                 <td>' . $spectateur->getNumGsm_Spectateur() . '</td>
                                 <td>' . $spectateur->getEmail_Spectateur() . '</td>
                            </tr>
                        </tbody>';
}/**
     * Modal ajout Spectateur
     */
echo'</table>
                    </p>
                </div>
                <!-- Modal nouveau spectateur -->
                <div class="modal fade" id="nouveauSpectateurAjout" role="dialog">
                    <div class="modal-dialog">
                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title">Nouveau spectateur</h4>
                            </div>
                            <div class="modal-body">
                                <form method="post" action="../Services/AjoutSpectateur.php">
                                    <fieldset>
                                        <div class="form-group">
                                            <label for="nom">Nom *</label><br>
                                            <input class="form-control" type="text" id="nom" name="nom" required><br>
                                            <label for="prenom">Prénom *</label><br>
                                            <input class="form-control" type="text" id="prenom" name="prenom" required><br>
                                            <label for="adresse">Adresse *</label><br>
                                            <input class="form-control" type="text" id="adresse" name="adresse" required><br>
                                            <label for="numero">Numéro *</label><br>
                                            <input class="form-control" type="text" id="numero" name="numero" required><br>
                                            <label for="cp">Code Postal *</label><br>
                                            <input class="form-control" type="text" id="cp" name="cp" required><br>
                                            <label for="ville">Localité *</label><br>
                                            <input class="form-control" type="text" id="ville" name="ville" required><br>
                                            <label for="fixe">Numéro fixe</label><br>
                                            <input class="form-control" type="text" id="fix" name="fix"><br>
                                            <label for="gsm">GSM</label><br>
                                            <input class="form-control" type="text" id="gsm" name="gsm"><br>
                                            <label for="mail">E-mail</label><br>
                                            <input class="form-control" type="text" id="mail" name="mail"><br>
                                            <label for="com">Commentaire</label><br>
                                            <input class="form-control" type="text" id="com" name="com"><br>
                                            <br>
                                            <input class="btn btn-primary" type="submit" name="validerNouveauSpactateur" value="valider">
                                        </div>
                                    </fieldset>
                                </form>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Fin Modal nouveau spectateur-->
            </div>
        </div>
    </div>
</div>
<div class="gototop js-top">
    <a href="#" class="js-gotop"><i class="icon-arrow-up"></i></a>
</div>
<!-- jQuery -->
<script src="../js/jquery.min.js"></script>
<!-- jQuery Easing -->
<script src="../js/jquery.easing.1.3.js"></script>
<!-- Bootstrap -->
<script src="../js/bootstrap.min.js"></script>
<!-- Waypoints -->
<script src="../js/jquery.waypoints.min.js"></script>
<!-- Carousel -->
<script src="../js/owl.carousel.min.js"></script>
<!-- countTo -->
<script src="../js/jquery.countTo.js"></script>
<!-- Magnific Popup -->
<script src="../js/jquery.magnific-popup.min.js"></script>
<script src="../js/magnific-popup-options.js"></script>
<!-- Main -->
<script src="../js/main.js"></script>
</body>
</html>';
}else header("Location: ../index.php");