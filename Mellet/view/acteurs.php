<?php
/**
 * Page Acteurs
 */
if(isset($_COOKIE['ID_login'])) {
require_once '../manager/DBManager.php';
require_once '../manager/ActeursManager.php';
require_once  '../model/Acteurs.php';
    /**
     * Variables nécessaires
     */
$dbManager = new DBManager();
$pdo = $dbManager->connect();
$ActeurManager = new acteursManager($pdo);
$acteur = null;
    /**
     * Affichage de la page Acteurs
     */

echo'<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Mellet &mdash; Acteurs</title>
    <link rel="icon" href="../images/favicon.ico" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Animate.css -->
    <link rel="stylesheet" href="../css/animate.css">
    <!-- Icomoon Icon Fonts-->
    <link rel="stylesheet" href="../css/icomoon.css">
    <!-- Bootstrap  -->
    <link rel="stylesheet" href="../css/bootstrap.css">
    <!-- Magnific Popup -->
    <link rel="stylesheet" href="../css/magnific-popup.css">
    <!-- Owl Carousel  -->
    <link rel="stylesheet" href="../css/owl.carousel.min.css">
    <link rel="stylesheet" href="../css/owl.theme.default.min.css">
    <!-- Theme style  -->
    <link rel="stylesheet" href="../css/style.css">
    <!-- Modernizr JS -->
    <script src="../js/modernizr-2.6.2.min.js"></script>
    <link rel="stylesheet" href="../css/bootstrap.min.import.css">
    <script src="../js/jquery.min.import.js"></script>
    <script src="../js/bootstrap.min.import.js"></script>
    <script src="../js/restrictionUser.js"></script>
</head>
<body>';
    /**
     * Message lors de l'ajout d'un acteur
     */
if(isset($_GET["ajout"])) {
    if ($_GET["ajout"] == "1")
        echo '<div class="alert alert-success" role="alert"><strong>Super !</strong> Votre acteur a été ajouté avec succès.</div>';
    elseif ($_GET["ajout"] == "0")
        echo '<div class="alert alert-success" role="alert"><strong>Dommage !</strong> Une erreur est survenue lors de l\'ajout.</div>';
}
echo'<div id="page">
    <nav class="gtco-nav" role="navigation">
        <div class="gtco-container">
            <div class="row">
                <div class="col-xs-2">
                    <div id="gtco-logo"><img src="../images/melletCalque.png" width="30 height="30">
                            <a href="#">Mellet.</a></div>
                </div>
                <div class="col-xs-8 text-center menu-1">
                    <ul>
                        <li><a href="salle.php">Salle</a></li>
                        <li><a href="spectacle.php">Spectacle</a></li>
                        <li><a href="spectateurs.php">Spectateurs</a></li>
                        <li class="active"><a href="acteurs.php">Acteurs</a></li>
                        <li><a href="reservation.php">Réservation</a></li>
                        <li><a href="listes.php">Listes</a></li>
                        <li><a href="administration.php">Administration</a></li>
                    </ul>
                </div>
                <div class="col-xs-2 text-right hidden-xs menu-2">
                    <ul>
                         <li class="btn-cta"><a href="../Services/Deconnection.php"><span>Déconnexion</span></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </nav>
    <header id="gtco-header" class="gtco-cover gtco-cover-sm" role="banner" style="background-image:url(../images/img_bg_1.jpg);">
        <div class="gtco-container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2 text-center">
                    <div class="display-t">
                        <div class="display-tc animate-box" data-animate-effect="fadeIn">
                            <h1>ACTEURS</h1>
                                <form method="post"><!-- lien vers page php -->
                                    <div class="form-group">
                                    <input type="text"  class="form-control" name="rechercheActeur"  placeholder="Saisie Recherche" size="30"/>
                                    <br>
                                    <button type="submit"  class="btn btn-primary" value="rechercher" >Rechercher</button>
                                     <button type="button" id="nouveauActeur" class="btn btn-primary" data-toggle="modal" data-backdrop="false" data-target="#nouveauActeurAjout">Ajouter</button>
                                    </div>
                                </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <div class="gtco-section">
        <div class="gtco-container">
            <div class="row animate-box">
                <div class="col-lg-12 center-table">
                    <p>
                        <table id="tabHigh" class="table table-bordered" >
                        <thead>
                            <tr>
                                <th>Nom</th>
                                <th>Prénom</th>
                                <th>Statut</th>
                            </tr>
                        </thead>
                        <tbody>';
    /**
     * Affichage du tableau des acteurs + fonctionnalité de recherche
     */
if(isset($_POST['rechercheActeur'])){
    $acteurs = $ActeurManager->getfullActeur($_POST['rechercheActeur']);
}
else if (empty($acteurs)){
    $acteurs = $ActeurManager->selectAll();
}
foreach ($acteurs as $acteur) {
    echo '
                            <tr>
                                 <td>' . $acteur->getNom_Acteur() . '</td>
                                 <td>' . $acteur->getPrenom_Acteur() . '</td>
                                 <td>' . $acteur->getStatut_Acteurs() . '</td>
                            </tr>
                        </tbody>';
}
echo'</table>
                    </p>
                </div>
                <!-- Modal nouveau spectateur -->
                <div class="modal fade" id="nouveauActeurAjout" role="dialog">
                    <div class="modal-dialog">
                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title">Nouvel acteur</h4>
                            </div>
                            <div class="modal-body">
                                <form method="post" action="../Services/AjoutActeur.php">
                                    <fieldset>
                                        <div class="form-group">
                                            <label for="nom">Nom</label><br>
                                            <input class="form-control" type="text" id="nom" name="nom" required><br>
                                            <label for="prenom">Prénom</label><br>
                                            <input class="form-control" type="text" id="prenom" name="prenom" required><br>
                                            <label for="statut">Statut</label><br>
                                            <SELECT class=\'form-control\' name="statut" id="statut" size="1">
                                            <option value="acteur">Acteur</option>
                                            <option value="scene">Metteur en scène</option>
                                            <option value="regie">Régie</option>
                                            <option value="decor">Décor</option>
                                            <option value="aide">Aide-mémoire</option>
                                            <option value="present">Présentation du spectacle</option>
                                            <option value="maqui">Maquillage</option>
                                            </SELECT>
                                            <br>
                                            <input class="btn btn-primary" type="submit" name="validerNouveauActeur" value="valider">
                                        </div>
                                    </fieldset>
                                </form>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Fin Modal nouveau acteur-->
            </div>
        </div>
    </div>
</div>
<div class="gototop js-top">
    <a href="#" class="js-gotop"><i class="icon-arrow-up"></i></a>
</div>
<!-- jQuery -->
<script src="../js/jquery.min.js"></script>
<!-- jQuery Easing -->
<script src="../js/jquery.easing.1.3.js"></script>
<!-- Bootstrap -->
<script src="../js/bootstrap.min.js"></script>
<!-- Waypoints -->
<script src="../js/jquery.waypoints.min.js"></script>
<!-- Carousel -->
<script src="../js/owl.carousel.min.js"></script>
<!-- countTo -->
<script src="../js/jquery.countTo.js"></script>
<!-- Magnific Popup -->
<script src="../js/jquery.magnific-popup.min.js"></script>
<script src="../js/magnific-popup-options.js"></script>
<!-- Main -->
<script src="../js/main.js"></script>
</body>
</html>';}else header("Location: ../index.php");