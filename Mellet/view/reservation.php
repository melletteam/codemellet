<?php
/**
 * Page Réservation
 */
if(isset($_COOKIE['ID_login'])) {
require_once '../manager/DBManager.php';
require_once '../manager/SpectateurManager.php';
require_once '../model/Spectateur.php';
require_once '../manager/DBManager.php';
require_once '../manager/representationManager.php';
require_once  '../model/representation.php';
require_once '../manager/spectacleManager.php';
require_once '../model/spectacle.php';
require_once '../manager/reservationManager.php';
require_once '../model/reservation.php';
    /**
     * Variables nécessaires
     */
$dbManager = new DBManager();
$pdo = $dbManager->connect();
$representationManager= new representationManager($pdo);
$arrayRepresentations= $representationManager->selectAllRepresentation();
$spectacleManager= new spectacleManager($pdo);
$spectateurManager = new spectateurManager($pdo);
$spectateur = null;
    /**
     * Affichage de la page Réservation
     */
echo'<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Mellet &mdash; Réservation</title>
    <link rel="icon" href="../images/favicon.ico" />
    <meta name="viewport" content="width=device-width, initial-scale=1">    
    <!-- Animate.css -->
    <link rel="stylesheet" href="../css/animate.css">
    <!-- Icomoon Icon Fonts-->
    <link rel="stylesheet" href="../css/icomoon.css">
    <!-- Bootstrap  -->
    <link rel="stylesheet" href="../css/bootstrap.css">
    <!-- Magnific Popup -->
    <link rel="stylesheet" href="../css/magnific-popup.css">
    <!-- Owl Carousel  -->
    <link rel="stylesheet" href="../css/owl.carousel.min.css">
    <link rel="stylesheet" href="../css/owl.theme.default.min.css">
    <!-- Theme style  -->
    <link rel="stylesheet" href="../css/style.css">
    <!-- Modernizr JS -->
    <script src="../js/modernizr-2.6.2.min.js"></script>
    <link rel="stylesheet" href="../css/bootstrap.min.import.css">
    <script src="../js/jquery.min.import.js"></script>
    <script src="../js/bootstrap.min.import.js"></script>
    <script src="../js/boutonPlace.js"></script>
    <script src="../js/reloadDiv.js"></script>
</head>
<body>
<div class="gtco-loader"></div>
<div id="page">
    <nav class="gtco-nav" role="navigation">
        <div class="gtco-container">
            <div class="row">
                <div class="col-xs-2">
                    <div id="gtco-logo"><img src="../images/melletCalque.png" width="30 height="30">
                            <a href="#">Mellet.</a></div>
                </div>
                <div class="col-xs-8 text-center menu-1">
                    <ul>
                        <li><a href="salle.php">Salle</a></li>
                        <li><a href="spectacle.php">Spectacle</a></li>
                        <li><a href="spectateurs.php">Spectateurs</a></li>
                        <li><a href="acteurs.php">Acteurs</a></li>
                        <li class="active"><a href="reservation.php">Réservation</a></li>
                        <li><a href="listes.php">Listes</a></li>
                        <li><a href="administration.php">Administration</a></li>
                    </ul>
                </div>
                <div class="col-xs-2 text-right hidden-xs menu-2">
                    <ul>
                        <li class="btn-cta"><a href="../Services/Deconnection.php"><span>Déconnexion</span></a></li>
                    </ul>
                </div>
            </div>       
        </div>
    </nav>
    <header id="gtco-header" class="gtco-cover gtco-cover-sm" role="banner" style="background-image:url(../images/img_bg_1.jpg);">
        <div class="gtco-container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2 text-center">
                    <div class="display-t">
                        <div class="display-tc animate-box" data-animate-effect="fadeIn">
                            <h1>RESERVATION</h1>
                            <FORM>
                                <SELECT class="form-control" name="piece" size="1" required id="reservation">
                                <option value="">Choisissez une représentation:</option>';
    /**
     * Choix de la représentation
     */
if(!empty($arrayRepresentations)) {
    foreach ($arrayRepresentations as $representation) {
        echo '<option value='. $representation->getID_Representation() . '>' .$spectacleManager->selectByIdAndReturnTitle($representation->getID_Spectacle())."   ".$representation->getDate_Representation() . '</option>';
    }
}else echo "<option value=''>Aucune Représentation actuellement</option>";
echo'</SELECT>
                            </FORM><br>
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <div class="gtco-section">
        <div class="gtco-container">
            <div class="row animate-box">
                <div id="txtHint"><h2><strong>veuillez sélectionner une représentation</strong></h2></div>
            </div>
        </div>
    </div>
</div>
<div class="gototop js-top">
    <a href="#" class="js-gotop"><i class="icon-arrow-up"></i></a>
</div>
<!-- jQuery -->
<script src="../js/jquery.min.js"></script>
<!-- jQuery Easing -->
<script src="../js/jquery.easing.1.3.js"></script>
<!-- Bootstrap -->
<script src="../js/bootstrap.min.js"></script>
<!-- Waypoints -->
<script src="../js/jquery.waypoints.min.js"></script>
<!-- Carousel -->
<script src="../js/owl.carousel.min.js"></script>
<!-- countTo -->
<script src="../js/jquery.countTo.js"></script>
<!-- Magnific Popup -->
<script src="../js/jquery.magnific-popup.min.js"></script>
<script src="../js/magnific-popup-options.js"></script>
<!-- Main -->
<script src="../js/main.js"></script>
</body>
</html>';
}else header("Location: ../index.php");