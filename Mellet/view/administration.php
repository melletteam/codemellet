<?php
/**
 * Page Administration
 */
if(isset($_COOKIE['ID_login'])) {
    require_once '../manager/DBManager.php';
    require_once '../manager/AdministrationManager.php';
    require_once '../model/Administration.php';
    /**
     * Variables nécessaires
     */
    $dbManager = new DBManager();
    $pdo = $dbManager->connect();
    $administrationManager = new administrationManager($pdo);
    $user = null;
    /**
     * Affichage de la page Administration
     */
    echo '<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Mellet &mdash; Administration</title>
    <link rel="icon" href="../images/favicon.ico" />
    <meta name="viewport" content="width=device-width, initial-scale=1">   
    <!-- Animate.css -->
    <link rel="stylesheet" href="../css/animate.css">
    <!-- Icomoon Icon Fonts-->
    <link rel="stylesheet" href="../css/icomoon.css">
    <!-- Bootstrap  -->
    <link rel="stylesheet" href="../css/bootstrap.css">
    <!-- Magnific Popup -->
    <link rel="stylesheet" href="../css/magnific-popup.css">
    <!-- Owl Carousel  -->
    <link rel="stylesheet" href="../css/owl.carousel.min.css">
    <link rel="stylesheet" href="../css/owl.theme.default.min.css">
    <!-- Theme style  -->
    <link rel="stylesheet" href="../css/style.css">
    <!-- Modernizr JS -->
    <script src="../js/modernizr-2.6.2.min.js"></script>
    <link rel="stylesheet" href="../css/bootstrap.min.import.css">
    <script src="../js/jquery.min.import.js"></script>
    <script src="../js/bootstrap.min.import.js"></script>
    <script src="../js/restrictionUser.js"></script>
</head>
<body>';
    /**
     * Message lors de l'ajout de User(s)
     */
    if (isset($_GET["ajout"])) {
        if ($_GET["ajout"] == "2")
            echo '<div class="alert alert-success" role="alert"><strong>Super !</strong> Votre utilisateur a été ajouté avec succès.</div>';
        elseif ($_GET["ajout"] == "1")
            echo '<div class="alert alert-success" role="alert"><strong>Dommage !</strong> Ce nom d\'utilisateur est déja utilisé.</div>';
        elseif ($_GET["ajout"] == "0")
            echo '<div class="alert alert-success" role="alert"><strong>Dommage !</strong> Une erreur est survenue lors de l\'ajout.</div>';
    }
    if(isset($_GET["modif"])){
        if ($_GET["modif"] == "2")
            echo '<div class="alert alert-success" role="alert"><strong>Super !</strong> Votre Mot de Passe a été modifié avec succès.</div>';
        elseif ($_GET["modif"] == "1")
            echo '<div class="alert alert-success" role="alert"><strong>Dommage !</strong> Votre ancien est invalide.</div>';
        elseif ($_GET["modif"] == "0")
            echo '<div class="alert alert-success" role="alert"><strong>Dommage !</strong> La confirmation du nouveau mot de passe est invalide.</div>';
    }
    echo '<div class="gtco-loader"></div>
<div id="page">
    <nav class="gtco-nav" role="navigation">
        <div class="gtco-container">
            <div class="row">
                <div class="col-xs-2">
                    <div id="gtco-logo"><img src="../images/melletCalque.png" width="30" height="30">
                            <a href="#">Mellet.</a></div>
                </div>
                <div class="col-xs-8 text-center menu-1">
                    <ul>
                        <li><a href="salle.php">Salle</a></li>
                        <li><a href="spectacle.php">Spectacle</a></li>
                        <li><a href="spectateurs.php">Spectateurs</a></li>
                        <li><a href="acteurs.php">Acteurs</a></li>
                        <li><a href="reservation.php">Réservation</a></li>
                        <li><a href="listes.php">Listes</a></li>
                        <li class="active"><a href="administration.php">Administration</a></li>
                    </ul>
                </div>
                <div class="col-xs-2 text-right hidden-xs menu-2">
                    <ul>
                        <li class="btn-cta"><a href="../Services/Deconnection.php"><span>Déconnexion</span></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </nav>
    <header id="gtco-header" class="gtco-cover gtco-cover-sm" role="banner" style="background-image:url(../images/img_bg_1.jpg);">
        <div class="gtco-container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2 text-center">
                    <div class="display-t">
                        <div class="display-tc animate-box" data-animate-effect="fadeIn">
                            <h1>ADMINISTRATION</h1>
                                    <form method="post"><!-- lien vers page php -->
                                    <div class="form-group">
                                     <br>                                 
                                     <button type="button" id="ajoutUser" class="btn btn-primary" data-toggle="modal" data-backdrop="false" data-target="#nouveauUserAjout">Ajouter</button>
                                    </div>
                                </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <div class="gtco-section">
        <div class="gtco-container">
                    <div class="row animate-box">
                <div class="col-lg-12 center-table">
                        <table id="tabHigh" class="table table-bordered" >
                        <thead>
                            <tr>
                                <th>User</th>
                                <th>Statut</th>
                                <th>Statut User ?</th>
                                <th>Supprimer ?</th> 
                            </tr>
                        </thead>
                        <tbody>';
    /**
     * Affichage du tableau des Users
     */
    $users = $administrationManager->selectAll();
    $cpt=0;
    foreach ($users as $user) {
        if($user->getStatut_User() == 's') {
            $statut = 'Super-User';
        }else {$statut = 'User';}

        echo '
                            <tr>
                                 <td>' . $user->getLogin_User() . '</td>
                                 <td>' . $statut . '</td>
                                 <td>  
                                    <form method="post" action="../Services/ModifierStatutUser.php"><input type="hidden" name="ID_User" value="' . $user->getID_User() . '"><button type="submit" id='."modifierUser".$cpt.' class="btn btn-primary" data-toggle="modal" data-backdrop="false" >Modifier</button></form>
                                 </td>   
                                 <td><form method="post" action="../Services/SupprimerUser.php"><input type="hidden" name="ID_User" value="' . $user->getID_User() . '"><button type="submit" id='."supprimerUser".$cpt.' class="btn btn-primary" data-toggle="modal" data-backdrop="false" >Supprimer</button></form></td>
                            </tr>
                        </tbody>';
        $cpt++;
    }
    echo'<div style="display: none;"><input id="NbUser" value='.$cpt.'></div>';
    /**
     * Fonctionnalités de la page Administration
     */
    echo '</div>
         </div>
    </div>
</div>
                    <button type="button" id="suppressionSpectacle" class="btn btn-primary" data-toggle="modal" data-backdrop="false" data-target="#confirmerSuppression">Supprimer Spectacles ?</button>
                    <button type="button"  class="btn btn-primary" data-toggle="modal" data-backdrop="false" data-target="#modifierPswd">Modifier Mot de Passe</button>
                    <form action="../Services/SauveDB.php">
                    <button type="submit" id="backup" class="btn btn-primary" >Faire un Backup</button></form><br> 
 <!-- Modal nouveau user -->
                <div class="modal fade" id="nouveauUserAjout" role="dialog">
                    <div class="modal-dialog">
                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title">Nouveau User</h4>
                            </div>
                            <div class="modal-body">
                                <form method="POST" action="../Services/AjoutUser.php">
                                    <fieldset>
                                        <div class="form-group">
                                            <label for="login">Login *</label><br>
                                            <input class="form-control" type="text" id="login" name="login" required><br>
                                            <label for="pswd">Mot de passe *</label><br>
                                            <input class="form-control" type="password" id="pswd" name="pswd" required><br>
                                            <label for="statut">Statut *</label><br>
                                            <SELECT class=\'form-control\' name="statut" id="statut" size="1" value>
                                            <option value="u">User</option>
                                            <option value="s">Super User</option>
                                            </SELECT>           
                                            <br>
                                            <input class="btn btn-primary" type="submit" name="validerNouveauUser" value="Valider">
                                        </div>
                                    </fieldset>
                                </form>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Fin Modal nouveau user-->               
                <!-- Modal confirmer suppression -->
                <div class="modal fade" id="confirmerSuppression" role="dialog">
                    <div class="modal-dialog">
                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Suppression Spectacles</h5>
                            </div>
                            <div class="modal-body">
                                        <p>
                                            <strong>Etes-vous sûr de vouloir supprimer toutes les données suivantes : Réservations, Représentations et Spectacles ?</strong>
                                        </p>
                                        <div class="form-group">
                                            <span class="marge"><form method="post" action="../Services/SupprimerSpectacles.php"><input type="hidden" name="SupprimerSpectacles"><button type="submit"  class="btn btn-primary" data-toggle="modal" data-backdrop="false" >Oui</button></form></span>
                                            <span class="marge"><button type="button" class="btn btn-primary" data-dismiss="modal">Non</button></span>
                                        </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Fin Modal confirmer suppression-->               
                <!-- Modal modifier pswd -->
                <div class="modal fade" id="modifierPswd" role="dialog">
                    <div class="modal-dialog">
                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title">Modifier Mot de Passe</h4>
                            </div>
                            <div class="modal-body">
                                <form method="POST" action="../Services/ModifierPswd.php">
                                    <fieldset>
                                        <div class="form-group">
                                            <label for="oldPswd">Ancien Mot de Passe *</label><br>
                                            <input class="form-control" type="password" id="oldPswd" name="oldPswd" required><br>
                                            <label for="pswd">Nouveau Mot de Passe *</label><br>
                                            <input class="form-control" type="password" id="pswd" name="pswd" required><br>                                           
                                            <br>
                                            <input class="btn btn-primary" type="submit" name="validerNouveauUser" value="Valider">
                                        </div>
                                    </fieldset>
                                </form>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Fin Modal modifier pswd-->
<div class="gototop js-top">
    <a href="#" class="js-gotop"><i class="icon-arrow-up"></i></a>
</div>
<!-- jQuery -->
<script src="../js/jquery.min.js"></script>
<!-- jQuery Easing -->
<script src="../js/jquery.easing.1.3.js"></script>
<!-- Bootstrap -->
<script src="../js/bootstrap.min.js"></script>
<!-- Waypoints -->
<script src="../js/jquery.waypoints.min.js"></script>
<!-- Carousel -->
<script src="../js/owl.carousel.min.js"></script>
<!-- countTo -->
<script src="../js/jquery.countTo.js"></script>
<!-- Magnific Popup -->
<script src="../js/jquery.magnific-popup.min.js"></script>
<script src="../js/magnific-popup-options.js"></script>
<!-- Main -->
<script src="../js/main.js"></script>
</body>
</html>';
}else header("Location: ../index.php");