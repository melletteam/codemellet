<?php
/**
 * Created by IntelliJ IDEA.
 * User: bouffa
 * Date: 08-03-18
 * Time: 13:33
 * Modèle de l'objet Représentation dans la base de données
 */

class representation
{
    private $ID_Representation;
    private $Date_Representation;
    private $Nb_Lignes_Representation;
    private $nb_Col_Representation;
    private $ID_Spectacle;

    function __construct($arr = null)
    {
        if($arr != null){
            $this->fillObject($arr);
            //var_dump($this);
        }
    }
    private function fillObject($arr){
        foreach ($arr as $key => $value){
            $method = 'set'.ucfirst($key);
            if(method_exists($this,$method)){
                $this->$method($value);
            }
        }
    }

    /**
     * @return mixed
     */
    public function getID_Representation()
    {
        return $this->ID_Representation;
    }

    /**
     * @param mixed $ID_Representation
     */
    public function setID_Representation($ID_Representation)
    {
        $this->ID_Representation = $ID_Representation;
    }

    /**
     * @return mixed
     */
    public function getDate_Representation()
    {
        return $this->Date_Representation;
    }

    /**
     * @param mixed $Date_Representation
     */
    public function setDate_Representation($Date_Representation)
    {
        $this->Date_Representation = $Date_Representation;
    }

    /**
     * @return mixed
     */
    public function getNb_Lignes_Representation()
    {
        return $this->Nb_Lignes_Representation;
    }

    /**
     * @param mixed $Nb_Lignes_Representation
     */
    public function setNb_Lignes_Representation($Nb_Lignes_Representation)
    {
        $this->Nb_Lignes_Representation = $Nb_Lignes_Representation;
    }

    /**
     * @return mixed
     */
    public function getNb_Col_Representation()
    {
        return $this->nb_Col_Representation;
    }

    /**
     * @param mixed $nb_Col_Representation
     */
    public function setNb_Col_Representation($nb_Col_Representation)
    {
        $this->nb_Col_Representation = $nb_Col_Representation;
    }

    /**
     * @return mixed
     */
    public function getID_Spectacle()
    {
        return $this->ID_Spectacle;
    }

    /**
     * @param mixed $ID_Spectacle
     */
    public function setID_Spectacle($ID_Spectacle)
    {
        $this->ID_Spectacle = $ID_Spectacle;
    }


}