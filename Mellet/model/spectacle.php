<?php
/**
 * Created by IntelliJ IDEA.
 * User: bouffa
 * Date: 08-03-18
 * Time: 13:33
 * Modèle de l'objet Spectacle dans la base de données
 */

class spectacle
{
    private $ID_Spectacle;
    private $Titre_Spectacle;
    private $Prix_Spectacle;



    function __construct($arr = null)
    {
        if($arr != null){
            $this->fillObject($arr);
            //var_dump($this);
        }
    }
    private function fillObject($arr){
        foreach ($arr as $key => $value){
            $method = 'set'.ucfirst($key);
            if(method_exists($this,$method)){
                $this->$method($value);
            }
        }
    }

    /**
     * @return mixed
     */
    public function getID_Spectacle()
    {
        return $this->ID_Spectacle;
    }

    /**
     * @param mixed $ID_Spectacle
     */
    public function setID_Spectacle($ID_Spectacle)
    {
        $this->ID_Spectacle = $ID_Spectacle;
    }

    /**
     * @return mixed
     */
    public function getTitre_Spectacle()
    {
        return $this->Titre_Spectacle;
    }

    /**
     * @param mixed $Titre_Spectacle
     */
    public function setTitre_Spectacle($Titre_Spectacle)
    {
        $this->Titre_Spectacle = $Titre_Spectacle;
    }

    /**
     * @return mixed
     */
    public function getPrix_Spectacle()
    {
        return $this->Prix_Spectacle;
    }

    /**
     * @param mixed $Prix_Spectacle
     */
    public function setPrix_Spectacle($Prix_Spectacle)
    {
        $this->Prix_Spectacle = $Prix_Spectacle;
    }


}