<?php
/**
 * Created by IntelliJ IDEA.
 * User: Josua
 * Date: 11-04-18
 * Time: 18:11
 * Modèle de l'objet User dans la base de données
 */

class administration
{

    private $ID_User;
    private $Login_User;
    private $Pswd_User;
    private $Statut_User;

    function __construct($arr = null)
    {
        if($arr != null){
            $this->fillObject($arr);
        }
    }
    private function fillObject($arr){
        foreach ($arr as $key => $value){
            $method = 'set'.ucfirst($key);
            if(method_exists($this,$method)){
                $this->$method($value);
            }
        }
    }

    /**
     * @return mixed
     */
    public function getID_User()
    {
        return $this->ID_User;
    }

    /**
     * @param mixed $ID_User
     */
    public function setID_User($ID_User)
    {
        $this->ID_User = $ID_User;
    }

    /**
     * @return mixed
     */
    public function getLogin_User()
    {
        return $this->Login_User;
    }

    /**
     * @param mixed $Login_User
     */
    public function setLogin_User($Login_User)
    {
        $this->Login_User = $Login_User;
    }

    /**
     * @return mixed
     */
    public function getPswd_User()
    {
        return $this->Pswd_User;
    }

    /**
     * @param mixed $Pswd_User
     */
    public function setPswd_User($Pswd_User)
    {
        $this->Pswd_User = $Pswd_User;
    }

    /**
     * @return mixed
     */
    public function getStatut_User()
    {
        return $this->Statut_User;
    }

    /**
     * @param mixed $Statut_User
     */
    public function setStatut_User($Statut_User)
    {
        $this->Statut_User = $Statut_User;
    }


}