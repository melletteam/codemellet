<?php
/**
 * Created by IntelliJ IDEA.
 * User: bouffa
 * Date: 08-03-18
 * Time: 13:33
 * Modèle de l'objet Réservation dans la base de données
 */

class reservation
{
    private $ID_Reservation;
    private $NbPlaces_Reser;
    private $ID_Representation;
    private $ID_Spectateur;

    function __construct($arr = null)
    {
        if($arr != null){
            $this->fillObject($arr);
            //var_dump($this);
        }
    }
    private function fillObject($arr){
        foreach ($arr as $key => $value){
            $method = 'set'.ucfirst($key);
            if(method_exists($this,$method)){
                $this->$method($value);
            }
        }
    }

    /**
     * @return mixed
     */
    public function getID_Reservation()
    {
        return $this->ID_Reservation;
    }

    /**
     * @param mixed $ID_Reservation
     */
    public function setID_Reservation($ID_Reservation)
    {
        $this->ID_Reservation = $ID_Reservation;
    }

    /**
     * @return mixed
     */
    public function getNbPlaces_Reser()
    {
        return $this->NbPlaces_Reser;
    }

    /**
     * @param mixed $NbPlaces_Reser
     */
    public function setNbPlaces_Reser($NbPlaces_Reser)
    {
        $this->NbPlaces_Reser = $NbPlaces_Reser;
    }

    /**
     * @return mixed
     */
    public function getID_Representation()
    {
        return $this->ID_Representation;
    }

    /**
     * @param mixed $ID_Representation
     */
    public function setID_Representation($ID_Representation)
    {
        $this->ID_Representation = $ID_Representation;
    }

    /**
     * @return mixed
     */
    public function getID_Spectateur()
    {
        return $this->ID_Spectateur;
    }

    /**
     * @param mixed $ID_Spectateur
     */
    public function setID_Spectateur($ID_Spectateur)
    {
        $this->ID_Spectateur = $ID_Spectateur;
    }


}