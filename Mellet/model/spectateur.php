<?php
/**
 * Created by IntelliJ IDEA.
 * User: bouffa
 * Date: 08-03-18
 * Time: 13:33
 * Modèle de l'objet Spectateur dans la base de données
 */

class spectateur
{
    private $ID_Spectateur;
    private $Nom_Spectateur;
    private $Prenom_Spectateur;
    private $Rue_Spectateur;
    private $NumeroRue_Spectateur;
    private $CP_Spectateur;
    private $Ville_Spectateur;
    private $NumFix_Spectateur;
    private $NumGsm_Spectateur;
    private $Email_Spectateur;
    private $Com_Spectateur;

    function __construct($arr = null)
    {
        if($arr != null){
            $this->fillObject($arr);
        }
    }
    private function fillObject($arr){
        foreach ($arr as $key => $value){
            $method = 'set'.ucfirst($key);
            if(method_exists($this,$method)){
                $this->$method($value);
            }
        }
    }

    /**
     * @return mixed
     */
    public function getID_Spectateur()
    {
        return $this->ID_Spectateur;
    }

    /**
     * @param mixed $ID_Spectateur
     */
    public function setID_Spectateur($ID_Spectateur)
    {
        $this->ID_Spectateur = $ID_Spectateur;
    }

    /**
     * @return mixed
     */
    public function getNom_Spectateur()
    {
        return $this->Nom_Spectateur;
    }

    /**
     * @param mixed $Nom_Spectateur
     */
    public function setNom_Spectateur($Nom_Spectateur)
    {
        $this->Nom_Spectateur = $Nom_Spectateur;
    }

    /**
     * @return mixed
     */
    public function getPrenom_Spectateur()
    {
        return $this->Prenom_Spectateur;
    }

    /**
     * @param mixed $Prenom_Spectateur
     */
    public function setPrenom_Spectateur($Prenom_Spectateur)
    {
        $this->Prenom_Spectateur = $Prenom_Spectateur;
    }

    /**
     * @return mixed
     */
    public function getRue_Spectateur()
    {
        return $this->Rue_Spectateur;
    }

    /**
     * @param mixed $Rue_Spectateur
     */
    public function setRue_Spectateur($Rue_Spectateur)
    {
        $this->Rue_Spectateur = $Rue_Spectateur;
    }

    /**
     * @return mixed
     */
    public function getNumeroRue_Spectateur()
    {
        return $this->NumeroRue_Spectateur;
    }

    /**
     * @param mixed $NumeroRue_Spectateur
     */
    public function setNumeroRue_Spectateur($NumeroRue_Spectateur)
    {
        $this->NumeroRue_Spectateur = $NumeroRue_Spectateur;
    }

    /**
     * @return mixed
     */
    public function getCP_Spectateur()
    {
        return $this->CP_Spectateur;
    }

    /**
     * @param mixed $CP_Spectateur
     */
    public function setCP_Spectateur($CP_Spectateur)
    {
        $this->CP_Spectateur = $CP_Spectateur;
    }

    /**
     * @return mixed
     */
    public function getVille_Spectateur()
    {
        return $this->Ville_Spectateur;
    }

    /**
     * @param mixed $Ville_Spectateur
     */
    public function setVille_Spectateur($Ville_Spectateur)
    {
        $this->Ville_Spectateur = $Ville_Spectateur;
    }

    /**
     * @return mixed
     */
    public function getNumFix_Spectateur()
    {
        return $this->NumFix_Spectateur;
    }

    /**
     * @param mixed $NumFix_Spectateur
     */
    public function setNumFix_Spectateur($NumFix_Spectateur)
    {
        $this->NumFix_Spectateur = $NumFix_Spectateur;
    }

    /**
     * @return mixed
     */
    public function getNumGsm_Spectateur()
    {
        return $this->NumGsm_Spectateur;
    }

    /**
     * @param mixed $NumGsm_Spectateur
     */
    public function setNumGsm_Spectateur($NumGsm_Spectateur)
    {
        $this->NumGsm_Spectateur = $NumGsm_Spectateur;
    }

    /**
     * @return mixed
     */
    public function getEmail_Spectateur()
    {
        return $this->Email_Spectateur;
    }

    /**
     * @param mixed $Email_Spectateur
     */
    public function setEmail_Spectateur($Email_Spectateur)
    {
        $this->Email_Spectateur = $Email_Spectateur;
    }

    /**
     * @return mixed
     */
    public function getCom_Spectateur()
    {
        return $this->Com_Spectateur;
    }

    /**
     * @param mixed $Com_Spectateur
     */
    public function setCom_Spectateur($Com_Spectateur)
    {
        $this->Com_Spectateur = $Com_Spectateur;
    }


}