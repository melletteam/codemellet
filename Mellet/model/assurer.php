<?php
/**
 * Created by IntelliJ IDEA.
 * User: bouffa
 * Date: 08-03-18
 * Time: 13:33
 * Modèle de l'objet Assurer dans la base de données
 */

class assurer
{
    private $ID_Acteur;
    private $ID_Spectacle;
    private $Role_Acteur;


    function __construct($arr = null)
    {
        if($arr != null){
            $this->fillObject($arr);
        }
    }
    private function fillObject($arr){
        foreach ($arr as $key => $value){
            $method = 'set'.ucfirst($key);
            if(method_exists($this,$method)){
                $this->$method($value);
            }
        }
    }

    /**
     * @return mixed
     */
    public function getID_Acteur()
    {
        return $this->ID_Acteur;
    }

    /**
     * @param mixed $ID_Acteur
     */
    public function setID_Acteur($ID_Acteur)
    {
        $this->ID_Acteur = $ID_Acteur;
    }

    /**
     * @return mixed
     */
    public function getID_Spectacle()
    {
        return $this->ID_Spectacle;
    }

    /**
     * @param mixed $ID_Spectacle
     */
    public function setID_Spectacle($ID_Spectacle)
    {
        $this->ID_Spectacle = $ID_Spectacle;
    }
    /**
     * @return mixed
     */
    public function getRole_Acteur()
    {
        return $this->Role_Acteur;
    }

    /**
     * @param mixed $Role_Acteur
     */
    public function setRole_Acteur($Role_Acteur)
    {
        $this->Role_Acteur = $Role_Acteur;
    }


}