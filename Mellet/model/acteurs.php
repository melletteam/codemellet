<?php
/**
 * Created by IntelliJ IDEA.
 * User: bouffa
 * Date: 08-03-18
 * Time: 13:33
 * Modèle de l'objet Acteur dans la base de données
 */

class acteurs
{
    private $ID_Acteur;
    private $Nom_Acteur;
    private $Prenom_Acteur;
    private $Statut_Acteurs;

    function __construct($arr = null)
    {
        if($arr != null){
            $this->fillObject($arr);
            //var_dump($this);
        }
    }

    private function fillObject($arr){
        foreach ($arr as $key => $value){
            $method = 'set'.ucfirst($key);
            if(method_exists($this,$method)){
                $this->$method($value);
            }
        }
    }

    /**
     * @return mixed
     */
    public function getID_Acteur()
    {
        return $this->ID_Acteur;
    }

    /**
     * @param mixed $ID_Acteur
     */
    public function setID_Acteur($ID_Acteur)
    {
        $this->ID_Acteur = $ID_Acteur;
    }

    /**
     * @return mixed
     */
    public function getNom_Acteur()
    {
        return $this->Nom_Acteur;
    }

    /**
     * @param mixed $Nom_Acteur
     */
    public function setNom_Acteur($Nom_Acteur)
    {
        $this->Nom_Acteur = $Nom_Acteur;
    }

    /**
     * @return mixed
     */
    public function getPrenom_Acteur()
    {
        return $this->Prenom_Acteur;
    }

    /**
     * @param mixed $Prenom_Acteur
     */
    public function setPrenom_Acteur($Prenom_Acteur)
    {
        $this->Prenom_Acteur = $Prenom_Acteur;
    }

    /**
     * @return mixed
     */
    public function getStatut_Acteurs()
    {
        return $this->Statut_Acteurs;
    }

    /**
     * @param mixed $Statut_Acteurs
     */
    public function setStatut_Acteurs($Statut_Acteurs)
    {
        $this->Statut_Acteurs = $Statut_Acteurs;
    }



}