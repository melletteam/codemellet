<?php
/**
 * Created by IntelliJ IDEA.
 * User: bouffa
 * Date: 08-03-18
 * Time: 13:33
 * Modèle de l'objet Chaise dans la base de données
 */

class chaise
{
    private $ID_Chaise;
    private $ID_Representation;
    private $Num_Lignes_Chaise;
    private $Num_Col_Chaise;
    private $Statut_Chaise;
    private $ID_Reservation;

    function __construct($arr = null)
    {
        if($arr != null){
            $this->fillObject($arr);
        }
    }
    private function fillObject($arr){
        foreach ($arr as $key => $value){
            $method = 'set'.ucfirst($key);
            if(method_exists($this,$method)){
                $this->$method($value);
            }
        }
    }
    /**
     * @return mixed
     */
    public function getID_Representation()
    {
        return $this->ID_Representation;
    }

    /**
     * @param mixed $ID_Representation
     */
    public function setID_Representation($ID_Representation)
    {
        $this->ID_Representation = $ID_Representation;
    }

    /**
     * @return mixed
     */
    public function getNum_Lignes_Chaise()
    {
        return $this->Num_Lignes_Chaise;
    }

    /**
     * @param mixed $Num_Lignes_Chaise
     */
    public function setNum_Lignes_Chaise($Num_Lignes_Chaise)
    {
        $this->Num_Lignes_Chaise = $Num_Lignes_Chaise;
    }

    /**
     * @return mixed
     */
    public function getNum_Col_Chaise()
    {
        return $this->Num_Col_Chaise;
    }

    /**
     * @param mixed $Num_Col_Chaise
     */
    public function setNum_Col_Chaise($Num_Col_Chaise)
    {
        $this->Num_Col_Chaise = $Num_Col_Chaise;
    }

    /**
     * @return mixed
     */
    public function getStatut_Chaise()
    {
        return $this->Statut_Chaise;
    }

    /**
     * @param mixed $Statut_Chaise
     */
    public function setStatut_Chaise($Statut_Chaise)
    {
        $this->Statut_Chaise = $Statut_Chaise;
    }

    /**
     * @return mixed
     */
    public function getID_Reservation()
    {
        return $this->ID_Reservation;
    }

    /**
     * @param mixed $ID_Reservation
     */
    public function setID_Reservation($ID_Reservation)
    {
        $this->ID_Reservation = $ID_Reservation;
    }

    /**
     * @return mixed
     */
    public function getIDChaise()
    {
        return $this->ID_Chaise;
    }

    /**
     * @param mixed $ID_Chaise
     */
    public function setIDChaise($ID_Chaise)
    {
        $this->ID_Chaise = $ID_Chaise;
    }



}