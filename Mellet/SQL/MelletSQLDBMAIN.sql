
create table Acteurs (
     ID_Acteur int(2) not null AUTO_INCREMENT,
     Nom_Acteur varchar(20) not null,
     Prenom_Acteur varchar(20) not null,
     Statut_Acteurs varchar(20) not null,
     constraint ID_Acteurs_ID primary key (ID_Acteur))
	 ENGINE=InnoDB;

create table Assurer (
     ID_Acteur int(2) not null AUTO_INCREMENT,
     ID_Spectacle int(2) not null,
	 Role_Acteur varchar(20) not null,
     constraint ID_Assurer_ID primary key (ID_Acteur, ID_Spectacle))
	 ENGINE=InnoDB;

create table Chaise (
     ID_Representation int(2) not null AUTO_INCREMENT,
     Num_Lignes_Chaise int(2) not null,
     Num_Col_Chaise int(2) not null,
     Statut_Chaise varchar(1) not null,
     ID_Reservation int(3),
     constraint ID_Chaise_ID primary key (ID_Representation, Num_Lignes_Chaise, Num_Col_Chaise))
	 ENGINE=InnoDB;

create table Representation (
     ID_Representation int(2) not null AUTO_INCREMENT,
     Date_Representation date not null,
     Nb_Lignes_Representation int(2) not null,
     Nb_Col_Representation int(2) not null,
     ID_Spectacle int(2) not null,
     constraint ID_Representation_ID primary key (ID_Representation))
	 ENGINE=InnoDB;

create table Reservation (
     ID_Reservation int(3) not null AUTO_INCREMENT,
     NbPlaces_Reser int(2) not null,
     ID_Representation int(2) not null,
     ID_Spectateur int(4) not null,
     constraint ID_Reservation_ID primary key (ID_Reservation))
	 ENGINE=InnoDB;

create table Spectacle (
     ID_Spectacle int(2) not null AUTO_INCREMENT,
     Titre_Spectacle varchar(55) not null,
     constraint ID_Spectacle_ID primary key (ID_Spectacle))
	 ENGINE=InnoDB;

create table Spectateur (
     ID_Spectateur int(4) not null AUTO_INCREMENT,
     Nom_Spectateur varchar(20) not null,
     Prenom_Spectateur varchar(20) not null,
     Rue_Spectateur varchar(55) not null,
     NumeroRue_Spectateur varchar(10) not null,
     CP_Spectateur int(6) not null,
     Ville_Spectateur varchar(55) not null,
     NumFix_Spectateur varchar(12) not null,
     NumGsm_Spectateur varchar(16) not null,
     Email_Spectateur varchar(55) not null,
     Com_Spectateur varchar(55) not null,
     constraint ID_Spectateur_ID primary key (ID_Spectateur))
	 ENGINE=InnoDB;

create table User (
     ID_User int(1) not null AUTO_INCREMENT,
     Login_User varchar(55) not null,
     Pswd_User varchar(55) not null,
     Statut_User char(1) not null,
     constraint ID_User_ID primary key (ID_User))
	 ENGINE=InnoDB;

alter table Acteurs add constraint ID_Acteurs_CHK
     check(exists(select * from Assurer
                  where Assurer.ID_Acteur = ID_Acteur)); 

alter table Assurer add constraint REF_Assur_Spect_FK
     foreign key (ID_Spectacle)
     references Spectacle(ID_Spectacle);

alter table Assurer add constraint EQU_Assur_Acteu
     foreign key (ID_Acteur)
     references Acteurs(ID_Acteur);

alter table Chaise add constraint REF_Chais_Repre
     foreign key (ID_Representation)
     references Representation(ID_Representation);

alter table Chaise add constraint EQU_Chais_Reser_FK
     foreign key (ID_Reservation)
     references Reservation(ID_Reservation);

alter table Representation add constraint REF_Repre_Spect_FK
     foreign key (ID_Spectacle)
     references Spectacle(ID_Spectacle);

alter table Reservation add constraint ID_Reservation_CHK
     check(exists(select * from Chaise
                  where Chaise.ID_Reservation = ID_Reservation)); 

alter table Reservation add constraint REF_Reser_Repre_FK
     foreign key (ID_Representation)
     references Representation(ID_Representation);

alter table Reservation add constraint REF_Reser_Spect_FK
     foreign key (ID_Spectateur)
     references Spectateur(ID_Spectateur);

create unique index ID_Acteurs_IND
     on Acteurs (ID_Acteur);

create unique index ID_Assurer_IND
     on Assurer (ID_Acteur, ID_Spectacle);

create index REF_Assur_Spect_IND
     on Assurer (ID_Spectacle);

create unique index ID_Chaise_IND
     on Chaise (ID_Representation, Num_Lignes_Chaise, Num_Col_Chaise);

create index EQU_Chais_Reser_IND
     on Chaise (ID_Reservation);

create unique index ID_Representation_IND
     on Representation (ID_Representation);

create index REF_Repre_Spect_IND
     on Representation (ID_Spectacle);

create unique index ID_Reservation_IND
     on Reservation (ID_Reservation);

create index REF_Reser_Repre_IND
     on Reservation (ID_Representation);

create index REF_Reser_Spect_IND
     on Reservation (ID_Spectateur);

create unique index ID_Spectacle_IND
     on Spectacle (ID_Spectacle);

create unique index ID_Spectateur_IND
     on Spectateur (ID_Spectateur);

create unique index ID_User_IND
     on User (ID_User);

