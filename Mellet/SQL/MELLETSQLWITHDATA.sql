-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- H�te : 127.0.0.1:3306
-- G�n�r� le :  lun. 30 avr. 2018 � 15:31
-- Version du serveur :  5.7.19
-- Version de PHP :  5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de donn�es :  `mellet`
--

-- --------------------------------------------------------

--
-- Structure de la table `acteurs`
--

DROP TABLE IF EXISTS `acteurs`;
CREATE TABLE IF NOT EXISTS `acteurs` (
  `ID_Acteur` int(2) NOT NULL AUTO_INCREMENT,
  `Nom_Acteur` varchar(20) NOT NULL,
  `Prenom_Acteur` varchar(20) NOT NULL,
  `Statut_Acteurs` varchar(20) NOT NULL,
  PRIMARY KEY (`ID_Acteur`),
  UNIQUE KEY `ID_Acteurs_IND` (`ID_Acteur`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

--
-- D�chargement des donn�es de la table `acteurs`
--

INSERT INTO `acteurs` (`ID_Acteur`, `Nom_Acteur`, `Prenom_Acteur`, `Statut_Acteurs`) VALUES
(1, 'Dewez', 'Richard', 'acteur'),
(2, 'Arcq', 'Dominique', 'acteur'),
(3, 'Dewez', 'Céline', 'acteur'),
(4, 'Gryspeert', 'Colin', 'acteur'),
(5, 'Jordens', 'Jessica', 'acteur'),
(6, 'Moser', 'Christian', 'acteur'),
(7, 'Spilette', 'Mélissa', 'acteur'),
(8, 'Galloy', 'Philippe', 'scene'),
(9, 'Palau', 'Jordan', 'regie'),
(10, 'Jacob', 'Cédric', 'regie'),
(11, 'De Buysser', 'Godelive', 'aide'),
(12, 'Malherbe', 'Pierre', 'decor'),
(13, 'Polus', 'Jean', 'decor'),
(14, 'Dewez', 'Louis', 'decor'),
(15, 'Dewez', 'Richard', 'decor'),
(16, 'Purnode', 'Yves', 'decor'),
(17, 'Tonneaux', 'Alain', 'decor'),
(18, 'Baquet', 'Bertin', 'decor'),
(19, 'Jacob', 'Cédric', 'decor'),
(20, 'Moser', 'Nicolas', 'acteur');

-- --------------------------------------------------------

--
-- Structure de la table `assurer`
--

DROP TABLE IF EXISTS `assurer`;
CREATE TABLE IF NOT EXISTS `assurer` (
  `ID_Acteur` int(2) NOT NULL AUTO_INCREMENT,
  `ID_Spectacle` int(2) NOT NULL,
  `Role_Acteur` varchar(20) NOT NULL,
  PRIMARY KEY (`ID_Acteur`,`ID_Spectacle`),
  UNIQUE KEY `ID_Assurer_IND` (`ID_Acteur`,`ID_Spectacle`),
  KEY `REF_Assur_Spect_IND` (`ID_Spectacle`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

--
-- D�chargement des donn�es de la table `assurer`
--

INSERT INTO `assurer` (`ID_Acteur`, `ID_Spectacle`, `Role_Acteur`) VALUES
(1, 1, 'Valère'),
(2, 1, 'Flore'),
(3, 1, 'Vanèssa'),
(4, 1, 'Bèrnard'),
(5, 1, 'Chantal'),
(6, 1, 'René'),
(7, 1, 'Erika'),
(8, 1, ''),
(9, 1, ''),
(10, 1, ''),
(11, 1, ''),
(12, 1, ''),
(13, 1, ''),
(14, 1, ''),
(15, 1, ''),
(16, 1, ''),
(17, 1, ''),
(18, 1, ''),
(19, 1, ''),
(20, 1, 'Filipe');

-- --------------------------------------------------------

--
-- Structure de la table `chaise`
--

DROP TABLE IF EXISTS `chaise`;
CREATE TABLE IF NOT EXISTS `chaise` (
  `ID_Chaise` int(10) NOT NULL AUTO_INCREMENT,
  `ID_Representation` int(2) NOT NULL,
  `Num_Lignes_Chaise` varchar(1) NOT NULL,
  `Num_Col_Chaise` int(2) NOT NULL,
  `Statut_Chaise` varchar(1) NOT NULL,
  `ID_Reservation` int(10) DEFAULT NULL,
  PRIMARY KEY (`ID_Chaise`),
  UNIQUE KEY `ID_Chaise_IND` (`ID_Chaise`),
  KEY `EQU_Chais_Reser_IND` (`ID_Reservation`),
  KEY `REF_Chais_Repre` (`ID_Representation`)
) ENGINE=InnoDB AUTO_INCREMENT=601 DEFAULT CHARSET=latin1;

--
-- D�chargement des donn�es de la table `chaise`
--

INSERT INTO `chaise` (`ID_Chaise`, `ID_Representation`, `Num_Lignes_Chaise`, `Num_Col_Chaise`, `Statut_Chaise`, `ID_Reservation`) VALUES
(1, 1, 'A', 1, 'D', NULL),
(2, 1, 'A', 2, 'D', NULL),
(3, 1, 'A', 3, 'D', NULL),
(4, 1, 'A', 4, 'D', NULL),
(5, 1, 'A', 5, 'D', NULL),
(6, 1, 'A', 6, 'D', NULL),
(7, 1, 'A', 7, 'D', NULL),
(8, 1, 'A', 8, 'D', NULL),
(9, 1, 'A', 9, 'D', NULL),
(10, 1, 'A', 10, 'D', NULL),
(11, 1, 'B', 1, 'D', NULL),
(12, 1, 'B', 2, 'D', NULL),
(13, 1, 'B', 3, 'D', NULL),
(14, 1, 'B', 4, 'R', 4),
(15, 1, 'B', 5, 'R', 4),
(16, 1, 'B', 6, 'R', 4),
(17, 1, 'B', 7, 'R', 4),
(18, 1, 'B', 8, 'D', NULL),
(19, 1, 'B', 9, 'D', NULL),
(20, 1, 'B', 10, 'D', NULL),
(21, 1, 'C', 1, 'D', NULL),
(22, 1, 'C', 2, 'D', NULL),
(23, 1, 'C', 3, 'D', NULL),
(24, 1, 'C', 4, 'R', 4),
(25, 1, 'C', 5, 'R', 4),
(26, 1, 'C', 6, 'R', 4),
(27, 1, 'C', 7, 'R', 4),
(28, 1, 'C', 8, 'D', NULL),
(29, 1, 'C', 9, 'D', NULL),
(30, 1, 'C', 10, 'D', NULL),
(31, 1, 'D', 1, 'D', NULL),
(32, 1, 'D', 2, 'D', NULL),
(33, 1, 'D', 3, 'D', NULL),
(34, 1, 'D', 4, 'D', NULL),
(35, 1, 'D', 5, 'D', NULL),
(36, 1, 'D', 6, 'D', NULL),
(37, 1, 'D', 7, 'D', NULL),
(38, 1, 'D', 8, 'D', NULL),
(39, 1, 'D', 9, 'D', NULL),
(40, 1, 'D', 10, 'D', NULL),
(41, 1, 'E', 1, 'D', NULL),
(42, 1, 'E', 2, 'D', NULL),
(43, 1, 'E', 3, 'D', NULL),
(44, 1, 'E', 4, 'O', 5),
(45, 1, 'E', 5, 'O', 5),
(46, 1, 'E', 6, 'O', 5),
(47, 1, 'E', 7, 'O', 5),
(48, 1, 'E', 8, 'D', NULL),
(49, 1, 'E', 9, 'D', NULL),
(50, 1, 'E', 10, 'D', NULL),
(51, 1, 'F', 1, 'D', NULL),
(52, 1, 'F', 2, 'D', NULL),
(53, 1, 'F', 3, 'D', NULL),
(54, 1, 'F', 4, 'O', 5),
(55, 1, 'F', 5, 'O', 5),
(56, 1, 'F', 6, 'O', 5),
(57, 1, 'F', 7, 'O', 5),
(58, 1, 'F', 8, 'D', NULL),
(59, 1, 'F', 9, 'D', NULL),
(60, 1, 'F', 10, 'D', NULL),
(61, 1, 'G', 1, 'D', NULL),
(62, 1, 'G', 2, 'D', NULL),
(63, 1, 'G', 3, 'D', NULL),
(64, 1, 'G', 4, 'O', 5),
(65, 1, 'G', 5, 'O', 5),
(66, 1, 'G', 6, 'O', 5),
(67, 1, 'G', 7, 'O', 5),
(68, 1, 'G', 8, 'D', NULL),
(69, 1, 'G', 9, 'D', NULL),
(70, 1, 'G', 10, 'D', NULL),
(71, 1, 'H', 1, 'D', NULL),
(72, 1, 'H', 2, 'D', NULL),
(73, 1, 'H', 3, 'D', NULL),
(74, 1, 'H', 4, 'D', NULL),
(75, 1, 'H', 5, 'D', NULL),
(76, 1, 'H', 6, 'D', NULL),
(77, 1, 'H', 7, 'D', NULL),
(78, 1, 'H', 8, 'D', NULL),
(79, 1, 'H', 9, 'D', NULL),
(80, 1, 'H', 10, 'D', NULL),
(81, 1, 'I', 1, 'D', NULL),
(82, 1, 'I', 2, 'D', NULL),
(83, 1, 'I', 3, 'D', NULL),
(84, 1, 'I', 4, 'D', NULL),
(85, 1, 'I', 5, 'D', NULL),
(86, 1, 'I', 6, 'D', NULL),
(87, 1, 'I', 7, 'D', NULL),
(88, 1, 'I', 8, 'D', NULL),
(89, 1, 'I', 9, 'D', NULL),
(90, 1, 'I', 10, 'D', NULL),
(91, 1, 'J', 1, 'D', NULL),
(92, 1, 'J', 2, 'D', NULL),
(93, 1, 'J', 3, 'D', NULL),
(94, 1, 'J', 4, 'D', NULL),
(95, 1, 'J', 5, 'D', NULL),
(96, 1, 'J', 6, 'D', NULL),
(97, 1, 'J', 7, 'D', NULL),
(98, 1, 'J', 8, 'D', NULL),
(99, 1, 'J', 9, 'D', NULL),
(100, 1, 'J', 10, 'D', NULL),
(101, 2, 'A', 1, 'D', NULL),
(102, 2, 'A', 2, 'D', NULL),
(103, 2, 'A', 3, 'D', NULL),
(104, 2, 'A', 4, 'D', NULL),
(105, 2, 'A', 5, 'D', NULL),
(106, 2, 'A', 6, 'D', NULL),
(107, 2, 'A', 7, 'D', NULL),
(108, 2, 'A', 8, 'D', NULL),
(109, 2, 'A', 9, 'D', NULL),
(110, 2, 'A', 10, 'D', NULL),
(111, 2, 'B', 1, 'D', NULL),
(112, 2, 'B', 2, 'D', NULL),
(113, 2, 'B', 3, 'D', NULL),
(114, 2, 'B', 4, 'D', NULL),
(115, 2, 'B', 5, 'D', NULL),
(116, 2, 'B', 6, 'D', NULL),
(117, 2, 'B', 7, 'D', NULL),
(118, 2, 'B', 8, 'D', NULL),
(119, 2, 'B', 9, 'D', NULL),
(120, 2, 'B', 10, 'D', NULL),
(121, 2, 'C', 1, 'D', NULL),
(122, 2, 'C', 2, 'D', NULL),
(123, 2, 'C', 3, 'D', NULL),
(124, 2, 'C', 4, 'D', NULL),
(125, 2, 'C', 5, 'D', NULL),
(126, 2, 'C', 6, 'D', NULL),
(127, 2, 'C', 7, 'D', NULL),
(128, 2, 'C', 8, 'D', NULL),
(129, 2, 'C', 9, 'D', NULL),
(130, 2, 'C', 10, 'D', NULL),
(131, 2, 'D', 1, 'D', NULL),
(132, 2, 'D', 2, 'D', NULL),
(133, 2, 'D', 3, 'D', NULL),
(134, 2, 'D', 4, 'D', NULL),
(135, 2, 'D', 5, 'D', NULL),
(136, 2, 'D', 6, 'D', NULL),
(137, 2, 'D', 7, 'D', NULL),
(138, 2, 'D', 8, 'D', NULL),
(139, 2, 'D', 9, 'D', NULL),
(140, 2, 'D', 10, 'D', NULL),
(141, 2, 'E', 1, 'D', NULL),
(142, 2, 'E', 2, 'D', NULL),
(143, 2, 'E', 3, 'D', NULL),
(144, 2, 'E', 4, 'D', NULL),
(145, 2, 'E', 5, 'D', NULL),
(146, 2, 'E', 6, 'D', NULL),
(147, 2, 'E', 7, 'D', NULL),
(148, 2, 'E', 8, 'D', NULL),
(149, 2, 'E', 9, 'D', NULL),
(150, 2, 'E', 10, 'D', NULL),
(151, 2, 'F', 1, 'D', NULL),
(152, 2, 'F', 2, 'D', NULL),
(153, 2, 'F', 3, 'D', NULL),
(154, 2, 'F', 4, 'D', NULL),
(155, 2, 'F', 5, 'D', NULL),
(156, 2, 'F', 6, 'D', NULL),
(157, 2, 'F', 7, 'D', NULL),
(158, 2, 'F', 8, 'D', NULL),
(159, 2, 'F', 9, 'D', NULL),
(160, 2, 'F', 10, 'D', NULL),
(161, 2, 'G', 1, 'D', NULL),
(162, 2, 'G', 2, 'D', NULL),
(163, 2, 'G', 3, 'D', NULL),
(164, 2, 'G', 4, 'D', NULL),
(165, 2, 'G', 5, 'D', NULL),
(166, 2, 'G', 6, 'D', NULL),
(167, 2, 'G', 7, 'D', NULL),
(168, 2, 'G', 8, 'D', NULL),
(169, 2, 'G', 9, 'D', NULL),
(170, 2, 'G', 10, 'D', NULL),
(171, 2, 'H', 1, 'D', NULL),
(172, 2, 'H', 2, 'D', NULL),
(173, 2, 'H', 3, 'D', NULL),
(174, 2, 'H', 4, 'D', NULL),
(175, 2, 'H', 5, 'D', NULL),
(176, 2, 'H', 6, 'D', NULL),
(177, 2, 'H', 7, 'D', NULL),
(178, 2, 'H', 8, 'D', NULL),
(179, 2, 'H', 9, 'D', NULL),
(180, 2, 'H', 10, 'D', NULL),
(181, 2, 'I', 1, 'D', NULL),
(182, 2, 'I', 2, 'D', NULL),
(183, 2, 'I', 3, 'D', NULL),
(184, 2, 'I', 4, 'D', NULL),
(185, 2, 'I', 5, 'D', NULL),
(186, 2, 'I', 6, 'D', NULL),
(187, 2, 'I', 7, 'D', NULL),
(188, 2, 'I', 8, 'D', NULL),
(189, 2, 'I', 9, 'D', NULL),
(190, 2, 'I', 10, 'D', NULL),
(191, 2, 'J', 1, 'D', NULL),
(192, 2, 'J', 2, 'D', NULL),
(193, 2, 'J', 3, 'D', NULL),
(194, 2, 'J', 4, 'D', NULL),
(195, 2, 'J', 5, 'D', NULL),
(196, 2, 'J', 6, 'D', NULL),
(197, 2, 'J', 7, 'D', NULL),
(198, 2, 'J', 8, 'D', NULL),
(199, 2, 'J', 9, 'D', NULL),
(200, 2, 'J', 10, 'D', NULL),
(201, 3, 'A', 1, 'D', NULL),
(202, 3, 'A', 2, 'D', NULL),
(203, 3, 'A', 3, 'D', NULL),
(204, 3, 'A', 4, 'D', NULL),
(205, 3, 'A', 5, 'D', NULL),
(206, 3, 'A', 6, 'D', NULL),
(207, 3, 'A', 7, 'D', NULL),
(208, 3, 'A', 8, 'D', NULL),
(209, 3, 'A', 9, 'D', NULL),
(210, 3, 'A', 10, 'D', NULL),
(211, 3, 'B', 1, 'D', NULL),
(212, 3, 'B', 2, 'D', NULL),
(213, 3, 'B', 3, 'D', NULL),
(214, 3, 'B', 4, 'D', NULL),
(215, 3, 'B', 5, 'D', NULL),
(216, 3, 'B', 6, 'D', NULL),
(217, 3, 'B', 7, 'D', NULL),
(218, 3, 'B', 8, 'D', NULL),
(219, 3, 'B', 9, 'D', NULL),
(220, 3, 'B', 10, 'D', NULL),
(221, 3, 'C', 1, 'D', NULL),
(222, 3, 'C', 2, 'D', NULL),
(223, 3, 'C', 3, 'D', NULL),
(224, 3, 'C', 4, 'D', NULL),
(225, 3, 'C', 5, 'D', NULL),
(226, 3, 'C', 6, 'D', NULL),
(227, 3, 'C', 7, 'D', NULL),
(228, 3, 'C', 8, 'D', NULL),
(229, 3, 'C', 9, 'D', NULL),
(230, 3, 'C', 10, 'D', NULL),
(231, 3, 'D', 1, 'D', NULL),
(232, 3, 'D', 2, 'D', NULL),
(233, 3, 'D', 3, 'D', NULL),
(234, 3, 'D', 4, 'D', NULL),
(235, 3, 'D', 5, 'D', NULL),
(236, 3, 'D', 6, 'D', NULL),
(237, 3, 'D', 7, 'D', NULL),
(238, 3, 'D', 8, 'D', NULL),
(239, 3, 'D', 9, 'D', NULL),
(240, 3, 'D', 10, 'D', NULL),
(241, 3, 'E', 1, 'D', NULL),
(242, 3, 'E', 2, 'D', NULL),
(243, 3, 'E', 3, 'D', NULL),
(244, 3, 'E', 4, 'D', NULL),
(245, 3, 'E', 5, 'D', NULL),
(246, 3, 'E', 6, 'D', NULL),
(247, 3, 'E', 7, 'D', NULL),
(248, 3, 'E', 8, 'D', NULL),
(249, 3, 'E', 9, 'D', NULL),
(250, 3, 'E', 10, 'D', NULL),
(251, 3, 'F', 1, 'D', NULL),
(252, 3, 'F', 2, 'D', NULL),
(253, 3, 'F', 3, 'D', NULL),
(254, 3, 'F', 4, 'D', NULL),
(255, 3, 'F', 5, 'D', NULL),
(256, 3, 'F', 6, 'D', NULL),
(257, 3, 'F', 7, 'D', NULL),
(258, 3, 'F', 8, 'D', NULL),
(259, 3, 'F', 9, 'D', NULL),
(260, 3, 'F', 10, 'D', NULL),
(261, 3, 'G', 1, 'D', NULL),
(262, 3, 'G', 2, 'D', NULL),
(263, 3, 'G', 3, 'D', NULL),
(264, 3, 'G', 4, 'D', NULL),
(265, 3, 'G', 5, 'D', NULL),
(266, 3, 'G', 6, 'D', NULL),
(267, 3, 'G', 7, 'D', NULL),
(268, 3, 'G', 8, 'D', NULL),
(269, 3, 'G', 9, 'D', NULL),
(270, 3, 'G', 10, 'D', NULL),
(271, 3, 'H', 1, 'D', NULL),
(272, 3, 'H', 2, 'D', NULL),
(273, 3, 'H', 3, 'D', NULL),
(274, 3, 'H', 4, 'D', NULL),
(275, 3, 'H', 5, 'D', NULL),
(276, 3, 'H', 6, 'D', NULL),
(277, 3, 'H', 7, 'D', NULL),
(278, 3, 'H', 8, 'D', NULL),
(279, 3, 'H', 9, 'D', NULL),
(280, 3, 'H', 10, 'D', NULL),
(281, 3, 'I', 1, 'D', NULL),
(282, 3, 'I', 2, 'D', NULL),
(283, 3, 'I', 3, 'D', NULL),
(284, 3, 'I', 4, 'D', NULL),
(285, 3, 'I', 5, 'D', NULL),
(286, 3, 'I', 6, 'D', NULL),
(287, 3, 'I', 7, 'D', NULL),
(288, 3, 'I', 8, 'D', NULL),
(289, 3, 'I', 9, 'D', NULL),
(290, 3, 'I', 10, 'D', NULL),
(291, 3, 'J', 1, 'D', NULL),
(292, 3, 'J', 2, 'D', NULL),
(293, 3, 'J', 3, 'D', NULL),
(294, 3, 'J', 4, 'D', NULL),
(295, 3, 'J', 5, 'D', NULL),
(296, 3, 'J', 6, 'D', NULL),
(297, 3, 'J', 7, 'D', NULL),
(298, 3, 'J', 8, 'D', NULL),
(299, 3, 'J', 9, 'D', NULL),
(300, 3, 'J', 10, 'D', NULL),
(301, 4, 'A', 1, 'D', NULL),
(302, 4, 'A', 2, 'D', NULL),
(303, 4, 'A', 3, 'D', NULL),
(304, 4, 'A', 4, 'D', NULL),
(305, 4, 'A', 5, 'D', NULL),
(306, 4, 'A', 6, 'D', NULL),
(307, 4, 'A', 7, 'D', NULL),
(308, 4, 'A', 8, 'D', NULL),
(309, 4, 'A', 9, 'D', NULL),
(310, 4, 'A', 10, 'D', NULL),
(311, 4, 'B', 1, 'D', NULL),
(312, 4, 'B', 2, 'D', NULL),
(313, 4, 'B', 3, 'D', NULL),
(314, 4, 'B', 4, 'R', 6),
(315, 4, 'B', 5, 'R', 6),
(316, 4, 'B', 6, 'R', 6),
(317, 4, 'B', 7, 'D', NULL),
(318, 4, 'B', 8, 'D', NULL),
(319, 4, 'B', 9, 'D', NULL),
(320, 4, 'B', 10, 'D', NULL),
(321, 4, 'C', 1, 'D', NULL),
(322, 4, 'C', 2, 'D', NULL),
(323, 4, 'C', 3, 'R', 6),
(324, 4, 'C', 4, 'R', 6),
(325, 4, 'C', 5, 'R', 6),
(326, 4, 'C', 6, 'R', 6),
(327, 4, 'C', 7, 'R', 6),
(328, 4, 'C', 8, 'D', NULL),
(329, 4, 'C', 9, 'D', NULL),
(330, 4, 'C', 10, 'D', NULL),
(331, 4, 'D', 1, 'D', NULL),
(332, 4, 'D', 2, 'D', NULL),
(333, 4, 'D', 3, 'R', 6),
(334, 4, 'D', 4, 'R', 6),
(335, 4, 'D', 5, 'R', 6),
(336, 4, 'D', 6, 'R', 6),
(337, 4, 'D', 7, 'R', 6),
(338, 4, 'D', 8, 'D', NULL),
(339, 4, 'D', 9, 'D', NULL),
(340, 4, 'D', 10, 'D', NULL),
(341, 4, 'E', 1, 'D', NULL),
(342, 4, 'E', 2, 'D', NULL),
(343, 4, 'E', 3, 'D', NULL),
(344, 4, 'E', 4, 'R', 6),
(345, 4, 'E', 5, 'R', 6),
(346, 4, 'E', 6, 'R', 6),
(347, 4, 'E', 7, 'D', NULL),
(348, 4, 'E', 8, 'D', NULL),
(349, 4, 'E', 9, 'D', NULL),
(350, 4, 'E', 10, 'D', NULL),
(351, 4, 'F', 1, 'D', NULL),
(352, 4, 'F', 2, 'D', NULL),
(353, 4, 'F', 3, 'D', NULL),
(354, 4, 'F', 4, 'D', NULL),
(355, 4, 'F', 5, 'D', NULL),
(356, 4, 'F', 6, 'D', NULL),
(357, 4, 'F', 7, 'D', NULL),
(358, 4, 'F', 8, 'D', NULL),
(359, 4, 'F', 9, 'D', NULL),
(360, 4, 'F', 10, 'D', NULL),
(361, 4, 'G', 1, 'D', NULL),
(362, 4, 'G', 2, 'D', NULL),
(363, 4, 'G', 3, 'D', NULL),
(364, 4, 'G', 4, 'D', NULL),
(365, 4, 'G', 5, 'D', NULL),
(366, 4, 'G', 6, 'D', NULL),
(367, 4, 'G', 7, 'D', NULL),
(368, 4, 'G', 8, 'D', NULL),
(369, 4, 'G', 9, 'D', NULL),
(370, 4, 'G', 10, 'D', NULL),
(371, 4, 'H', 1, 'D', NULL),
(372, 4, 'H', 2, 'D', NULL),
(373, 4, 'H', 3, 'D', NULL),
(374, 4, 'H', 4, 'D', NULL),
(375, 4, 'H', 5, 'D', NULL),
(376, 4, 'H', 6, 'D', NULL),
(377, 4, 'H', 7, 'D', NULL),
(378, 4, 'H', 8, 'D', NULL),
(379, 4, 'H', 9, 'D', NULL),
(380, 4, 'H', 10, 'D', NULL),
(381, 4, 'I', 1, 'D', NULL),
(382, 4, 'I', 2, 'D', NULL),
(383, 4, 'I', 3, 'D', NULL),
(384, 4, 'I', 4, 'D', NULL),
(385, 4, 'I', 5, 'D', NULL),
(386, 4, 'I', 6, 'D', NULL),
(387, 4, 'I', 7, 'D', NULL),
(388, 4, 'I', 8, 'D', NULL),
(389, 4, 'I', 9, 'D', NULL),
(390, 4, 'I', 10, 'D', NULL),
(391, 4, 'J', 1, 'D', NULL),
(392, 4, 'J', 2, 'D', NULL),
(393, 4, 'J', 3, 'D', NULL),
(394, 4, 'J', 4, 'D', NULL),
(395, 4, 'J', 5, 'D', NULL),
(396, 4, 'J', 6, 'D', NULL),
(397, 4, 'J', 7, 'D', NULL),
(398, 4, 'J', 8, 'D', NULL),
(399, 4, 'J', 9, 'D', NULL),
(400, 4, 'J', 10, 'D', NULL),
(401, 5, 'A', 1, 'D', NULL),
(402, 5, 'A', 2, 'D', NULL),
(403, 5, 'A', 3, 'D', NULL),
(404, 5, 'A', 4, 'D', NULL),
(405, 5, 'A', 5, 'D', NULL),
(406, 5, 'A', 6, 'D', NULL),
(407, 5, 'A', 7, 'D', NULL),
(408, 5, 'A', 8, 'D', NULL),
(409, 5, 'A', 9, 'D', NULL),
(410, 5, 'A', 10, 'D', NULL),
(411, 5, 'B', 1, 'D', NULL),
(412, 5, 'B', 2, 'D', NULL),
(413, 5, 'B', 3, 'D', NULL),
(414, 5, 'B', 4, 'D', NULL),
(415, 5, 'B', 5, 'D', NULL),
(416, 5, 'B', 6, 'D', NULL),
(417, 5, 'B', 7, 'D', NULL),
(418, 5, 'B', 8, 'D', NULL),
(419, 5, 'B', 9, 'D', NULL),
(420, 5, 'B', 10, 'D', NULL),
(421, 5, 'C', 1, 'D', NULL),
(422, 5, 'C', 2, 'D', NULL),
(423, 5, 'C', 3, 'D', NULL),
(424, 5, 'C', 4, 'D', NULL),
(425, 5, 'C', 5, 'D', NULL),
(426, 5, 'C', 6, 'D', NULL),
(427, 5, 'C', 7, 'D', NULL),
(428, 5, 'C', 8, 'D', NULL),
(429, 5, 'C', 9, 'D', NULL),
(430, 5, 'C', 10, 'D', NULL),
(431, 5, 'D', 1, 'D', NULL),
(432, 5, 'D', 2, 'D', NULL),
(433, 5, 'D', 3, 'D', NULL),
(434, 5, 'D', 4, 'D', NULL),
(435, 5, 'D', 5, 'D', NULL),
(436, 5, 'D', 6, 'D', NULL),
(437, 5, 'D', 7, 'D', NULL),
(438, 5, 'D', 8, 'D', NULL),
(439, 5, 'D', 9, 'D', NULL),
(440, 5, 'D', 10, 'D', NULL),
(441, 5, 'E', 1, 'D', NULL),
(442, 5, 'E', 2, 'D', NULL),
(443, 5, 'E', 3, 'D', NULL),
(444, 5, 'E', 4, 'D', NULL),
(445, 5, 'E', 5, 'D', NULL),
(446, 5, 'E', 6, 'D', NULL),
(447, 5, 'E', 7, 'D', NULL),
(448, 5, 'E', 8, 'D', NULL),
(449, 5, 'E', 9, 'D', NULL),
(450, 5, 'E', 10, 'D', NULL),
(451, 5, 'F', 1, 'D', NULL),
(452, 5, 'F', 2, 'D', NULL),
(453, 5, 'F', 3, 'D', NULL),
(454, 5, 'F', 4, 'D', NULL),
(455, 5, 'F', 5, 'D', NULL),
(456, 5, 'F', 6, 'D', NULL),
(457, 5, 'F', 7, 'D', NULL),
(458, 5, 'F', 8, 'D', NULL),
(459, 5, 'F', 9, 'D', NULL),
(460, 5, 'F', 10, 'D', NULL),
(461, 5, 'G', 1, 'D', NULL),
(462, 5, 'G', 2, 'D', NULL),
(463, 5, 'G', 3, 'D', NULL),
(464, 5, 'G', 4, 'D', NULL),
(465, 5, 'G', 5, 'D', NULL),
(466, 5, 'G', 6, 'D', NULL),
(467, 5, 'G', 7, 'D', NULL),
(468, 5, 'G', 8, 'D', NULL),
(469, 5, 'G', 9, 'D', NULL),
(470, 5, 'G', 10, 'D', NULL),
(471, 5, 'H', 1, 'D', NULL),
(472, 5, 'H', 2, 'D', NULL),
(473, 5, 'H', 3, 'D', NULL),
(474, 5, 'H', 4, 'D', NULL),
(475, 5, 'H', 5, 'D', NULL),
(476, 5, 'H', 6, 'D', NULL),
(477, 5, 'H', 7, 'D', NULL),
(478, 5, 'H', 8, 'D', NULL),
(479, 5, 'H', 9, 'D', NULL),
(480, 5, 'H', 10, 'D', NULL),
(481, 5, 'I', 1, 'D', NULL),
(482, 5, 'I', 2, 'D', NULL),
(483, 5, 'I', 3, 'D', NULL),
(484, 5, 'I', 4, 'D', NULL),
(485, 5, 'I', 5, 'D', NULL),
(486, 5, 'I', 6, 'D', NULL),
(487, 5, 'I', 7, 'D', NULL),
(488, 5, 'I', 8, 'D', NULL),
(489, 5, 'I', 9, 'D', NULL),
(490, 5, 'I', 10, 'D', NULL),
(491, 5, 'J', 1, 'D', NULL),
(492, 5, 'J', 2, 'D', NULL),
(493, 5, 'J', 3, 'D', NULL),
(494, 5, 'J', 4, 'D', NULL),
(495, 5, 'J', 5, 'D', NULL),
(496, 5, 'J', 6, 'D', NULL),
(497, 5, 'J', 7, 'D', NULL),
(498, 5, 'J', 8, 'D', NULL),
(499, 5, 'J', 9, 'D', NULL),
(500, 5, 'J', 10, 'D', NULL),
(501, 6, 'A', 1, 'O', 1),
(502, 6, 'A', 2, 'O', 1),
(503, 6, 'A', 3, 'O', 1),
(504, 6, 'A', 4, 'O', 1),
(505, 6, 'A', 5, 'D', NULL),
(506, 6, 'A', 6, 'D', NULL),
(507, 6, 'A', 7, 'D', NULL),
(508, 6, 'A', 8, 'D', NULL),
(509, 6, 'A', 9, 'D', NULL),
(510, 6, 'A', 10, 'D', NULL),
(511, 6, 'B', 1, 'O', 1),
(512, 6, 'B', 2, 'O', 1),
(513, 6, 'B', 3, 'D', NULL),
(514, 6, 'B', 4, 'D', NULL),
(515, 6, 'B', 5, 'D', NULL),
(516, 6, 'B', 6, 'D', NULL),
(517, 6, 'B', 7, 'D', NULL),
(518, 6, 'B', 8, 'D', NULL),
(519, 6, 'B', 9, 'D', NULL),
(520, 6, 'B', 10, 'D', NULL),
(521, 6, 'C', 1, 'D', NULL),
(522, 6, 'C', 2, 'D', NULL),
(523, 6, 'C', 3, 'D', NULL),
(524, 6, 'C', 4, 'D', NULL),
(525, 6, 'C', 5, 'R', 2),
(526, 6, 'C', 6, 'R', 2),
(527, 6, 'C', 7, 'R', 2),
(528, 6, 'C', 8, 'D', NULL),
(529, 6, 'C', 9, 'D', NULL),
(530, 6, 'C', 10, 'D', NULL),
(531, 6, 'D', 1, 'D', NULL),
(532, 6, 'D', 2, 'D', NULL),
(533, 6, 'D', 3, 'D', NULL),
(534, 6, 'D', 4, 'D', NULL),
(535, 6, 'D', 5, 'D', NULL),
(536, 6, 'D', 6, 'D', NULL),
(537, 6, 'D', 7, 'D', NULL),
(538, 6, 'D', 8, 'D', NULL),
(539, 6, 'D', 9, 'D', NULL),
(540, 6, 'D', 10, 'D', NULL),
(541, 6, 'E', 1, 'D', NULL),
(542, 6, 'E', 2, 'D', NULL),
(543, 6, 'E', 3, 'D', NULL),
(544, 6, 'E', 4, 'D', NULL),
(545, 6, 'E', 5, 'D', NULL),
(546, 6, 'E', 6, 'D', NULL),
(547, 6, 'E', 7, 'D', NULL),
(548, 6, 'E', 8, 'D', NULL),
(549, 6, 'E', 9, 'D', NULL),
(550, 6, 'E', 10, 'D', NULL),
(551, 6, 'F', 1, 'O', 3),
(552, 6, 'F', 2, 'O', 3),
(553, 6, 'F', 3, 'O', 3),
(554, 6, 'F', 4, 'O', 3),
(555, 6, 'F', 5, 'O', 3),
(556, 6, 'F', 6, 'O', 3),
(557, 6, 'F', 7, 'O', 3),
(558, 6, 'F', 8, 'D', NULL),
(559, 6, 'F', 9, 'D', NULL),
(560, 6, 'F', 10, 'D', NULL),
(561, 6, 'G', 1, 'D', NULL),
(562, 6, 'G', 2, 'D', NULL),
(563, 6, 'G', 3, 'D', NULL),
(564, 6, 'G', 4, 'D', NULL),
(565, 6, 'G', 5, 'D', NULL),
(566, 6, 'G', 6, 'D', NULL),
(567, 6, 'G', 7, 'D', NULL),
(568, 6, 'G', 8, 'D', NULL),
(569, 6, 'G', 9, 'D', NULL),
(570, 6, 'G', 10, 'D', NULL),
(571, 6, 'H', 1, 'D', NULL),
(572, 6, 'H', 2, 'D', NULL),
(573, 6, 'H', 3, 'D', NULL),
(574, 6, 'H', 4, 'D', NULL),
(575, 6, 'H', 5, 'D', NULL),
(576, 6, 'H', 6, 'D', NULL),
(577, 6, 'H', 7, 'D', NULL),
(578, 6, 'H', 8, 'D', NULL),
(579, 6, 'H', 9, 'D', NULL),
(580, 6, 'H', 10, 'D', NULL),
(581, 6, 'I', 1, 'D', NULL),
(582, 6, 'I', 2, 'D', NULL),
(583, 6, 'I', 3, 'D', NULL),
(584, 6, 'I', 4, 'D', NULL),
(585, 6, 'I', 5, 'D', NULL),
(586, 6, 'I', 6, 'D', NULL),
(587, 6, 'I', 7, 'D', NULL),
(588, 6, 'I', 8, 'D', NULL),
(589, 6, 'I', 9, 'D', NULL),
(590, 6, 'I', 10, 'D', NULL),
(591, 6, 'J', 1, 'D', NULL),
(592, 6, 'J', 2, 'D', NULL),
(593, 6, 'J', 3, 'D', NULL),
(594, 6, 'J', 4, 'D', NULL),
(595, 6, 'J', 5, 'D', NULL),
(596, 6, 'J', 6, 'D', NULL),
(597, 6, 'J', 7, 'D', NULL),
(598, 6, 'J', 8, 'D', NULL),
(599, 6, 'J', 9, 'D', NULL),
(600, 6, 'J', 10, 'D', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `representation`
--

DROP TABLE IF EXISTS `representation`;
CREATE TABLE IF NOT EXISTS `representation` (
  `ID_Representation` int(2) NOT NULL AUTO_INCREMENT,
  `Date_Representation` datetime NOT NULL,
  `Nb_Lignes_Representation` int(2) NOT NULL,
  `Nb_Col_Representation` int(2) NOT NULL,
  `ID_Spectacle` int(2) NOT NULL,
  PRIMARY KEY (`ID_Representation`),
  UNIQUE KEY `ID_Representation_IND` (`ID_Representation`),
  KEY `REF_Repre_Spect_IND` (`ID_Spectacle`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- D�chargement des donn�es de la table `representation`
--

INSERT INTO `representation` (`ID_Representation`, `Date_Representation`, `Nb_Lignes_Representation`, `Nb_Col_Representation`, `ID_Spectacle`) VALUES
(1, '2018-04-20 19:30:00', 10, 10, 1),
(2, '2018-04-21 19:30:00', 10, 10, 1),
(3, '2018-04-22 15:30:00', 10, 10, 1),
(4, '2018-04-27 19:30:00', 10, 10, 1),
(5, '2018-04-28 19:30:00', 10, 10, 1),
(6, '2018-04-29 15:30:00', 10, 10, 1);

-- --------------------------------------------------------

--
-- Structure de la table `reservation`
--

DROP TABLE IF EXISTS `reservation`;
CREATE TABLE IF NOT EXISTS `reservation` (
  `ID_Reservation` int(3) NOT NULL AUTO_INCREMENT,
  `NbPlaces_Reser` int(2) NOT NULL,
  `ID_Representation` int(2) NOT NULL,
  `ID_Spectateur` int(4) NOT NULL,
  PRIMARY KEY (`ID_Reservation`),
  UNIQUE KEY `ID_Reservation_IND` (`ID_Reservation`),
  KEY `REF_Reser_Repre_IND` (`ID_Representation`),
  KEY `REF_Reser_Spect_IND` (`ID_Spectateur`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- D�chargement des donn�es de la table `reservation`
--

INSERT INTO `reservation` (`ID_Reservation`, `NbPlaces_Reser`, `ID_Representation`, `ID_Spectateur`) VALUES
(1, 6, 6, 1),
(2, 3, 6, 2),
(3, 7, 6, 5),
(4, 8, 1, 6),
(5, 12, 1, 4),
(6, 16, 4, 3);

-- --------------------------------------------------------

--
-- Structure de la table `spectacle`
--

DROP TABLE IF EXISTS `spectacle`;
CREATE TABLE IF NOT EXISTS `spectacle` (
  `ID_Spectacle` int(2) NOT NULL AUTO_INCREMENT,
  `Titre_Spectacle` varchar(55) NOT NULL,
  `Prix_Spectacle` float NOT NULL,
  PRIMARY KEY (`ID_Spectacle`),
  UNIQUE KEY `ID_Spectacle_IND` (`ID_Spectacle`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- D�chargement des donn�es de la table `spectacle`
--

INSERT INTO `spectacle` (`ID_Spectacle`, `Titre_Spectacle`, `Prix_Spectacle`) VALUES
(1, 'In si pôjêre lodji !', 7);

-- --------------------------------------------------------

--
-- Structure de la table `spectateur`
--

DROP TABLE IF EXISTS `spectateur`;
CREATE TABLE IF NOT EXISTS `spectateur` (
  `ID_Spectateur` int(4) NOT NULL AUTO_INCREMENT,
  `Nom_Spectateur` varchar(20) NOT NULL,
  `Prenom_Spectateur` varchar(20) NOT NULL,
  `Rue_Spectateur` varchar(55) NOT NULL,
  `NumeroRue_Spectateur` varchar(10) NOT NULL,
  `CP_Spectateur` int(6) NOT NULL,
  `Ville_Spectateur` varchar(55) NOT NULL,
  `NumFix_Spectateur` varchar(12) NOT NULL,
  `NumGsm_Spectateur` varchar(16) NOT NULL,
  `Email_Spectateur` varchar(55) NOT NULL,
  `Com_Spectateur` varchar(55) NOT NULL,
  PRIMARY KEY (`ID_Spectateur`),
  UNIQUE KEY `ID_Spectateur_IND` (`ID_Spectateur`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- D�chargement des donn�es de la table `spectateur`
--

INSERT INTO `spectateur` (`ID_Spectateur`, `Nom_Spectateur`, `Prenom_Spectateur`, `Rue_Spectateur`, `NumeroRue_Spectateur`, `CP_Spectateur`, `Ville_Spectateur`, `NumFix_Spectateur`, `NumGsm_Spectateur`, `Email_Spectateur`, `Com_Spectateur`) VALUES
(1, 'Bouffa', 'Diego', 'rue du berceau', '29', 1495, 'Marbais', '', '+32498593774', 'diego.bouffa@gmail.com', 'lol'),
(2, 'Deridder', 'Carine', 'rue du berceau', '29', 1495, 'Marbais', '', '+32472683563', 'caci.dr@hotmail.com', 'Mme'),
(3, 'Minadeo', 'Robin', 'rue de la Paix', '69', 9000, 'charleroi', '', '0495756841', 'robin.minadeo@gmail.com', ''),
(4, 'Minad&eacute;o', 'nicolas', 'rue de la league', '56', 6000, 'namur', '071696969', '', 'nico.M@gmail.com', ''),
(5, 'Paesmans', 'Maxima', 'rue de la grosse boule', '14', 1495, 'Sart Dames Avelines', '', '', 'maxi.pa@gmail.com', ''),
(6, 'Foulon', 'Josua', 'avenue G&eacute;n&eacute;ral Jacques', '23B', 6000, 'Namur', '064582532', '', 'jojo@gmail.com', '');

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `ID_User` int(1) NOT NULL AUTO_INCREMENT,
  `Login_User` varchar(55) NOT NULL,
  `Pswd_User` varchar(64) NOT NULL,
  `Statut_User` char(1) NOT NULL,
  PRIMARY KEY (`ID_User`),
  UNIQUE KEY `ID_User_IND` (`ID_User`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- D�chargement des donn�es de la table `user`
--

INSERT INTO `user` (`ID_User`, `Login_User`, `Pswd_User`, `Statut_User`) VALUES
(1, 'admin', '8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918', 's');

--
-- Contraintes pour les tables d�charg�es
--

--
-- Contraintes pour la table `assurer`
--
ALTER TABLE `assurer`
  ADD CONSTRAINT `EQU_Assur_Acteu` FOREIGN KEY (`ID_Acteur`) REFERENCES `acteurs` (`ID_Acteur`),
  ADD CONSTRAINT `REF_Assur_Spect_FK` FOREIGN KEY (`ID_Spectacle`) REFERENCES `spectacle` (`ID_Spectacle`);

--
-- Contraintes pour la table `chaise`
--
ALTER TABLE `chaise`
  ADD CONSTRAINT `EQU_Chais_Reser_FK` FOREIGN KEY (`ID_Reservation`) REFERENCES `reservation` (`ID_Reservation`),
  ADD CONSTRAINT `REF_Chais_Repre` FOREIGN KEY (`ID_Representation`) REFERENCES `representation` (`ID_Representation`);

--
-- Contraintes pour la table `representation`
--
ALTER TABLE `representation`
  ADD CONSTRAINT `REF_Repre_Spect_FK` FOREIGN KEY (`ID_Spectacle`) REFERENCES `spectacle` (`ID_Spectacle`);

--
-- Contraintes pour la table `reservation`
--
ALTER TABLE `reservation`
  ADD CONSTRAINT `REF_Reser_Repre_FK` FOREIGN KEY (`ID_Representation`) REFERENCES `representation` (`ID_Representation`),
  ADD CONSTRAINT `REF_Reser_Spect_FK` FOREIGN KEY (`ID_Spectateur`) REFERENCES `spectateur` (`ID_Spectateur`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;