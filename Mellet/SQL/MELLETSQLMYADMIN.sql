-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  lun. 30 avr. 2018 à 15:31
-- Version du serveur :  5.7.19
-- Version de PHP :  5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `mellet`
--

-- --------------------------------------------------------

--
-- Structure de la table `acteurs`
--

DROP TABLE IF EXISTS `acteurs`;
CREATE TABLE IF NOT EXISTS `acteurs` (
  `ID_Acteur` int(2) NOT NULL AUTO_INCREMENT,
  `Nom_Acteur` varchar(20) NOT NULL,
  `Prenom_Acteur` varchar(20) NOT NULL,
  `Statut_Acteurs` varchar(20) NOT NULL,
  PRIMARY KEY (`ID_Acteur`),
  UNIQUE KEY `ID_Acteurs_IND` (`ID_Acteur`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;
-- --------------------------------------------------------

--
-- Structure de la table `assurer`
--

DROP TABLE IF EXISTS `assurer`;
CREATE TABLE IF NOT EXISTS `assurer` (
  `ID_Acteur` int(2) NOT NULL AUTO_INCREMENT,
  `ID_Spectacle` int(2) NOT NULL,
  `Role_Acteur` varchar(20) NOT NULL,
  PRIMARY KEY (`ID_Acteur`,`ID_Spectacle`),
  UNIQUE KEY `ID_Assurer_IND` (`ID_Acteur`,`ID_Spectacle`),
  KEY `REF_Assur_Spect_IND` (`ID_Spectacle`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;
-- --------------------------------------------------------

--
-- Structure de la table `chaise`
--

DROP TABLE IF EXISTS `chaise`;
CREATE TABLE IF NOT EXISTS `chaise` (
  `ID_Chaise` int(10) NOT NULL AUTO_INCREMENT,
  `ID_Representation` int(2) NOT NULL,
  `Num_Lignes_Chaise` varchar(1) NOT NULL,
  `Num_Col_Chaise` int(2) NOT NULL,
  `Statut_Chaise` varchar(1) NOT NULL,
  `ID_Reservation` int(10) DEFAULT NULL,
  PRIMARY KEY (`ID_Chaise`),
  UNIQUE KEY `ID_Chaise_IND` (`ID_Chaise`),
  KEY `EQU_Chais_Reser_IND` (`ID_Reservation`),
  KEY `REF_Chais_Repre` (`ID_Representation`)
) ENGINE=InnoDB AUTO_INCREMENT=601 DEFAULT CHARSET=latin1;
-- --------------------------------------------------------

--
-- Structure de la table `representation`
--

DROP TABLE IF EXISTS `representation`;
CREATE TABLE IF NOT EXISTS `representation` (
  `ID_Representation` int(2) NOT NULL AUTO_INCREMENT,
  `Date_Representation` datetime NOT NULL,
  `Nb_Lignes_Representation` int(2) NOT NULL,
  `Nb_Col_Representation` int(2) NOT NULL,
  `ID_Spectacle` int(2) NOT NULL,
  PRIMARY KEY (`ID_Representation`),
  UNIQUE KEY `ID_Representation_IND` (`ID_Representation`),
  KEY `REF_Repre_Spect_IND` (`ID_Spectacle`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
-- --------------------------------------------------------

--
-- Structure de la table `reservation`
--

DROP TABLE IF EXISTS `reservation`;
CREATE TABLE IF NOT EXISTS `reservation` (
  `ID_Reservation` int(3) NOT NULL AUTO_INCREMENT,
  `NbPlaces_Reser` int(2) NOT NULL,
  `ID_Representation` int(2) NOT NULL,
  `ID_Spectateur` int(4) NOT NULL,
  PRIMARY KEY (`ID_Reservation`),
  UNIQUE KEY `ID_Reservation_IND` (`ID_Reservation`),
  KEY `REF_Reser_Repre_IND` (`ID_Representation`),
  KEY `REF_Reser_Spect_IND` (`ID_Spectateur`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
-- --------------------------------------------------------

--
-- Structure de la table `spectacle`
--

DROP TABLE IF EXISTS `spectacle`;
CREATE TABLE IF NOT EXISTS `spectacle` (
  `ID_Spectacle` int(2) NOT NULL AUTO_INCREMENT,
  `Titre_Spectacle` varchar(55) NOT NULL,
  `Prix_Spectacle` float NOT NULL,
  PRIMARY KEY (`ID_Spectacle`),
  UNIQUE KEY `ID_Spectacle_IND` (`ID_Spectacle`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
-- --------------------------------------------------------

--
-- Structure de la table `spectateur`
--

DROP TABLE IF EXISTS `spectateur`;
CREATE TABLE IF NOT EXISTS `spectateur` (
  `ID_Spectateur` int(4) NOT NULL AUTO_INCREMENT,
  `Nom_Spectateur` varchar(20) NOT NULL,
  `Prenom_Spectateur` varchar(20) NOT NULL,
  `Rue_Spectateur` varchar(55) NOT NULL,
  `NumeroRue_Spectateur` varchar(10) NOT NULL,
  `CP_Spectateur` int(6) NOT NULL,
  `Ville_Spectateur` varchar(55) NOT NULL,
  `NumFix_Spectateur` varchar(12) NOT NULL,
  `NumGsm_Spectateur` varchar(16) NOT NULL,
  `Email_Spectateur` varchar(55) NOT NULL,
  `Com_Spectateur` varchar(55) NOT NULL,
  PRIMARY KEY (`ID_Spectateur`),
  UNIQUE KEY `ID_Spectateur_IND` (`ID_Spectateur`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
-- --------------------------------------------------------

--
-- Structure de la table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `ID_User` int(1) NOT NULL AUTO_INCREMENT,
  `Login_User` varchar(55) NOT NULL,
  `Pswd_User` varchar(64) NOT NULL,
  `Statut_User` char(1) NOT NULL,
  PRIMARY KEY (`ID_User`),
  UNIQUE KEY `ID_User_IND` (`ID_User`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `user`
--

INSERT INTO `user` (`ID_User`, `Login_User`, `Pswd_User`, `Statut_User`) VALUES
(1, 'admin', '8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918', 's');
--
-- Contraintes pour la table `assurer`
--
ALTER TABLE `assurer`
  ADD CONSTRAINT `EQU_Assur_Acteu` FOREIGN KEY (`ID_Acteur`) REFERENCES `acteurs` (`ID_Acteur`),
  ADD CONSTRAINT `REF_Assur_Spect_FK` FOREIGN KEY (`ID_Spectacle`) REFERENCES `spectacle` (`ID_Spectacle`);

--
-- Contraintes pour la table `chaise`
--
ALTER TABLE `chaise`
  ADD CONSTRAINT `EQU_Chais_Reser_FK` FOREIGN KEY (`ID_Reservation`) REFERENCES `reservation` (`ID_Reservation`),
  ADD CONSTRAINT `REF_Chais_Repre` FOREIGN KEY (`ID_Representation`) REFERENCES `representation` (`ID_Representation`);

--
-- Contraintes pour la table `representation`
--
ALTER TABLE `representation`
  ADD CONSTRAINT `REF_Repre_Spect_FK` FOREIGN KEY (`ID_Spectacle`) REFERENCES `spectacle` (`ID_Spectacle`);

--
-- Contraintes pour la table `reservation`
--
ALTER TABLE `reservation`
  ADD CONSTRAINT `REF_Reser_Repre_FK` FOREIGN KEY (`ID_Representation`) REFERENCES `representation` (`ID_Representation`),
  ADD CONSTRAINT `REF_Reser_Spect_FK` FOREIGN KEY (`ID_Spectateur`) REFERENCES `spectateur` (`ID_Spectateur`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
