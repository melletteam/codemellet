<?php

/*
 * Ce service permet l'accès de JasperStudio à la BDD afin de générer un plan de salle pour une représentation.
 */

require_once '../RapportsJaspersoft/JasperPHP/JasperPHP.php';

$jasper= new JasperPHP();

$data = explode('_',$_POST['ID_Representation']);

$jasper->process(
    '..\RapportsJaspersoft\RapportPlanSalle.jasper',//emplacement rapport compiler + rapport
    false,
    array('pdf'),
    array('ID_Representation'=>$data[0],'directory'=>'../RapportsJaspersoft/'),
    array(
        'driver'=>'mysql',
        'username'=>'root',
        'host'=>'localhost',
        'database'=>'mellet',
        'port'=>'3306'
    )
)->execute();

$file = '..\RapportsJaspersoft\RapportPlanSalle.pdf';
header('Content-type: application/pdf');
header('Content-Disposition: inline; filename="PlanSalle'.$data[1].'.pdf"');
@readfile($file);