<?php
/*
 * Ce service permet la modification du statut d'un utilisateur à l'aide d'un bouton switch
 */

require_once  '../manager/DBManager.php';
require_once '../manager/administrationManager.php';
require_once '../model/administration.php';

$dbManager = new DBManager();
$pdo = $dbManager->connect();
$administrationManager = new administrationManager($pdo);
$user = null;
$location= "Location: ../view/administration.php";


if(!empty($_POST['ID_User'])){

    $id= (int) $_POST['ID_User'];

    $statutActuel=$administrationManager->getStatut($id);
    if($statutActuel == 's'){
        $statut='u';
    }else if($statutActuel == 'u')  {$statut='s';};

    $administrationManager->modify($id,$statut);
}else {
    echo("Erreur lors de la modification");
}
header($location);