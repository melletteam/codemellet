
<?php
/*
 * Ce service permet de supprimer un spectacle.
 */

require_once  '../manager/DBManager.php';
require_once '../manager/administrationManager.php';
require_once '../model/administration.php';

$dbManager = new DBManager();
$pdo = $dbManager->connect();
$administrationManager = new administrationManager($pdo);
$location= "Location: ../view/administration.php";

$administrationManager->deleteSpectacles();

header($location);