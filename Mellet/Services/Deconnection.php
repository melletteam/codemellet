<?php
/*
 * Ce service permet de détruire les cookies de l'utilisateur connecté afin de sécuriser le site
 */
if(isset($_COOKIE['ID_login']) && isset($_COOKIE['Name_login']) && isset($_COOKIE['Statut_login'])){
    setcookie('ID_login',false,time()-3600*24,'/');
    setcookie("Name_login",false, time()-3600*24,'/');
    setcookie("Statut_login",false, time()-3600*24,'/');
    unset($_COOKIE['ID_login']);
    unset($_COOKIE['Name_login']);
    unset($_COOKIE['Statut_login']);
}
header("Location: ../index.php");