<?php
/*
 * Ce service permet la suppresion d'un User de la BDD.
 */
require_once  '../manager/DBManager.php';
require_once '../manager/administrationManager.php';
require_once '../model/administration.php';

$dbManager = new DBManager();
$pdo = $dbManager->connect();
$administrationManager = new administrationManager($pdo);
$id = null;
$location= "Location: ../view/administration.php";

if(!empty($_POST['ID_User'])){
    $id= (int)$_POST['ID_User'];
    $administrationManager->delete($id);
}else {
    echo("Erreur lors de la suppresion");
}
header($location);