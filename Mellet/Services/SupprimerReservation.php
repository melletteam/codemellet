<?php
/*
 * Ce service permet de supprimer une réservation pour une représentation précise.
 */
require_once  '../manager/DBManager.php';
require_once '../manager/chaiseManager.php';
require_once '../manager/reservationManager.php';
require_once '../model/chaise.php';
require_once '../model/reservation.php';
$dbManager = new DBManager();
$pdo = $dbManager->connect();
$reservationManager= new reservationManager($pdo);
$chaiseManager=new chaiseManager($pdo);
$chaise = new chaise();
$reser= new reservation();
$idRep = null;
$idRes = null;
$location= "Location: ../view/reservation.php";
/*
 * Vérification de la représentation où l'on veut supprimer la réservation ensuite il l'enlève.
 */
if(!empty($_POST['idRepSup'])){
    $idRep=$_POST['idRepSup'];
    $idRes=$_POST['IDRes'];
    $nbPlaceReser=$reservationManager->selectByIDRes($idRes);
    $nbPlace=$nbPlaceReser->getNbPlaces_Reser();



    //pour la boucle aller rechercher le nbre de places par reservation
    for($i=0;$i<$nbPlace;$i++){
        $chaise->setID_Representation((int)$idRep);
        $chaise->setID_Reservation((int)$idRes);
        $chaiseManager->UpdateChaiseByID($chaise);
    }
    $reser->setID_Reservation($idRes);
    $reser->setID_Representation($idRep);
    $reservationManager->delete($reser);



}    header($location);