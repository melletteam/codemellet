<?php
/*
 * Ce service permet de gérer le plan de salle d'une représentation et de rendre une place non disponible
 */
require_once  '../manager/DBManager.php';
require_once '../model/chaise.php';
require_once '../manager/chaiseManager.php';
$dbManager = new DBManager();
$pdo = $dbManager->connect();
$chaiseManager= new chaiseManager($pdo);
$chaise= new chaise();
$NbPlaces=null;
$NLignes=null;
$NCol=null;
$location= "Location: ../view/salle.php";
/*
 * Vérifie si une place est sélectionnée afin de la rendre non disponible
 */
if(isset($_POST['place'])) {
    $idrep = $_POST['idrep'];
    $place = $_POST['place'];
    $typeR = "A";

  $places=explode(",",$place);
    //calcule le nombre de chaise(s)
    $nb=count($places);
    //le met dans la variable qui va vers la DB
    $NbPlaces=$nb;
    $val[]=null;
    for($i=0;$i<$nb;$i++){
        $val[$i]=str_split($places[$i]);
        //verifie si le chiffre est composée de 2 chiffre
        if(count($val[$i])==3){
            $NLignes=$val[$i][0];
            $NCol=$val[$i][1].$val[$i][2];
            $chaise->setID_Representation((int)$idrep);
            $chaise->setID_Reservation(null);
            $chaise->setStatut_Chaise($typeR);
            $chaise->setNum_Lignes_Chaise($NLignes);
            $chaise->setNum_Col_Chaise((int)$NCol);
        }else{
            $NLignes=$val[$i][0];
            $NCol=$val[$i][1];
            $chaise->setID_Representation((int)$idrep);
            $chaise->setID_Reservation(null);
            $chaise->setStatut_Chaise($typeR);
            $chaise->setNum_Lignes_Chaise($NLignes);
            $chaise->setNum_Col_Chaise((int)$NCol);
        }
        $chaiseManager->UpdateChaise($chaise);
    }

}
header($location);