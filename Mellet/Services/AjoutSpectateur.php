<?php
/*
 * Ce service permet l'ajout des spectateurs à la BDD
 */
require_once  '../manager/DBManager.php';
require_once '../manager/spectateurManager.php';
require_once '../model/spectateur.php';

$dbManager = new DBManager();
$pdo = $dbManager->connect();
$spectateurManager = new spectateurManager($pdo);
$spectateur = null;
$valid=null;
/*
 * Vérifie si les informations du spectateur sont bien renseignées puis ajoute le spectateur.
 */
if(!empty($_POST['nom']) && !empty($_POST['prenom'])&& !empty($_POST['adresse']) && !empty($_POST['numero']) && !empty($_POST['cp']) && !empty($_POST['ville'])){
    $nom = htmlentities($_POST['nom']);
    $prenom = htmlentities($_POST['prenom']);
    $adr = htmlentities($_POST['adresse']);
    $ville = htmlentities($_POST['ville']);
    $mail = htmlentities($_POST['mail']);
    $com =htmlentities($_POST['com']);
    $num=htmlentities($_POST["numero"]);
    $cp=htmlentities($_POST["cp"]);
    $fix=htmlentities($_POST["fix"]);
    $gsm=htmlentities($_POST["gsm"]);
    $spectateur = new spectateur();
    $spectateur->setNom_Spectateur($nom);
    $spectateur->setPrenom_Spectateur($prenom);
    $spectateur->setRue_Spectateur($adr);
    $spectateur->setNumeroRue_Spectateur($num);
    $spectateur->setCP_Spectateur($cp);
    $spectateur->setVille_Spectateur($ville);
    $spectateur->setNumFix_Spectateur($fix);
    $spectateur->setNumGsm_Spectateur($gsm);
    $spectateur->setEmail_Spectateur($mail);
    $spectateur->setCom_Spectateur($com);
    $spectateurManager->save($spectateur);
    $valid=1;
}else $valid=0;
switch ($valid){
    case 0:
        header("Location: ../view/spectateurs.php?ajout=0");
        break;
    case 1:
        header("Location: ../view/spectateurs.php?ajout=1");
        break;
}
