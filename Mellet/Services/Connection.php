<?php
/*
 * Ce service permet de créer 3 cookies afin de permettre à un utilisateur de naviguer dans le site et de définir son niveau de restriction.
 * Sans cette connection aucun utilisateur ne pourra avoir accès au site.
 */
require_once  '../manager/DBManager.php';
require_once '../manager/administrationManager.php';
require_once '../model/user.php';
$dbManager = new DBManager();
$pdo = $dbManager->connect();
$administrationManager = new administrationManager($pdo);
$user=null;
$valid=0;
$location= "Location: ../view/Reservation.php";
if(isset($_POST["user"])&& isset($_POST["pass"])){
    $user=$administrationManager->getUserByName($_POST["user"]);
    if($user!=null){
        $pswdChiff=hash('sha256',$_POST["pass"]);
        if($user->getPswd_User()==$pswdChiff){
            setcookie("ID_login", $user->getID_User(), (time() + 3600),'/');
            setcookie("Name_login",$user->getLogin_User(), (time() + 3600),'/');
            setcookie("Statut_login",$user->getStatut_User(), (time() + 3600),'/');
            $valid=1;
        }
    }
}
switch ($valid){
    case 0:
        header("Location: ../index.php?connection=0");
        break;
    case 1:
        header("Location: ../view/Reservation.php");
        break;
}
?>