<?php

/*
 * Ce service permet l'accès de JasperStudio à la BDD afin de générer une liste de réservation pour une représentation.
 */

require_once '../RapportsJaspersoft/JasperPHP/JasperPHP.php';

$jasper= new JasperPHP();

$data = explode('_',$_POST['ID_Representation']);

$jasper->process(
    '..\RapportsJaspersoft\RapportListeReservation.jasper',//emplacement rapport compiler + rapport
    false,
    array('pdf'),
    array('ID_Representation'=>$data[0],'directory'=>'../RapportsJaspersoft/'),
    array(
        'driver'=>'mysql',
        'username'=>'root',
        'host'=>'localhost',
        'database'=>'mellet',
        'port'=>'3306'
    )
)->execute();

$file = '..\RapportsJaspersoft\RapportListeReservation.pdf';
header('Content-type: application/pdf');
header('Content-Disposition: inline; filename="ListeReservation'.$data[1].'.pdf"');
@readfile($file);