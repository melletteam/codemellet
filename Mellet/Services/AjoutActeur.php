<?php
/*
 * Ce service permet d'ajouter un Acteur à la BDD.
 */
require_once  '../manager/DBManager.php';
require_once '../manager/ActeursManager.php';
require_once '../model/Acteurs.php';

$dbManager = new DBManager();
$pdo = $dbManager->connect();
$acteursManager = new acteursManager($pdo);
$acteur = null;
$valid=null;
/*
 * Contrôle des valeurs avant l'ajout
 */
if( isset($_POST["nom"]) && isset($_POST["prenom"])&& isset($_POST["statut"])){
    $nom=htmlentities($_POST["nom"]);
    $prenom=htmlentities($_POST["prenom"]);
    $acteur = new acteurs();
    $acteur->setNom_Acteur($nom);
    $acteur->setPrenom_Acteur($prenom);
    $acteur->setStatut_Acteurs($_POST["statut"]);
    $acteursManager->save($acteur);
    $valid=1;
}else $valid=0;
switch ($valid){
    case 0:
        header("Location: ../view/acteurs.php?ajout=0");
        break;
    case 1:
        header("Location: ../view/acteurs.php?ajout=1");
        break;
}