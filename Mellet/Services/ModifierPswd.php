<?php
/*
 * Ce service permet la modification de l'utilisateur connecté au site.
 */

require_once  '../manager/DBManager.php';
require_once '../manager/administrationManager.php';
require_once '../model/administration.php';

$dbManager = new DBManager();
$pdo = $dbManager->connect();
$administrationManager = new administrationManager($pdo);
$id = null;
$user = null;
$location= "Location: ../view/administration.php";
/*
 * Vérification des champs avant modification
 * Ce code vérifie également l'ancien mot de passe afin de savoir si l'utilisateur connecté est bien maître de son compte pour ensuite
 * modifier son mot de passe.
 */
if(!empty($_COOKIE['ID_login']) && !empty($_POST['pswd']) && !empty($_POST['oldPswd'])) {

    $id = (int)$_COOKIE['ID_login'];
    $user = $administrationManager->getUser($id);
    $pswdActuel = $user->getPswd_User();//pswd en BD
    $oldPswd = $_POST['oldPswd'];//ancien pswd entrer par le user
    $oldPswdChiff = hash('sha256', $oldPswd);//algo de Hash => SHA-256 : Produit un hash en 256 bits
    $pswd = $_POST['pswd'];//nouvea pswd entrer par le user
    $newPswdChiff = hash('sha256', $pswd);//algo de Hash => SHA-256 : Produit un hash en 256 bits
    if (($pswdActuel == $oldPswdChiff)) {
        $administrationManager->modifierPswd($id,$newPswdChiff);
        $valid = 2;
    } elseif ($pswdActuel != $oldPswd) {
        $valid = 1;
    } elseif ($pswd != $pswd) {
        $valid = 0;
    }
}
    switch ($valid){
        case 0:
            header("Location: ../view/administration.php?modif=0");
            break;
        case 1:
            header("Location: ../view/administration.php?modif=1");
            break;
        case 2:
            header("Location: ../view/administration.php?modif=2");
            break;
    }

