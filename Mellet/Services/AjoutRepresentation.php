<?php
/*
 * Ce service permet d'ajouter une représentation.
 */
require_once  '../manager/DBManager.php';
require_once '../manager/SpectacleManager.php';
require_once '../manager/chaiseManager.php';
require_once '../manager/representationManager.php';
require_once '../model/Spectacle.php';
require_once '../model/representation.php';
require_once '../model/chaise.php';

$dbManager = new DBManager();
$pdo = $dbManager->connect();
$spectacleManager= new spectacleManager($pdo);
$representationManager= new representationManager($pdo);
$chaiseManager= new chaiseManager($pdo);
$valid=null;
/*
 * Ce bout de code vérifie si le titre du spectacle est défini.
 * Si des représentations de se spectacle sont ajoutées, alors ce code les ajoutera en BDD également
 */
if(isset($_POST['Spectacle']))
{

    $idSpectacle = htmlentities($_POST["Spectacle"]);
    $nbR= htmlentities($_POST['nbRepresentationAdd']);
    $representation= new representation();
    $representation->setID_Spectacle($idSpectacle);
    if($idSpectacle != null) {
        if ($nbR > 0) {
            //creation objet chaise
            $arrayLettre = array(0 => "A", 1 => "B", 2 => "C", 3 => "D", 4 => "E", 5 => "F", 6 => "G", 7 => "H", 8 => "I", 9 => "J", 10 => "K", 11 => "L", 12 => "M", 13 => "N", 14 => "O", 15 => "P", 16 => "Q", 17 => "R", 18 => "S", 19 => "T");
            $chaise = new chaise();
            $chaise->setStatut_Chaise("D");
            for ($i = 0; $i <$nbR; $i++) {
                $nbColRep=htmlentities($_POST['nbColonneRepresentation' . "$i"]);
                $nbLigRep=htmlentities($_POST['nbLigneRepresentation' . "$i"]);
                $representation->setDate_Representation($_POST['datepicker' . "$i"]);
                $representation->setNb_Col_Representation($nbColRep);
                $representation->setNb_Lignes_Representation($nbLigRep);
                $idRepresentation = $representationManager->createRepresentationAndReturnId($representation);
                $representation->setID_Representation($idRepresentation);
                //AJOUT DES CHAISES DANS LA BASE DE DONNEE
                $chaise->setID_Representation($representation->getID_Representation());
                $nbLigne = intval($representation->getNb_Lignes_Representation(), 10);
                $nbCol = intval($representation->getNb_Col_Representation(), 10);
                for ($ligne = 0; $ligne < $nbLigne; $ligne++) {
                    $chaise->setNum_Lignes_Chaise($arrayLettre[$ligne]);
                    for ($col = 0; $col < $nbCol; $col++) {
                        $chaise->setNum_Col_Chaise(($col + 1));
                        $chaiseManager->ajoutChaise($chaise);
                    }
                }

            }
            $valid=1;
        }
    }
}else $valid=0;
switch ($valid){
    case 0:
        header("Location: ../view/Spectacle.php?ajoutRep=0");
        break;
    case 1:
        header("Location: ../view/Spectacle.php?ajoutRep=1");
        break;
}