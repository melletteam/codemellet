<?php
/*
 * Ce service permet l'ajout en BDD d'une réservation après la sélection des places et du spectateur
 * qui voudrait réserver.
 */
require_once  '../manager/DBManager.php';
require_once '../model/reservation.php';
require_once '../model/chaise.php';
require_once '../manager/reservationManager.php';
require_once '../manager/chaiseManager.php';
$dbManager = new DBManager();
$pdo = $dbManager->connect();
$reservationManager= new reservationManager($pdo);
$chaiseManager= new chaiseManager($pdo);
$reservation=new reservation();
$chaise= new chaise();
$NbPlaces=null;
$NLignes=null;
$NCol=null;
$location= "Location: ../view/reservation.php";
/*
 * Vérification si des places sont sélectionnées.
 * Si oui, alors une boucle ajoutera une à une les places à la BDD.
 */
if(isset($_POST['place'])){
    $idrep=htmlentities($_POST['idRep']);
    $idspect=htmlentities($_POST['idSpect']);
    $place=htmlentities($_POST['place']);
    $typeR=htmlentities($_POST['typeReservation']);
    $places=explode(",",$place);
    //calcule le nombre de chaise(s)
    $nb=count($places);
    //le met dans la variable qui va vers la DB
    $NbPlaces=$nb;
    $reservation->setID_Representation((int)$idrep);
    $reservation->setID_Spectateur((int)$idspect);
    $reservation->setNbPlaces_Reser($NbPlaces);
    $reserID=$reservationManager->ajoutReservation($reservation);
    $val[]=null;
    for($i=0;$i<$nb;$i++){
        $val[$i]=str_split($places[$i]);
        //verifie si le chiffre est composée de 2 chiffre
        if(count($val[$i])==3){
            $NLignes=$val[$i][0];
            $NCol=$val[$i][1].$val[$i][2];
            $chaise->setID_Representation((int)$idrep);
            $chaise->setID_Reservation((int)$reserID);
            $chaise->setStatut_Chaise($typeR);
            $chaise->setNum_Lignes_Chaise($NLignes);
            $chaise->setNum_Col_Chaise((int)$NCol);
        }else{
            $NLignes=$val[$i][0];
            $NCol=$val[$i][1];
            $chaise->setID_Representation((int)$idrep);
            $chaise->setID_Reservation((int)$reserID);
            $chaise->setStatut_Chaise($typeR);
            $chaise->setNum_Lignes_Chaise($NLignes);
            $chaise->setNum_Col_Chaise((int)$NCol);
        }
        $chaiseManager->UpdateChaise($chaise);
    }



}
header($location);
