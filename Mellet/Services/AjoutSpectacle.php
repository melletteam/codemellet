<?php
/*
 * Ce service permet l'ajout du spectacle en BDD ainsi que les acteurs qui assurent celui-ci
 */
require_once  '../manager/DBManager.php';
require_once '../manager/SpectacleManager.php';
require_once '../manager/AssurerManager.php';
require_once '../manager/chaiseManager.php';
require_once '../manager/representationManager.php';
require_once '../model/Spectacle.php';
require_once '../model/Assurer.php';
require_once '../model/representation.php';
require_once '../model/chaise.php';
$dbManager = new DBManager();
$pdo = $dbManager->connect();
$spectacleManager= new spectacleManager($pdo);
$assurerManager= new assurerManager($pdo);
$representationManager= new representationManager($pdo);
$chaiseManager= new chaiseManager($pdo);
$valid=null;
/*
 * Ce morceau de code vérifie si le titre du spectacle est bien défini.
 * Si oui, le code va permettre l'ajout de toutes les informations utiles à celui-ci(acteur,représentation,...)
 */
if(isset($_POST['titre']))
{
    $idSpectacle = $spectacleManager->createSpectacleAndReturnID($_POST['titre'],$_POST['prix']);
    $assure= new assurer();
    $assure->setID_Spectacle($idSpectacle);
    $representation= new representation();
    $representation->setID_Spectacle($idSpectacle);
    if($idSpectacle != null) {
        $valid=1;
        if ($_POST['nbRepresentation'] > 0) {
            //creation objet chaise
            $arrayLettre=array(0=>"A",1=>"B",2=>"C",3=>"D",4=>"E",5=>"F",6=>"G",7=>"H",8=>"I",9=>"J",10=>"K",11=>"L",12=>"M",13=>"N",14=>"O",15=>"P",16=>"Q",17=>"R",18=>"S",19=>"T");
            $chaise= new chaise();
            $chaise->setStatut_Chaise("D");
            for ($i = 0; $i < $_POST['nbRepresentation']; $i++) {
                $representation->setDate_Representation($_POST['datepicker' . "$i"]);
                $representation->setNb_Col_Representation($_POST['nbColonneRepresentation' . "$i"]);
                $representation->setNb_Lignes_Representation($_POST['nbLigneRepresentation' . "$i"]);
                $idRepresentation=$representationManager->createRepresentationAndReturnId($representation);
                $representation->setID_Representation($idRepresentation);
                //AJOUT DES CHAISES DANS LA BASE DE DONNEE
                $chaise->setID_Representation($representation->getID_Representation());
                $nbLigne=intval($representation->getNb_Lignes_Representation(),10);
                $nbCol=intval($representation->getNb_Col_Representation(),10);
                for ($ligne=0;$ligne<$nbLigne;$ligne++) {
                    $chaise->setNum_Lignes_Chaise($arrayLettre[$ligne]);
                    for ($col = 0; $col <$nbCol; $col++) {
                        $chaise->setNum_Col_Chaise(($col + 1));
                        $chaiseManager->ajoutChaise($chaise);
                    }
                }

            }
        }
        /*
         * Vérifie si des acteurs sont à ajouter à la table Assurer.Si oui, le code s'en occupe.
         */
        if ($_POST['nbActeur'] > 0) {
            for ($i = 0; $i < $_POST['nbActeur']; $i++) {
                $assure->setID_Acteur($_POST['acteur' . "$i"]);
                $assure->setRole_Acteur($_POST['roleActeur' . "$i"]);
                $assurerManager->addAssurer($assure);
            }
        }
        if ($_POST['nbMise'] > 0) {
            for ($i = 0; $i < $_POST['nbMise']; $i++) {
                $assure->setID_Acteur($_POST['mise' . "$i"]);
                $assure->setRole_Acteur($_POST['roleMise' . "$i"]);
                $assurerManager->addAssurer($assure);
            }
        }
        if ($_POST['nbDecor'] > 0) {
            for ($i = 0; $i < $_POST['nbDecor']; $i++) {
                $assure->setID_Acteur($_POST['decor' . "$i"]);
                $assure->setRole_Acteur($_POST['roleDecor' . "$i"]);
                $assurerManager->addAssurer($assure);
            }
        }
        if ($_POST['nbAide'] > 0) {
            for ($i = 0; $i < $_POST['nbAide']; $i++) {
                $assure->setID_Acteur($_POST['aide' . "$i"]);
                $assure->setRole_Acteur($_POST['roleAide' . "$i"]);
                $assurerManager->addAssurer($assure);
            }
        }
        if ($_POST['nbRegie'] > 0) {
            for ($i = 0; $i < $_POST['nbRegie']; $i++) {
                $assure->setID_Acteur($_POST['regie' . "$i"]);
                $assure->setRole_Acteur($_POST['roleRegie' . "$i"]);
                $assurerManager->addAssurer($assure);
            }
        }
        if ($_POST['nbPresentation'] > 0) {
            for ($i = 0; $i < $_POST['nbPresentation']; $i++) {
                $assure->setID_Acteur($_POST['presentation' . "$i"]);
                $assure->setRole_Acteur($_POST['rolePresentation' . "$i"]);
                $assurerManager->addAssurer($assure);
            }
        }
        if ($_POST['nbMaquillage'] > 0) {
            for ($i = 0; $i < $_POST['nbMaquillage']; $i++) {
                $assure->setID_Acteur($_POST['maquillage' . "$i"]);
                $assure->setRole_Acteur($_POST['roleMaquillage' . "$i"]);
                $assurerManager->addAssurer($assure);
            }
        }
    }
}else $valid=0;
switch ($valid){
    case 0:
        header("Location: ../view/Spectacle.php?ajoutSpect=0");
        break;
    case 1:
        header("Location: ../view/Spectacle.php?ajoutSpect=1");
        break;
}