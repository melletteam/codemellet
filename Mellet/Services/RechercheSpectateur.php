<?php
/*
 * Ce service permet l'affichage de tous les spectateurs en BDD  dont le nom est égal au nom inscrit.
 */
require_once  '../manager/DBManager.php';
require_once '../manager/SpectateurManager.php';
require_once '../model/Spectateur.php';

$dbManager = new DBManager();
$pdo = $dbManager->connect();
$spectateurManager = new spectateurManager($pdo);
$spectateur = null;
$location= "Location: ../view/spectateurs.php";

if(!empty($_POST['nom'])){

    $nom = htmlentities($_POST['nom']);
    $spectateurManager->getfullSpectateur($nom);

    header($location);
}