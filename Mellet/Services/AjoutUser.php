<?php
/*
 * ce service permet l'ajout d'un user a la BDD
 */
require_once  '../manager/DBManager.php';
require_once '../manager/administrationManager.php';
require_once '../model/user.php';

$dbManager = new DBManager();
$pdo = $dbManager->connect();
$administrationManager = new administrationManager($pdo);
$user = null;
$valid=null;
/*
 * Contrôle des informations afin de vérifier si l'User est valide
 */
if( !empty($_POST['login']) && !empty($_POST['pswd'])&& !empty($_POST['statut'])){
    $log=htmlentities($_POST["login"]);
    $pswd=htmlentities($_POST["pswd"]);
    $user = new user();
    $pswdChiff=hash('sha256',$pswd);//algo de Hash => SHA-256 : Produit un hash en 256 bits
    $user->setLogin_User($log);
    $user->setPswd_User($pswdChiff);
    $user->setStatut_User($_POST['statut']);
    $verifUser=$administrationManager->getUserByName($user->getLogin_User());
    if($verifUser==null || ($user->getLogin_User()!= $verifUser->getLogin_User())) {
        $administrationManager->save($user);
        $valid=2;
    }else $valid=1;
}else $valid=0;
switch ($valid){
    case 0:
        header("Location: ../view/administration.php?ajout=0");
        break;
    case 1:
        header("Location: ../view/administration.php?ajout=1");
        break;
    case 2:
        header("Location: ../view/administration.php?ajout=2");
        break;
}
